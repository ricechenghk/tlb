package hk.com.playmore.syncframework.util;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class RunnableJSON implements RunnableArgument {
	@Override
	final public void run(String object) {
		try {
			JSONObject jsonObject = new JSONObject(object);
//			if (jsonObject.has("sort")) {
//				jsonObject.put("sort", Integer.valueOf(jsonObject.optString("sort")));
//			}
			run(jsonObject);
		} catch (JSONException e) {
//			e.printStackTrace();
			try {
				JSONArray array = new JSONArray(object);
//				loopArray(array);
				run(array);
				
			} catch (JSONException e1) {
				e1.printStackTrace();
				onJSONException(e1.getLocalizedMessage());
			}
		}
	}

	public abstract void run(JSONObject object);
	public abstract void run(JSONArray array);
	public abstract void onJSONException(String e);
}
