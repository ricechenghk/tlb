package hk.com.playmore.syncframework.util;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;

import android.util.Log;

import com.j256.ormlite.dao.Dao;

public class DatabaseSyncer<T extends DatabaseModel> {
	public interface DatabaseSyncerListener{
		public void willUpdateObject(DatabaseModel oldModel, DatabaseModel newModel);
		public void didUpdateObject(DatabaseModel oldModel, DatabaseModel newModel);
	}
	
	protected class ModelFieldsHelper {
		private Field[] cachedLocalFields;
		private Field[] cachedLocalIfNullFields;

		public ModelFieldsHelper() {
			cachedLocalFields = null;
			cachedLocalIfNullFields = null;
		}
		
		public void process(T server) {
			if (cachedLocalFields == null
					|| cachedLocalIfNullFields == null) {
				cachedLocalFields = new Field[0];
				cachedLocalIfNullFields = new Field[0];
				ArrayList<Field> localFields = new ArrayList<Field>();
				ArrayList<Field> localIfNullFields = new ArrayList<Field>();
				Class<?> clazz = server.getClass();
				while (clazz != null) {
					Field[] fields = clazz.getDeclaredFields();
					for (Field field : fields) {
						field.setAccessible(true);
						SyncLocal localAnnotation = field
								.getAnnotation(SyncLocal.class);
						if (localAnnotation != null) {
							localFields.add(field);
						}
						SyncLocalIfNull localIfNullAnnotation = field
								.getAnnotation(SyncLocalIfNull.class);
						if (localIfNullAnnotation != null) {
							localIfNullFields.add(field);
						}
					}
					
					clazz = clazz.getSuperclass();
				}
				cachedLocalFields = localFields.toArray(cachedLocalFields);
				cachedLocalIfNullFields = localIfNullFields
						.toArray(cachedLocalIfNullFields);
			}
		}

		public Field[] getCachedLocalFields() {
			return cachedLocalFields;
		}

		public Field[] getCachedLocalIfNullFields() {
			return cachedLocalIfNullFields;
		}

	}
	
	public HashSet<T> sync(final Dao<T, String> dao, List<T> localList, List<T> serverList, final DatabaseSyncerListener mDatabaseSyncerListener)
			throws Exception {
			return sync(dao, localList, serverList, true, mDatabaseSyncerListener);
	}
	
	@SuppressWarnings("unchecked")
	public HashSet<T> sync(final Dao<T, String> dao, List<T> localList, List<T> serverList, final boolean deleteOnNotFound, final DatabaseSyncerListener mDatabaseSyncerListener)
			throws Exception {
		final HashSet<T> result = new HashSet<T>();

		final Comparator<Object> idComparator = new Comparator<Object>() {
			@Override
			public int compare(Object olhs, Object orhs) {
				T lhs = (T) olhs;
				T rhs = (T) orhs;
				return lhs.getId().compareTo(rhs.getId());
			}
		};

		final Object[] locals = localList.toArray();
		Arrays.sort(locals, idComparator);

		final Object[] servers = serverList.toArray();
		Arrays.sort(servers, idComparator);

		final ModelFieldsHelper fieldsHelper = new ModelFieldsHelper();
		dao.callBatchTasks(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
        		int l = 0, s = 0;
        		while (l < locals.length && s < servers.length) {
        			T local = (T) locals[l];
        			T server = (T) servers[s];

        			String oid = local.getId();
        			String nid = server.getId();

        			if (oid.compareTo(nid) < 0) {
        				// Server id is even larger than us. Means old record is deleted
        				// in server. Proceeded one local.
        				if (deleteOnNotFound) {
        					dao.delete(local);
        				}
        				l++;
        			} else if (oid.compareTo(nid) > 0) {
        				// Server id is smaller than us. Found new item and add this.
        				// Proceed one server.
        				if (mDatabaseSyncerListener != null) {
        					mDatabaseSyncerListener.willUpdateObject(null, (DatabaseModel) servers[s]);
        				}
        				dao.createOrUpdate(server);
        				if (mDatabaseSyncerListener != null) {
        					mDatabaseSyncerListener.didUpdateObject(null, (DatabaseModel) servers[s]);
        				}
						result.add(server);
        				s++;
        			} else {
        				// Id is the same. Do the update. Process one server and one
        				// local.
        				setValuesForKeys(dao, fieldsHelper, local, server);
						
        				if (mDatabaseSyncerListener != null) {
        					mDatabaseSyncerListener.willUpdateObject(local, server);
        				}
        				dao.update(server);
        				if (mDatabaseSyncerListener != null) {
        					mDatabaseSyncerListener.didUpdateObject(local, server);
        				}
        				result.add(server);
        				l++;
        				s++;
        			}
        		}
        		while (l < locals.length) {
        			// Still have unprocessed old stuff. Remove them
        			dao.delete((T) locals[l]);
        			l++;
        		}
        		while (s < servers.length) {
        			// Still have unprocessed new stuff. Add them
        			if (mDatabaseSyncerListener != null) {
    					mDatabaseSyncerListener.willUpdateObject(null, (DatabaseModel) servers[s]);
    				}
        			dao.createOrUpdate((T) servers[s]);
        			if (mDatabaseSyncerListener != null) {
    					mDatabaseSyncerListener.didUpdateObject(null, (DatabaseModel) servers[s]);
    				}
        			result.add((T) servers[s]);
        			s++;
        		}
        		
        		return null;
            }
		});

		return result;
	}

	private void setValuesForKeys(Dao<T, String> dao,
			ModelFieldsHelper fieldsHelper, T local, T server)
			throws IllegalAccessException, SQLException {
		fieldsHelper.process(server);

		for (Field field : fieldsHelper.getCachedLocalFields()) {
			Log.d("model sync", "Use local copy of " + field.getName()
					+ " for " + local.getId());
			field.set(server, field.get(local));
		}
		for (Field field : fieldsHelper.getCachedLocalIfNullFields()) {
			if (field.get(server) == null) {
				Log.d("model sync",
						"Useed local copy of " + field.getName()
						+ " for " + local.getId() + " as null");
				field.set(server, field.get(local));
			}
		}

		dao.update(server);
	}

	public void sync(Dao<T, String> dao, T local, T server)
			throws SQLException, IllegalArgumentException,
			IllegalAccessException {
		ModelFieldsHelper fieldsHelper = new ModelFieldsHelper();
		
		if (local == null) {
			dao.createOrUpdate(server);
		} else {
			setValuesForKeys(dao, fieldsHelper, local, server);
		}
	}
	
	/*
	public interface DatabaseSyncerListener{
		public void willUpdateObject(DatabaseModel oldModel, DatabaseModel newModel);
		public void didUpdateObject(DatabaseModel oldModel, DatabaseModel newModel);
	}
	
	public static SyncLocal getSyncLocalAnnotation(Class<?> inputClass) {
		Class<?> clazz = inputClass;
		SyncLocal result = null;
		
		while (clazz != null) {
			result = clazz.getAnnotation(SyncLocal.class);
			if (result != null) {
				return result;
			}
			Field[] fields;
			try {
				fields = clazz.getDeclaredFields();
			} catch (Throwable t) {
				// amazingly, this sometimes throws an Error
				System.err.println("Could not load get delcared fields from: " + clazz);
				System.err.println("     " + t);
				return null;
			}
			for (Field field : fields) {
				result = field.getAnnotation(SyncLocal.class);
				if (result != null) {
					return result;
				}
			}
			try {
				clazz = clazz.getSuperclass();
			} catch (Throwable t) {
				// amazingly, this sometimes throws an Error
				System.err.println("Could not get super class for: " + clazz);
				System.err.println("     " + t);
				return null;
			}
		}

		return null;
	}
	
	public static SyncLocalIfNull getSyncLocalIfNullAnnotation(Class<?> inputClass) {
		Class<?> clazz = inputClass;
		SyncLocalIfNull result = null;
		
		while (clazz != null) {
			result = clazz.getAnnotation(SyncLocalIfNull.class);
			if (result != null) {
				return result;
			}
			Field[] fields;
			try {
				fields = clazz.getDeclaredFields();
			} catch (Throwable t) {
				// amazingly, this sometimes throws an Error
				System.err.println("Could not load get delcared fields from: " + clazz);
				System.err.println("     " + t);
				return null;
			}
			for (Field field : fields) {
				result = field.getAnnotation(SyncLocalIfNull.class);
				if (result != null) {
					return result;
				}
			}
			try {
				clazz = clazz.getSuperclass();
			} catch (Throwable t) {
				// amazingly, this sometimes throws an Error
				System.err.println("Could not get super class for: " + clazz);
				System.err.println("     " + t);
				return null;
			}
		}

		return null;
	}
	
	
	@SuppressWarnings("unchecked")
	public HashSet<T> sync(final Dao<T, String> dao, final List<T> localList, final List<T> serverList, final DatabaseSyncerListener mDatabaseSyncerListener)
			throws Exception, SQLException, IllegalArgumentException,
			IllegalAccessException {
		final HashSet<T> result = new HashSet<T>();
//		
		dao.callBatchTasks(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
        		Comparator<Object> idComparator = new Comparator<Object>() {
        			@Override
        			public int compare(Object olhs, Object orhs) {
        				T lhs = (T) olhs;
        				T rhs = (T) orhs;
        				return lhs.getId().compareTo(rhs.getId());
        			}
        		};

        		Object[] locals = localList.toArray();
        		Arrays.sort(locals, idComparator);

        		Object[] servers = serverList.toArray();
        		Arrays.sort(servers, idComparator);

        		Field[] cachedLocalFields = null;
        		Field[] cachedLocalIfNullFields = null;

        		int l = 0, s = 0;
        		while (l < locals.length && s < servers.length) {
        			T local = (T) locals[l];
        			T server = (T) servers[s];

        			String oid = local.getId();
        			String nid = server.getId();

        			if (oid.compareTo(nid) < 0) {
        				// Server id is even larger than us. Means old record is deleted
        				// in server. Proceeded one local.
        				dao.delete(local);
        				l++;
        			} else if (oid.compareTo(nid) > 0) {
        				// Server id is smaller than us. Found new item and add this.
        				// Proceed one server.
        				if (mDatabaseSyncerListener != null) {
        					mDatabaseSyncerListener.willUpdateObject(null, (DatabaseModel) servers[s]);
        				}
        				dao.createOrUpdate(server);
        				if (mDatabaseSyncerListener != null) {
        					mDatabaseSyncerListener.didUpdateObject(null, (DatabaseModel) servers[s]);
        				}
        				result.add(server);
        				s++;
        			} else {
        				// Id is the same. Do the update. Process one server and one
        				// local.

        				if (cachedLocalFields == null
        						|| cachedLocalIfNullFields == null) {
        					cachedLocalFields = new Field[0];
        					cachedLocalIfNullFields = new Field[0];
        					ArrayList<Field> localFields = new ArrayList<Field>();
        					ArrayList<Field> localIfNullFields = new ArrayList<Field>();
        					Field[] fields = server.getClass().getDeclaredFields();
        					for (Field field : fields) {       						
        						field.setAccessible(true);
        						
        						// Found 'SyncLocal' with ALL SuperClass
        						SyncLocal localAnnotation = field
        								.getAnnotation(SyncLocal.class);
        						
        						boolean shouldBreakSyncLocalLoop = (localAnnotation != null);
        						boolean shouldAddField = (localAnnotation != null);
        						
        						Class<?> superClass = null;
        						while (localAnnotation == null && !shouldBreakSyncLocalLoop
        								&& !shouldAddField) {
        							
        							// Current class NULL, found super class
        							if (superClass == null) {
        								superClass = field.getDeclaringClass().getSuperclass();  
        							} else {
        								superClass = superClass.getSuperclass();
        							}
            					

        							if (superClass == null) {
        								// Super class NULL , break
        								shouldBreakSyncLocalLoop = true;
        							}
        							String fieldName = field.getName();
        							
        							try {
        								Field f = superClass.getDeclaredField(fieldName);
        								localAnnotation = f.getAnnotation(SyncLocal.class);
        								shouldAddField = (localAnnotation != null);
									} catch (Exception e) {
										// TODO: handle exception
									}
        						}      
        						
        						if (localAnnotation != null) {
        							localFields.add(field);
        						}

        						// Found 'SyncLocalIfNull' with ALL SuperClass
        						SyncLocalIfNull localIfNullAnnotation = field
        								.getAnnotation(SyncLocalIfNull.class);
        						
        						boolean shouldBreakSyncLocalIfNullLoop = (localIfNullAnnotation != null);
        						shouldAddField = (localIfNullAnnotation != null);        						
        						
        						while (localIfNullAnnotation == null && !shouldBreakSyncLocalIfNullLoop
        								&& !shouldAddField) {
        							
        							// Current class NULL, found super class
        							if (superClass == null) {
        								superClass = field.getDeclaringClass().getSuperclass();  
        							} else {
        								superClass = superClass.getSuperclass();
        							}
            					
        							if (superClass == null) {
        								// Super class NULL , break
        								shouldBreakSyncLocalIfNullLoop = true;
        							}
        							String fieldName = field.getName();

        							try {
        								Field f = superClass.getDeclaredField(fieldName);
        								localIfNullAnnotation = f.getAnnotation(SyncLocalIfNull.class);
        								shouldAddField = (localIfNullAnnotation != null); 
									} catch (Exception e) {
										// TODO: handle exception
									}
        						}     
        						
        						if (localIfNullAnnotation != null) {
        							localIfNullFields.add(field);
        						}
        					}
        					cachedLocalFields = localFields.toArray(cachedLocalFields);
        					cachedLocalIfNullFields = localIfNullFields
        							.toArray(cachedLocalIfNullFields);
        				}
        				for (Field field : cachedLocalFields) {
        					Log.d("model sync", "Use local copy of " + field.getName()
        							+ " for " + local.getId());
        					field.set(server, field.get(local));
        				}
        				for (Field field : cachedLocalIfNullFields) {
        					if (field.get(server) == null) {
        						Log.d("model sync",
        								"Useed local copy of " + field.getName()
        										+ " for " + local.getId() + " as null");
        						field.set(server, field.get(local));
        					}
        				}

        				if (mDatabaseSyncerListener != null) {
        					mDatabaseSyncerListener.willUpdateObject(local, server);
        				}
        				dao.update(server);
        				if (mDatabaseSyncerListener != null) {
        					mDatabaseSyncerListener.didUpdateObject(local, server);
        				}
        				result.add(server);
        				l++;
        				s++;
        			}
        		}
        		while (l < locals.length) {
        			// Still have unprocessed old stuff. Remove them
        			dao.delete((T) locals[l]);
        			l++;
        		}
        		while (s < servers.length) {
        			// Still have unprocessed new stuff. ADd them
        			if (mDatabaseSyncerListener != null) {
    					mDatabaseSyncerListener.willUpdateObject(null, (DatabaseModel) servers[s]);
    				}
        			dao.createOrUpdate((T) servers[s]);
        			if (mDatabaseSyncerListener != null) {
    					mDatabaseSyncerListener.didUpdateObject(null, (DatabaseModel) servers[s]);
    				}
        			result.add((T) servers[s]);
        			s++;
        		}
				return null;
            }
        });
		return result;
	}
	
	public HashSet<T> sync(final Dao<T, String> dao, final List<T> localList, final List<T> serverList)
			throws Exception, SQLException, IllegalArgumentException,
			IllegalAccessException {
		return sync(dao, localList, serverList, null);
	}
/*
	public void sync(Dao<T, String> dao, T local, T server)
			throws SQLException, IllegalArgumentException,
			IllegalAccessException {

		Field[] cachedLocalFields = null;
		Field[] cachedLocalIfNullFields = null;
		
		if (local == null) {
			// Local record not found
			// update from Server
			dao.createOrUpdate(server);
			
		} else {
			// local , server should not be null
			// Id is the same. Do the update. Process one server and one
			// local.

			if (cachedLocalFields == null
					|| cachedLocalIfNullFields == null) {
				cachedLocalFields = new Field[0];
				cachedLocalIfNullFields = new Field[0];
				ArrayList<Field> localFields = new ArrayList<Field>();
				ArrayList<Field> localIfNullFields = new ArrayList<Field>();
				Field[] fields = server.getClass().getDeclaredFields();
				for (Field field : fields) {
					field.setAccessible(true);
					SyncLocal localAnnotation = field
							.getAnnotation(SyncLocal.class);
					if (localAnnotation != null) {
						localFields.add(field);
					}
					SyncLocalIfNull localIfNullAnnotation = field
							.getAnnotation(SyncLocalIfNull.class);
					if (localIfNullAnnotation != null) {
						localIfNullFields.add(field);
					}
				}
				cachedLocalFields = localFields.toArray(cachedLocalFields);
				cachedLocalIfNullFields = localIfNullFields
						.toArray(cachedLocalIfNullFields);
			}
			for (Field field : cachedLocalFields) {
				Log.d("model sync", "Use local copy of " + field.getName()
						+ " for " + local.getId());
				field.set(server, field.get(local));
			}
			for (Field field : cachedLocalIfNullFields) {
				if (field.get(server) == null) {
					Log.d("model sync",
							"Useed local copy of " + field.getName()
							+ " for " + local.getId() + " as null");
					field.set(server, field.get(local));
				}
			}

			dao.update(server);

		}
	}
	
	@SuppressWarnings("unchecked")
	public void createOrUpdate(Dao<T, String> dao, List<T> localList, List<T> serverList)
			throws SQLException, IllegalArgumentException,
			IllegalAccessException {
		Comparator<Object> idComparator = new Comparator<Object>() {
			@Override
			public int compare(Object olhs, Object orhs) {
				T lhs = (T) olhs;
				T rhs = (T) orhs;
				return lhs.getId().compareTo(rhs.getId());
			}
		};

		Object[] locals = localList.toArray();
		Arrays.sort(locals, idComparator);

		Object[] servers = serverList.toArray();
		Arrays.sort(servers, idComparator);

		Field[] cachedLocalFields = null;
		Field[] cachedLocalIfNullFields = null;

		int l = 0, s = 0;
		while (l < locals.length && s < servers.length) {
			T local = (T) locals[l];
			T server = (T) servers[s];

			String oid = local.getId();
			String nid = server.getId();

			if (oid.compareTo(nid) < 0) {
				// Server id is even larger than us. Means old record is deleted
				// in server. Proceeded one local.
				
				l++;
			} else if (oid.compareTo(nid) > 0) {
				// Server id is smaller than us. Found new item and add this.
				// Proceed one server.
				dao.createOrUpdate(server);
				s++;
			} else {
				// Id is the same. Do the update. Process one server and one
				// local.

				if (cachedLocalFields == null
						|| cachedLocalIfNullFields == null) {
					cachedLocalFields = new Field[0];
					cachedLocalIfNullFields = new Field[0];
					ArrayList<Field> localFields = new ArrayList<Field>();
					ArrayList<Field> localIfNullFields = new ArrayList<Field>();
					Field[] fields = server.getClass().getDeclaredFields();
					for (Field field : fields) {
						field.setAccessible(true);
						SyncLocal localAnnotation = field
								.getAnnotation(SyncLocal.class);
						if (localAnnotation != null) {
							localFields.add(field);
						}
						SyncLocalIfNull localIfNullAnnotation = field
								.getAnnotation(SyncLocalIfNull.class);
						if (localIfNullAnnotation != null) {
							localIfNullFields.add(field);
						}
					}
					cachedLocalFields = localFields.toArray(cachedLocalFields);
					cachedLocalIfNullFields = localIfNullFields
							.toArray(cachedLocalIfNullFields);
				}
				for (Field field : cachedLocalFields) {
					Log.d("model sync", "Use local copy of " + field.getName()
							+ " for " + local.getId());
					field.set(server, field.get(local));
				}
				for (Field field : cachedLocalIfNullFields) {
					if (field.get(server) == null) {
						Log.d("model sync",
								"Useed local copy of " + field.getName()
										+ " for " + local.getId() + " as null");
						field.set(server, field.get(local));
					}
				}

				dao.update(server);
				l++;
				s++;
			}
		}
		while (l < locals.length) {
			// Still have unprocessed old stuff. Remove them
			l++;
		}
		while (s < servers.length) {
			// Still have unprocessed new stuff. ADd them
			dao.createOrUpdate((T) servers[s]);
			s++;
		}
	}
	*/
}
