package hk.com.playmore.syncframework.util;

public interface DatabaseModel {
	public String getId();	
	public long getUserReadTime();
	public void setUserReadTime(long userReadTime);
}
