package hk.com.playmore.util;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

import com.google.bitmapfun.util.AsyncTask;
import com.google.bitmapfun.util.ImageFetcher;

public class PMImageFetcher extends ImageFetcher {
	
	private Context context;

	public PMImageFetcher(Context context, int imageSize) {
		super(context, imageSize);
		setContext(context);
	}

	public PMImageFetcher(Context context, int imageWidth, int imageHeight) {
		super(context, imageWidth, imageHeight);
		setContext(context);
	}
	
	public void loadImage(Object data, ImageView imageView, File storeFile) {
		loadImage(data, imageView, storeFile, 100);
	}
	
	public void loadImage(Object data, ImageView imageView, File storeFile, int storeFilemaxEdgeLength) {
		if (data == null) {
            return;
        }
		mLoadingBitmap = null;

        BitmapDrawable value = null;

        if (mImageCache != null) {
            value = mImageCache.getBitmapFromMemCache(String.valueOf(data));
        }
        
        if (value == null && storeFile.exists()) {        	
        	Bitmap bm = ImageUtils.getResizedBitmapFromFile(storeFile, storeFilemaxEdgeLength);
//        	Bitmap bm = BitmapFactory.decodeFile(storeFile.getAbsolutePath());
        	
        	if (bm != null) {
//        		value = new RecyclingBitmapDrawable(mResources, bm);
//        		mLoadingBitmap = bm;
        		/*if (imageView instanceof RecyclingImageView) {
            		RecyclingBitmapDrawable d = new RecyclingBitmapDrawable(mResources, bm);
            		imageView.setImageDrawable(d);
            	} else {
                	imageView.setImageBitmap(bm);
            	}*/
        	}        	
        }
        
        if (value != null) {
            // Bitmap found in memory cache
            imageView.setImageDrawable(value);
            
        } else if (cancelPotentialWork(data, imageView)) {
        	final TABitmapWorkerTask task = new TABitmapWorkerTask(imageView, storeFile.getAbsolutePath());
            final AsyncDrawable asyncDrawable =
                    new AsyncDrawable(mResources, mLoadingBitmap, task);
            imageView.setImageDrawable(asyncDrawable);

            // NOTE: This uses a custom version of AsyncTask that has been pulled from the
            // framework and slightly modified. Refer to the docs at the top of the class
            // for more info on what was changed.
            task.executeOnExecutor(AsyncTask.DUAL_THREAD_EXECUTOR, data);
        }
	}
	
	@Override
	protected Bitmap processBitmap(Object data) {
		Bitmap bm = super.processBitmap(data);
		
		return bm;
	}
	
	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	private class TABitmapWorkerTask extends BitmapWorkerTask {
		
		private String mStoreFilePath;

		public TABitmapWorkerTask(ImageView imageView, String storeFilePath) {
			super(imageView);
			this.mStoreFilePath = storeFilePath;
		}
		
		@Override
		protected Bitmap willProcessBitmap(Object obj) {
			Bitmap downloadBm = super.willProcessBitmap(obj);
			if (downloadBm != null) {
				CompressFormat compressFormat = CompressFormat.JPEG;
				FileOutputStream out = null;
				try {
					String extension = StringUtils.getPathExtension(mStoreFilePath);
					if (extension.contains("png")) {
						compressFormat = CompressFormat.PNG;
					}
					out = new FileOutputStream(mStoreFilePath);
					downloadBm.compress(compressFormat, 100, out);
					
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (out != null) {
					try {
						out.flush();
						out.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			return downloadBm;
		}
		
	}
}
