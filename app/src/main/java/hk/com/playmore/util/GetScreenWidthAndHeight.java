package hk.com.playmore.util;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.view.Display;
import android.view.WindowManager;

public class GetScreenWidthAndHeight {
	int width, height;
	
	public GetScreenWidthAndHeight(){
		this.width = 0;
		this.height = 0;
	}
	
	@SuppressWarnings("deprecation")
	public void getFromAservice(Service serivce){
		WindowManager window = (WindowManager) serivce.getSystemService(Context.WINDOW_SERVICE); 
	    Display display = window.getDefaultDisplay();
	    this.width = display.getWidth();
	    this.height = display.getHeight();
	}
	
	@SuppressWarnings("deprecation")
	public void getFromActivity(Activity activity){
		Display display = activity.getWindowManager().getDefaultDisplay();
		this.width = display.getWidth();
	    this.height = display.getHeight();
	}
}
