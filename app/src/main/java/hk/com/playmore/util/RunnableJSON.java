package hk.com.playmore.util;
import hk.com.playmore.syncframework.util.RunnableArgument;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class RunnableJSON implements RunnableArgument {
	@Override
	final public void run(String object) {
		try {
			JSONObject jsonObject = new JSONObject(object);
			run(jsonObject);
		} catch (JSONException e) {
			try {
				JSONArray array = new JSONArray(object);
				run(array);
				
			} catch (JSONException e1) {
				e1.printStackTrace();
				onJSONException(e1);
			}
		}
	}

	public abstract void run(JSONObject object);
	public abstract void run(JSONArray array);
	public abstract void onJSONException(JSONException e);
}
