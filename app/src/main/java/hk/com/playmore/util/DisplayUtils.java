package hk.com.playmore.util;
import android.app.Activity;
import android.util.DisplayMetrics;

public class DisplayUtils {

	public static DisplayMetrics getScreenDisplayMetrics(Activity a) {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		a.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		return displaymetrics;
	}
}
