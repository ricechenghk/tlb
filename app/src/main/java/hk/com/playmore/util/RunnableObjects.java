package hk.com.playmore.util;
import hk.com.playmore.syncframework.util.RunnableArgument;

import java.util.List;

public abstract class RunnableObjects implements RunnableArgument {
	
	@Override
	final public void run(String object) {
		onFailure(object);
	}

	public abstract void onSuccess(List<?> objects);
	public abstract void onFailure(String errorMessage);
}
