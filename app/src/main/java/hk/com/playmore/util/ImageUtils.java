package hk.com.playmore.util;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.util.Base64;

public class ImageUtils {
    /**
     * Decodes the supplied base 64 string into a bitmap.
     * 
     * @param base64  the string
     * @return        the bitmap, or null if cannot decode
     */
    public static Bitmap Base64StringToBitmap(String base64) {
        if (null == base64) {
            throw new IllegalArgumentException("Must supply the base 64 string");
        }
        
        try {
        	byte[] imageData = Base64.decode(base64, Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} catch (OutOfMemoryError e) {
			System.gc();
			return null;
		}
    }
    
    /**
     * Encodes the supplied bitmap in base-64 encoding.
     *  
     * @param bm  the bitmap
     * @return    the base-64 representation
     */
    public static String BitmapToBase64String(Bitmap bm) {
        if (null == bm) {
//            throw new IllegalArgumentException("Must supply the bitmap");
        	return "";
        }

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, os);
        
        return Base64.encodeToString(os.toByteArray(), Base64.DEFAULT);
    }
 
    public static Bitmap scaleBitmap(Bitmap bitmap, int w, int h) {
        if (null == bitmap) {
            throw new IllegalArgumentException("Must supply the bitmap");
        }

        int bW = bitmap.getWidth();
        int bH = bitmap.getHeight();

        float scaleWidth = (float)w / bW;
        float scaleHeight = (float)h / bH;
        Matrix m = new Matrix();
        m.postScale(scaleWidth, scaleHeight);
        Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0, 0, bW, bH, m, false);
        return newBitmap;
    }
    
    public static Bitmap mergeBitmaps(List<Bitmap> bitmaps, int width, int height) {
        if (null == bitmaps || 0 == bitmaps.size()) {
            throw new IllegalArgumentException("Must supply at least one bitmap");
        }

        Bitmap bmp = bitmaps.get(0);
        Bitmap bitmap = Bitmap.createBitmap(width, height, bmp.getConfig());
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(Color.argb(255, 228, 231, 230));
        
        int innerPadding = 6;
        int outerPadding = 12;
        
        Bitmap tmpBmp;
        switch (bitmaps.size()) {
            case 1:
                tmpBmp = scaleBitmap(bitmaps.get(0), width - 24, height - 24);
                canvas.drawBitmap(tmpBmp, 12, 12, null);
                tmpBmp.recycle();
                break;
            
            case 2: {
                int imgWidth = (width - (outerPadding << 1) - innerPadding) >> 1;
                int verticalSpace = (height - imgWidth) >> 1;
                
                tmpBmp = scaleBitmap(bitmaps.get(0), imgWidth, imgWidth);
                canvas.drawBitmap(tmpBmp, outerPadding, verticalSpace, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap(bitmaps.get(1), width / 2 - 12 - 3, height / 2 - 12 - 3);
                canvas.drawBitmap(tmpBmp, outerPadding + imgWidth + innerPadding, verticalSpace, null);
                tmpBmp.recycle();
            }
                break;
                
            case 3:
                tmpBmp = scaleBitmap(bitmaps.get(0), width / 2 - 12 - 3, height / 2 - 12 - 3);
                canvas.drawBitmap(tmpBmp, 12, 12, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap(bitmaps.get(1), width / 2 - 12 - 3, height / 2 - 12 - 3);
                canvas.drawBitmap(tmpBmp, width/ 2 + 3, 12, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap(bitmaps.get(2), width / 2 - 12 - 3, height / 2 - 12 - 3);
                canvas.drawBitmap(tmpBmp, width / 4 + 3, height / 2 + 3, null);
                tmpBmp.recycle();
                break;
                
            case 4:
                tmpBmp = scaleBitmap (bitmaps.get(0), width / 2 -12 - 3, height / 2 -12 - 3);
                canvas.drawBitmap(tmpBmp, 12, 12, null);
                tmpBmp.recycle();

                tmpBmp = scaleBitmap (bitmaps.get(1), width / 2 -12 - 3, height / 2 -12 - 3);
                canvas.drawBitmap(tmpBmp, width / 2 + 3, 12, null);
                tmpBmp.recycle();

                tmpBmp = scaleBitmap (bitmaps.get(2), width / 2 -12 - 3, height / 2 -12 - 3);
                canvas.drawBitmap(tmpBmp, 12, height / 2 + 3, null);
                tmpBmp.recycle();

                tmpBmp = scaleBitmap (bitmaps.get(3), width / 2 -12 - 3, height / 2 -12 - 3);
                canvas.drawBitmap(tmpBmp, width/ 2 + 3, height / 2 + 3, null);
                tmpBmp.recycle();
                break;
                
            case 5: {
                int imgSize = (width - (outerPadding << 1) - (innerPadding << 1)) / 3;
                int outerPaddingY = (height - (imgSize << 1) - innerPadding) >> 1;
                
                tmpBmp = scaleBitmap (bitmaps.get(0), imgSize, imgSize);
                canvas.drawBitmap(tmpBmp, outerPadding, outerPaddingY, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(1), imgSize, imgSize);
                canvas.drawBitmap(tmpBmp, imgSize + outerPadding + innerPadding, outerPaddingY, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(2), imgSize, imgSize);
                canvas.drawBitmap(tmpBmp, width - outerPadding - imgSize, outerPaddingY, null);
                tmpBmp.recycle();

                int outerSpacing = (width - (imgSize << 1) - innerPadding) >> 1;
                
                tmpBmp = scaleBitmap (bitmaps.get(3), imgSize, imgSize);
                canvas.drawBitmap(tmpBmp, outerSpacing, outerPaddingY + imgSize + innerPadding, null);
                tmpBmp.recycle();

                tmpBmp = scaleBitmap (bitmaps.get(4), imgSize, imgSize);
                canvas.drawBitmap(tmpBmp, width - outerSpacing - imgSize, outerPaddingY + imgSize + innerPadding, null);
                tmpBmp.recycle();
            }
                break;

            case 6: {
                int imgSize = (width - (outerPadding << 1) - (innerPadding << 1)) / 3;
                int outerPaddingY = (height - (imgSize << 1) - innerPadding) >> 1;
                
                tmpBmp = scaleBitmap (bitmaps.get(0), imgSize, imgSize);
                canvas.drawBitmap(tmpBmp, outerPadding, outerPaddingY, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(1), imgSize, imgSize);
                canvas.drawBitmap(tmpBmp, imgSize + outerPadding + innerPadding, outerPaddingY, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(2), imgSize, imgSize);
                canvas.drawBitmap(tmpBmp, width - outerPadding - imgSize, outerPaddingY, null);
                tmpBmp.recycle();

                tmpBmp = scaleBitmap (bitmaps.get(3), imgSize, imgSize);
                canvas.drawBitmap(tmpBmp, outerPadding, outerPaddingY + imgSize + innerPadding, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(4), imgSize, imgSize);
                canvas.drawBitmap(tmpBmp, imgSize + outerPadding + innerPadding, outerPaddingY + imgSize + innerPadding, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(5), imgSize, imgSize);
                canvas.drawBitmap(tmpBmp, width - outerPadding - imgSize, outerPaddingY + imgSize+ innerPadding, null);
                tmpBmp.recycle();
            }
                break;

            case 7: {
                int imgWidth = (width - (outerPadding << 1) - (innerPadding << 1)) / 3;
                int imgHeight = (height - (outerPadding << 1) - (innerPadding << 1)) / 3;
                
                tmpBmp = scaleBitmap (bitmaps.get(0), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, outerPadding, outerPadding, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(1), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, imgWidth + outerPadding + innerPadding, outerPadding, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(2), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, width - outerPadding - imgWidth, outerPadding, null);
                tmpBmp.recycle();

                tmpBmp = scaleBitmap (bitmaps.get(3), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, outerPadding, outerPadding + imgHeight + innerPadding, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(4), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, imgWidth + outerPadding + innerPadding, outerPadding + imgHeight + innerPadding, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(5), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, width - outerPadding - imgWidth, outerPadding + imgHeight + innerPadding, null);
                tmpBmp.recycle();

                tmpBmp = scaleBitmap (bitmaps.get(6), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, (width - imgWidth) >> 1, height - outerPadding - imgHeight, null);
                tmpBmp.recycle();
            }
                break;
                
            case 8: {
                int imgWidth = (width - (outerPadding << 1) - (innerPadding << 1)) / 3;
                int imgHeight = (height - (outerPadding << 1) - (innerPadding << 1)) / 3;
                
                tmpBmp = scaleBitmap (bitmaps.get(0), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, outerPadding, outerPadding, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(1), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, imgWidth + outerPadding + innerPadding, outerPadding, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(2), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, width - outerPadding - imgWidth, outerPadding, null);
                tmpBmp.recycle();

                tmpBmp = scaleBitmap (bitmaps.get(3), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, outerPadding, outerPadding + imgHeight + innerPadding, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(4), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, imgWidth + outerPadding + innerPadding, outerPadding + imgHeight + innerPadding, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(5), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, width - outerPadding - imgWidth, outerPadding + imgHeight + innerPadding, null);
                tmpBmp.recycle();

                int outerSpacing = (width - (imgWidth << 1) - innerPadding) >> 1;
                
                tmpBmp = scaleBitmap (bitmaps.get(6), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, outerSpacing, height - outerPadding - imgHeight, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(7), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, width - imgWidth - outerSpacing, height - outerPadding - imgHeight, null);
                tmpBmp.recycle();
            }
                break;

            case 9: {
                int imgWidth = (width - (outerPadding << 1) - (innerPadding << 1)) / 3;
                int imgHeight = (height - (outerPadding << 1) - (innerPadding << 1)) / 3;
                
                tmpBmp = scaleBitmap (bitmaps.get(0), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, outerPadding, outerPadding, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(1), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, imgWidth + outerPadding + innerPadding, outerPadding, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(2), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, width - outerPadding - imgWidth, outerPadding, null);
                tmpBmp.recycle();

                tmpBmp = scaleBitmap (bitmaps.get(3), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, outerPadding, outerPadding + imgHeight + innerPadding, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(4), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, imgWidth + outerPadding + innerPadding, outerPadding + imgHeight + innerPadding, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(5), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, width - outerPadding - imgWidth, outerPadding + imgHeight + innerPadding, null);
                tmpBmp.recycle();

                tmpBmp = scaleBitmap (bitmaps.get(6), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, outerPadding, height - outerPadding - imgHeight, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(7), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, imgWidth + outerPadding + innerPadding, height - outerPadding - imgHeight, null);
                tmpBmp.recycle();
                
                tmpBmp = scaleBitmap (bitmaps.get(8), imgWidth, imgHeight);
                canvas.drawBitmap(tmpBmp, width - outerPadding - imgWidth, height - outerPadding - imgHeight, null);
                tmpBmp.recycle();
            }
                break;
                
            default:
                break;
        }
        
        return bitmap;
    }
    
    public static Bitmap getResizedBitmapFromUri(ContentResolver resolver, Uri uri, int maxEdgeLength) {
        if (null == resolver) {
            throw new IllegalArgumentException("Must supply the content resolver to resolve the uri");
        }
        if (null == uri) {
            throw new IllegalArgumentException("Must supply the uri that contains the bitmap to be resized");
        }
        if (maxEdgeLength <= 0) {
            throw new IllegalArgumentException("Must supply a positive maximum length of the edge of the resized bitmap");
        }
        
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        
        InputStream is = null;
        try {
            is = resolver.openInputStream(uri);
            BitmapFactory.decodeStream(is, null, options);
        } catch (FileNotFoundException e) {
            return null;
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (Exception e) {
                    // ignored
                }
                is = null;
            }
        }
        
        int width = options.outWidth;
        int height = options.outHeight;
        int inSampleSize = 1;
        
        if (width > maxEdgeLength || height > maxEdgeLength) {
            int widthRatio = Math.round((float) width / (float) maxEdgeLength);
            int heightRatio = Math.round((float) height / (float) maxEdgeLength);
            
            inSampleSize = widthRatio > heightRatio ? widthRatio : heightRatio;
        }
        
        options.inJustDecodeBounds = false;
        options.inSampleSize = inSampleSize;
        
        Bitmap result = null;
        try {
            is = resolver.openInputStream(uri);
            result = BitmapFactory.decodeStream(is, null, options);
        } catch (FileNotFoundException e) {
            return null;
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (Exception e) {
                    // ignored
                }
            }
        }
        
        return result;
    }
    
    public static Bitmap getResizedBitmapFromFile(File file, int maxEdgeLength) {
        if (null == file) {
            throw new IllegalArgumentException("Must supply the File that contains the path to be resized");
        }
        if (maxEdgeLength <= 0) {
            throw new IllegalArgumentException("Must supply a positive maximum length of the edge of the resized bitmap");
        }
        
     // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);

        int width = options.outWidth;
        int height = options.outHeight;
        int inSampleSize = 1;
        
        if (width > maxEdgeLength || height > maxEdgeLength) {
            int widthRatio = Math.round((float) width / (float) maxEdgeLength);
            int heightRatio = Math.round((float) height / (float) maxEdgeLength);
            
            inSampleSize = widthRatio > heightRatio ? widthRatio : heightRatio;
        }
        
        options.inJustDecodeBounds = false;
        options.inSampleSize = inSampleSize;

        try {
        	// Decode bitmap with inSampleSize set
            Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
            return bm;
        } catch (OutOfMemoryError e) {
        	e.printStackTrace();
        	System.gc();
        	options.inSampleSize = 2;
            return BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        }
    }

    public static Bitmap getVideoFrame(File file) {
    	MediaMetadataRetriever retriever = new MediaMetadataRetriever();
    	try {
    		retriever.setDataSource(file.getAbsolutePath());
    		return retriever.getFrameAtTime();
    	} catch (IllegalArgumentException ex) {
    		ex.printStackTrace();
    	} catch (RuntimeException ex) {
    		ex.printStackTrace();
    	} finally {
    		try {
    			retriever.release();
    		} catch (RuntimeException ex) {
        		ex.printStackTrace();
    		}
    	}
    	return null;
    }
}
