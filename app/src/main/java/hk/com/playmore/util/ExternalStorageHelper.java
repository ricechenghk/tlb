package hk.com.playmore.util;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import org.xeustechnologies.jtar.TarEntry;
import org.xeustechnologies.jtar.TarInputStream;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ExternalStorageHelper {
    static int cacheIndex = 0; // 1-100
    public static String QUIZ_CATEGORY = ".quiz_category";
    final String TM_CHATROOM_AUDIO_TEMP_PATH = "chatroom_audio_temp";

    public static boolean isExternalStorageMounted() {
        return Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED);
    }

    public String getStoragePath(String path, Context ctx) {
        if (isExternalStorageMounted()) {
            return getExternalStoragePathByContext(path, ctx);
        } else {
            return getInternalStoragePathByContext(path, ctx);
        }
    }

    private String getInternalStoragePathByContext(String path, Context context) {
        File dirPath = context.getDir(path, Context.MODE_PRIVATE);
        if (!dirPath.exists()) {
            dirPath.mkdir();
        }
        return dirPath.getAbsolutePath();
    }

    public File getCacheDir(Context context) {
        return isExternalStorageMounted() ? getExternalCacheDir(context) : getCacheDirByContext(context);
    }

    public static File getExternalCacheDir(Context context) {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) ?
                context.getExternalFilesDir(null) :
                context.getFilesDir();
    }

    private static File getCacheDirByContext(Context context) {
        return context.getFilesDir();
    }


    private String getExternalStoragePathByContext(String path, Context context) {
        File dirPath = new File(getCacheDir(context).getAbsolutePath() + File.separator + path);

        if (!dirPath.exists()) {
            dirPath.mkdir();
        }
        return dirPath.getAbsolutePath();
    }

    public synchronized int getCacheIndex() {

        if (cacheIndex == 100) {
            cacheIndex = 0;
        }
        cacheIndex += 1;
        return cacheIndex;
    }

    public boolean isExternalStorageAvailable() {
        boolean mExternalStorageAvailable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            //  to know is we can neither read nor write
            mExternalStorageAvailable = false;
        }
        return mExternalStorageAvailable;
    }

    public boolean isExternalStorageWriteable() {
        boolean mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageWriteable = false;
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            //  to know is we can neither read nor write
            mExternalStorageWriteable = false;
        }
        return mExternalStorageWriteable;
    }

    private static String CACHE_PATH_FILE = "newsletter_download_cache";
    private static String ZIP_FILE_EXTENSION = ".zip";
    private static String TM_NEWSLETTER_PATH = "tm_newsletter";
    private static String TM_MESSAGING_PATH = "tm_messaging";
    /*private static String HIDDEN_IMAGE_PATH = ".ta_img";
    private static String HIDDEN_PDF_PATH = ".ta_pdf";
    private static String HIDDEN_MKT_ZONE_PATH = ".ta_mkt_zone";*/


    public String getHiddenCacheFilePath(Context context) {
        String parentPath = getCacheDir(context).getAbsolutePath() + File.separator + TM_NEWSLETTER_PATH + File.separator +
                "tm_newsletter_cache" + File.separator;
        File parentFile = new File(parentPath);
        parentFile.mkdirs();
        return parentPath;
    }

    public File getHiddenCacheFile(Context context) {
        String parentPath = getCacheDir(context).getAbsolutePath() + File.separator + TM_NEWSLETTER_PATH + File.separator +
                "tm_newsletter_cache" + File.separator;
        File parentFile = new File(parentPath);
        parentFile.mkdirs();
        String cacheFileName = CACHE_PATH_FILE + "_" + getCacheIndex();
        Log.e("cache file name", cacheFileName);
        return new File(parentPath, cacheFileName);
    }

    public File getHiddenFixedCacheFile(Context context) {
        String parentPath = getCacheDir(context).getAbsolutePath() + File.separator + TM_NEWSLETTER_PATH + File.separator +
                "tm_newsletter_cache" + File.separator;
        File parentFile = new File(parentPath);
        parentFile.mkdirs();
        String cacheFileName = CACHE_PATH_FILE;
        Log.e("cache file name", cacheFileName);
        return new File(parentPath, cacheFileName);
    }

    public void deleteRecursive(Context context, String newsletterId) {
        String parentPath = getCacheDir(context).getAbsolutePath() + File.separator + TM_NEWSLETTER_PATH + File.separator +
                "tm_newsletter_layout_" + newsletterId + File.separator;
        File parentFile = new File(parentPath);
        if (parentFile.isDirectory())
            for (File child : parentFile.listFiles())
                deleteRecursive(child);

        parentFile.delete();
    }

    public String getHiddenNewsletterPath(Context context, String newsletterId) {
        String parentPath = getCacheDir(context).getAbsolutePath() + File.separator + TM_NEWSLETTER_PATH + File.separator +
                "tm_newsletter_layout_" + newsletterId + File.separator;
        File parentFile = new File(parentPath);
        parentFile.mkdirs();
        return parentPath;
    }

    public String getMessenderTempFolderPath(Context context) {
        String parentPath = getCacheDir(context).getAbsolutePath() + File.separator +
                "tm_messenger" + File.separator;
        File parentFile = new File(parentPath);
        parentFile.mkdirs();
        return parentPath;
    }

    public String getHiddenParentTempPath(Context context) {
        String parentPath = getCacheDir(context).getAbsolutePath() + File.separator + TM_NEWSLETTER_PATH + File.separator +
                "tm_pdf" + File.separator;
        File parentFile = new File(parentPath);
        parentFile.mkdirs();
        return parentPath;
    }

    public String getHiddenPdfTempFileName(Context context) {
        return getHiddenTempFileName(context, "pdf");
    }

    public String getHiddenTempFileName(Context context, String fileExtension) {
        String fullPath = getHiddenParentTempPath(context) + "tempPdfFile." + fileExtension;
        return fullPath;
    }

    public String getMessageDownloadOrUploadPath(Context context, String filename) {
        String parentPath = getCacheDir(context).getAbsolutePath() + File.separator + TM_MESSAGING_PATH + File.separator;
        File parentFile = new File(parentPath);
        parentFile.mkdirs();
        return parentPath + filename;
    }

    public void deletePdfTempPdfFile(Context context) {
        File file = new File(getHiddenPdfTempFileName(context));
        file.delete();
    }

    public File getHiddenNewsletterFile(Context context, String newsletterId) {
        String parentPath = getCacheDir(context).getAbsolutePath() + File.separator + TM_NEWSLETTER_PATH + File.separator +
                "tm_newsletter_layout_" + newsletterId + File.separator;
        File parentFile = new File(parentPath);
        parentFile.mkdirs();
        String fileName = newsletterId + ZIP_FILE_EXTENSION;
        return new File(parentPath, fileName);
    }

    public String getHiddenNewsletterUnzipPath(Context context, String newsletterId) {
        String parentPath = getCacheDir(context).getAbsolutePath() + File.separator + TM_NEWSLETTER_PATH + File.separator +
                "tm_newsletter_layout_" + newsletterId + File.separator + newsletterId + File.separator;
        File parentFile = new File(parentPath);
        parentFile.mkdirs();
        return parentPath;
    }

    public String getHiddenNewsletterContentFileStringPath(Context context, String newsletterId, String filename) {
        String path = getHiddenNewsletterUnzipPath(context, newsletterId) + filename;
        return path;
    }

    public String getHiddenNewsletterLayoutFileStringPath(Context context, String newsletterId) {
        return getHiddenNewsletterContentFileStringPath(context, newsletterId, "layout.json");
    }

    public String getHiddenNewsletterFileStringPath(Context context, String newsletterId) {
        String parentPath = getCacheDir(context).getAbsolutePath() + File.separator + TM_NEWSLETTER_PATH + File.separator +
                "tm_newsletter_layout_" + newsletterId + File.separator;
        File parentFile = new File(parentPath);
        parentFile.mkdirs();
        String fileName = newsletterId + ZIP_FILE_EXTENSION;
        return parentPath + fileName;
    }

    public boolean hasExternalStorageNewsletterZipFileExist(Context context, String newsletterId) {
        String parentPath = getCacheDir(context).getAbsolutePath() + File.separator + TM_NEWSLETTER_PATH + File.separator +
                "tm_newsletter_layout_" + newsletterId + File.separator;
        //File path = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        String fileName = newsletterId + ZIP_FILE_EXTENSION;
        File file = new File(parentPath, fileName);
        return file.exists();
    }

    public boolean unpackTarZip(String outPath, String zipPath)
            throws IOException {

        String tarFile = zipPath;
        String destFolder = outPath;

        // Create a TarInputStream
        TarInputStream tis = new TarInputStream(new BufferedInputStream(
                new FileInputStream(tarFile)));
        TarEntry entry;
        while ((entry = tis.getNextEntry()) != null) {
            int count;
            byte data[] = new byte[2048];

            FileOutputStream fos = new FileOutputStream(destFolder + "/"
                    + entry.getName());
            BufferedOutputStream dest = new BufferedOutputStream(fos);

            while ((count = tis.read(data)) != -1) {
                dest.write(data, 0, count);
            }

            dest.flush();
            dest.close();
        }

        tis.close();

        return true;
    }

    public void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

    public String getAudioTempPath(Context context) {
        String parentPath = getCacheDir(context).getAbsolutePath() + File.separator + TM_CHATROOM_AUDIO_TEMP_PATH + File.separator +
                "tm_pdf" + File.separator;
        File parentFile = new File(parentPath);
        parentFile.mkdirs();
        return parentPath;
    }

    public String getAudioTempFileName(Context context) {
        String fullPath = getHiddenParentTempPath(context) + "tempAudoFile.pdf";
        return (new File(fullPath)).getAbsolutePath();
    }

    public void deleteAudioTempPdfFile(Context context) {
        File file = new File(getHiddenPdfTempFileName(context));
        file.delete();
    }

    public String getHiddenPdfTempPath(Context context) {
        String parentPath = getCacheDir(context).getAbsolutePath() + File.separator + TM_NEWSLETTER_PATH + File.separator +
                "tm_pdf" + File.separator;
        File parentFile = new File(parentPath);
        parentFile.mkdirs();
        return parentPath;
    }
}