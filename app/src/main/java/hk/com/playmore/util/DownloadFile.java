package hk.com.playmore.util;
import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

public class DownloadFile extends AsyncTask<String, Integer, String> {
	
	public static final String BROADCAST_ACTION = "DownloadFile.BROADCAST_ACTION";

	public static final String BROADCAST_TYPE = "DownloadFile.BROADCAST_TYPE";
	public static final String BROADCAST_ERROR = "DownloadFile.BROADCAST_ERROR";
	public static final String BROADCAST_PROGRESS = "DownloadFile.BROADCAST_PROGRESS";

	public static final String BROADCAST_STATE = "DownloadFile.BROADCAST_STATE";
	public static final String STATE_DOWNLOADING = "DownloadFile.STATE_DOWNLOADING";
	public static final String STATE_FINISH = "DownloadFile.STATE_FINISH";
	
	private String mUrlToDownload;
	private String mBroadcastType;
	private String mStorePath;
	private Context mCtx;
	
	private Runnable onFinishRunnable;
	
	public DownloadFile(Context ctx, String url, String storePath, String broadcast) {
		super();
		this.mCtx = ctx;
		this.mUrlToDownload = url;
		this.mBroadcastType = broadcast;
		this.mStorePath = storePath;
	}

	@Override
	protected String doInBackground(String... infos) {
		String urlToDownload = mUrlToDownload;		
		String storePath = mStorePath;

//		SharedPreferences prefs = mCtx.getSharedPreferences(AppDataSource.Prefs.TARGUS_PREFS, 0);
//		String userId = prefs.getString(AppDataSource.Prefs.USER_ID, null);		
//		String userToken = prefs.getString(AppDataSource.Prefs.USER_TOKEN, null);
		
		try {
			URL url = new URL(urlToDownload);
			URLConnection connection = url.openConnection();
//			connection.setRequestProperty("X-APP-USER-ID", userId);
//			connection.setRequestProperty("X-APP-DEVICE-DPI", "1");
//			connection.setRequestProperty("X-APP-USER-TOKEN", userToken);
			
			connection.connect();

			int fileLength = connection.getContentLength();

			// download the file
			InputStream input = new BufferedInputStream(
					(InputStream) connection.getContent());
			OutputStream output = new FileOutputStream(storePath);

			byte data[] = new byte[1024];
			long total = 0;
			int count;
			while ((count = input.read(data)) != -1) {
				total += count;
				// publishing the progress....
				Intent intent = new Intent(mBroadcastType);
				intent.putExtra(BROADCAST_TYPE, mBroadcastType);
				intent.putExtra(BROADCAST_STATE, STATE_DOWNLOADING);
				intent.putExtra(BROADCAST_PROGRESS,
						(int) (total * 100 / fileLength));
				LocalBroadcastManager.getInstance(mCtx)
						.sendBroadcast(intent);

				output.write(data, 0, count);
			}

			output.flush();
			output.close();
			input.close();
		} catch (IOException e) {
			e.printStackTrace();

			Intent intent = new Intent(mBroadcastType);
			intent.putExtra(BROADCAST_TYPE, mBroadcastType);
			intent.putExtra(BROADCAST_STATE, STATE_FINISH);
			intent.putExtra(BROADCAST_ERROR, e.toString());

			LocalBroadcastManager.getInstance(mCtx)
					.sendBroadcast(intent);
		}

		return null;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

	}

	@Override
	protected void onProgressUpdate(Integer... progress) {
		super.onProgressUpdate(progress);
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
/*
		String newsletterId = this.mDownloadingIds;
		String path = ExternalStorageHelper.pathForTarFileStorage(
				mPrefixStorePath, mCtx,
				newsletterId);
		if (new File(path).exists()) {
			
			try {
				unpackZip(ExternalStorageHelper.pathForStorage(
						mPrefixStorePath, mCtx,
						newsletterId), path);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}*/
		
		Intent intent = new Intent(mBroadcastType);
		intent.putExtra(BROADCAST_TYPE, mBroadcastType);
		intent.putExtra(BROADCAST_STATE, STATE_FINISH);
		LocalBroadcastManager.getInstance(mCtx)
					.sendBroadcast(intent);

		if (getOnFinishRunnable() != null) {
			getOnFinishRunnable().run();
		}
		
/*
		} else {
			Intent intent = new Intent(this.mBroadcastType);
			intent.putExtra("newsletter_id", newsletterId);
			Log.e(null, "Un-Zip error ID: " + newsletterId);
			intent.putExtra("error", "Un-Zip error");
			LocalBroadcastManager.getInstance(mCtx)
					.sendBroadcast(intent);
		}*/
	}

	public Runnable getOnFinishRunnable() {
		return onFinishRunnable;
	}

	public void setOnFinishRunnable(Runnable onFinishRunnable) {
		this.onFinishRunnable = onFinishRunnable;
	}
}
