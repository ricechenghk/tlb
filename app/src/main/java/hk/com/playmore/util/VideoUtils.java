package hk.com.playmore.util;
import android.media.MediaMetadataRetriever;

public class VideoUtils {

	public static void getVideoLengths(String videoDataSource, long[] results) {
		MediaMetadataRetriever retriever = new MediaMetadataRetriever();
		retriever.setDataSource(videoDataSource);
		String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
		long timeInmillisec = Long.parseLong( time );
		long duration = timeInmillisec / 1000;
		long hours = duration / 3600;
		long minutes = (duration - hours * 3600) / 60;
		long seconds = duration - (hours * 3600 + minutes * 60);
		
		results[0] = seconds;
		results[1] = minutes;
		results[2] = hours;
		results[3] = duration;
		results[4] = timeInmillisec;
	}
}
