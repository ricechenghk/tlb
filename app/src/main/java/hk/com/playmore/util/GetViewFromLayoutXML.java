package hk.com.playmore.util;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

public class GetViewFromLayoutXML {
	Context context;
	
	public GetViewFromLayoutXML(Context context){
		this.context = context;
	}
	public View getFromResource(int layoutXML){
		LayoutInflater inflater = (LayoutInflater) this.context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
	    return inflater.inflate(layoutXML, null);
	}
}
