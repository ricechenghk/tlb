package com.transamerican.tlb;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.transamerican.tlb.model.Contact;

import java.util.ArrayList;
import java.util.List;

public class ContactListAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<Contact> contact = new ArrayList<Contact>();

    public ContactListAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void replaceWith(List<Contact> contact) {
        this.contact = contact;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return contact.size();
    }

    @Override
    public Contact getItem(int position) {
        return contact.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.list_item_contact, viewGroup, false);

            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view.findViewById(R.id.contact_title);
            viewHolder.position = (TextView) view.findViewById(R.id.contact_position);
            viewHolder.tel = (TextView) view.findViewById(R.id.contact_tel);
            viewHolder.email = (TextView) view.findViewById(R.id.contact_email);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Contact category = getItem(position);

        viewHolder.title.setText(category.getName());
        viewHolder.position.setText(category.getPosition());
        viewHolder.tel.setText("Tel: " + category.getTel());
        viewHolder.email.setText(category.getEmail());

        return view;

    }

    static class ViewHolder {
        TextView title;
        TextView position;
        TextView tel;
        TextView email;
    }
}
