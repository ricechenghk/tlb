package com.transamerican.tlb;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.WindowManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.transamerican.tlb.listener.MainSectionChangeListener;
import com.transamerican.tlb.sync.AppDataSource;
import com.transamerican.tlb.util.SimpleLoadingDialog;

import java.io.IOException;

import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class MainBaseActivity extends FragmentActivity implements MainSectionChangeListener {
    private static final String TAG = MainBaseActivity.class.getSimpleName();

    boolean isShowMainMenu = true;

    private ProgressDialog mLoadingDialog;

    //Push Related:
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    String SENDER_ID = "817344971355";
    GoogleCloudMessaging gcm;
    String regid;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public abstract int getLayoutResId();

    public abstract Class getTutorialClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(getLayoutResId());
        ButterKnife.inject(this);

        if (AppDataSource.getInstance().isShowTutorial(this)) {
            isShowMainMenu = false;
            Intent intent = new Intent(this, getTutorialClass());
            startActivityForResult(intent, 999);
        } else {
            isShowMainMenu = true;
        }

        ((MainApplication) getApplication()).sendTrackerEvent(R.string.ga_home);

        //Push
        registerPush();
    }

    protected void onResume() {
        super.onResume();

        if (AppDataSource.getInstance().isSessionExpired(this)) {
            logout();
        }

        toggleExtra();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 999) {
            isShowMainMenu = true;
        }
    }

    public void logout() {
        AppDataSource.getInstance().setLoginUser(MainBaseActivity.this, "");
        AppDataSource.getInstance().setLoginPwd(MainBaseActivity.this, "");
        AppDataSource.getInstance().setLogin(MainBaseActivity.this, false, "");
        toggleExtra();
    }

    public abstract void toggleExtra();

    public abstract void showMainMenu();

    public void replaceFragment(Fragment fr) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fr);
        fragmentTransaction.commit();
    }

    public void addFragment(Fragment fr) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fr);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public abstract void onSectionChange(boolean showExtra);

    //Push Related Method
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }

    public void registerPush() {
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(this);

            if (regid.isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
    }

    private String getRegistrationId(Context context) {
        String registrationId = AppDataSource.getInstance().getRegistrationId(context);
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing registration ID is not guaranteed to work with
        // the new app version.
        int registeredVersion = AppDataSource.getInstance().getRegistrationVersion(context);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p/>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new AsyncTask() {

            @Override
            protected Object doInBackground(Object[] params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(MainBaseActivity.this);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;
                    AppDataSource.getInstance().storeRegistrationId(MainBaseActivity.this, regid, getAppVersion(MainBaseActivity.this));
                    AppDataSource.getInstance().regPushToServer(MainBaseActivity.this);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }
        }.execute(null, null, null);
    }


    public void showLoading() {
        try {
            hideLoading();
            mLoadingDialog = SimpleLoadingDialog.show(this, R.string.loading_text);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideLoading() {
        try {
            if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
                mLoadingDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
