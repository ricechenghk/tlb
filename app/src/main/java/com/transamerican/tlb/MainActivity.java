package com.transamerican.tlb;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;

import com.transamerican.tlb.about.AboutUsFragment;
import com.transamerican.tlb.library.LoginFragment;
import com.transamerican.tlb.setting.ContactInfoFragment;
import com.transamerican.tlb.setting.SettingFragment;
import com.transamerican.tlb.sync.AppDataSource;

import butterknife.InjectView;
import butterknife.OnClick;

public class MainActivity extends MainBaseActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    @InjectView(R.id.btn_home)
    ImageView home;

    @InjectView(R.id.btn_link)
    ImageView link;

    @InjectView(R.id.btn_contact)
    ImageView contact;

    @InjectView(R.id.btn_logout)
    ImageView logout;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    public Class getTutorialClass() {
        return TutorialActivity.class;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        home.setSelected(true);
    }

    protected void onResume() {
        super.onResume();
        if (isShowMainMenu && home.isSelected()) {
            showMainMenu();
        }
    }

    protected void onPause() {
        super.onPause();
        if (isShowMainMenu && home.isSelected()) {
            replaceFragment(new Fragment());
        }
    }

    public void showMainMenu() {
        MainMenuFragment fr = new MainMenuFragment();
        fr.setMainSectionChangeListener(this);
        replaceFragment(fr);
    }

    @OnClick(R.id.btn_home)
    public void onHomeClicked() {
        if (!home.isSelected()) {
            home.setSelected(true);
            showMainMenu();
        }
    }

    @OnClick(R.id.btn_setting)
    public void onSettingClicked(View v) {

        SettingFragment fr = new SettingFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fr);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    @OnClick(R.id.btn_logout)
    public void onLogoutClicked() {

        boolean isLogin = AppDataSource.getInstance().isLogin(this);
        if (isLogin) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.logout)
                    .setMessage(R.string.logout_confirm_msg)
                    .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            logout();
                            onHomeClicked();
                        }
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .show();
        } else {

            Fragment lastFr = getSupportFragmentManager().findFragmentById(R.id.container);
            if(lastFr instanceof  LoginFragment)
            {
                return;
            }

            LoginFragment fr = new LoginFragment();
            fr.setMainSectionChangeListener(this);
            addFragment(fr);
        }

    }

    @OnClick(R.id.btn_location)
    public void onLocationClicked() {
        home.setSelected(false);
        AboutUsFragment fr = new AboutUsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("section", TitleView.INDEX_ABOUT_CONTACT);
        fr.setArguments(bundle);
        replaceFragment(fr);
    }

    @OnClick(R.id.btn_link)
    public void onLinkClicked() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.links)
                .setItems(R.array.linkTextArray, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String[] linkArray = getResources().getStringArray(R.array.linkArray);
                        String link = linkArray[which];
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(link));
                        startActivity(i);
                    }
                });
        builder.show();
    }

    @OnClick(R.id.btn_contact)
    public void onContactClicked() {
        home.setSelected(false);
        ContactInfoFragment fr = new ContactInfoFragment();
        addFragment(fr);
    }

    @Override
    public void onSectionChange(boolean showExtra) {
        //If section is changed, enable the home button
        home.setSelected(false);
        toggleExtra();
    }


    public void toggleExtra() {
        boolean showExtra = AppDataSource.getInstance().isLogin(this);
        link.setVisibility(showExtra ? View.VISIBLE : View.INVISIBLE);
        contact.setVisibility(showExtra ? View.VISIBLE : View.INVISIBLE);
        logout.setImageResource(showExtra ? R.drawable.bu_login : R.drawable.bu_logout); //icon drawable revised only
    }

}
