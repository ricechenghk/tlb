package com.transamerican.tlb.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.transamerican.tlb.model.UnderwritingGuideLibrary;

import java.sql.SQLException;
import java.util.List;

public class UnderwritingGuideLibraryDao extends BaseDaoImpl<UnderwritingGuideLibrary, String> {

    public UnderwritingGuideLibraryDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, UnderwritingGuideLibrary.class);
    }

    public List<UnderwritingGuideLibrary> queryByCategoryIds(final List<String> subCatIds) throws SQLException {

        QueryBuilder<UnderwritingGuideLibrary, String> qb = queryBuilder();
        Where<UnderwritingGuideLibrary, String> where = qb.where();

        where.in("category", subCatIds);
        qb.orderBy("sort", true);

        return qb.query();
    }

}
