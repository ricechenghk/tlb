package com.transamerican.tlb.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.transamerican.tlb.model.FieldCommCategory;

import java.sql.SQLException;
import java.util.List;

public class FieldCommCategoryDao extends BaseDaoImpl<FieldCommCategory, String> {

    public FieldCommCategoryDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, FieldCommCategory.class);
    }

    public List<FieldCommCategory> querySubCategoryByCategoryId(final String id) throws SQLException {

        QueryBuilder<FieldCommCategory, String> qb = queryBuilder();
        Where<FieldCommCategory, String> where = qb.where();

        where.eq("category", id);

        qb.orderBy("sort", true);

        return qb.query();
    }
}
