package com.transamerican.tlb.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.transamerican.tlb.model.TranswareLibrary;

import java.sql.SQLException;
import java.util.List;

public class TranswareLibraryDao extends BaseDaoImpl<TranswareLibrary, String> {

    public TranswareLibraryDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, TranswareLibrary.class);
    }

    public List<TranswareLibrary> queryByCategoryIds(final List<String> subCatIds) throws SQLException {

        QueryBuilder<TranswareLibrary, String> qb = queryBuilder();
        Where<TranswareLibrary, String> where = qb.where();

        where.in("category", subCatIds);
        qb.orderBy("sort", true);

        return qb.query();
    }

}
