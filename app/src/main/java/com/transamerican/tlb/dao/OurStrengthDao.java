package com.transamerican.tlb.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.transamerican.tlb.model.OurStrength;

import java.sql.SQLException;

public class OurStrengthDao extends BaseDaoImpl<OurStrength, String> {

    public OurStrengthDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, OurStrength.class);
    }

}
