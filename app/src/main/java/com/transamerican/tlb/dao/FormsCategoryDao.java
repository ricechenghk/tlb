package com.transamerican.tlb.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.transamerican.tlb.model.FormsCategory;

import java.sql.SQLException;
import java.util.List;

public class FormsCategoryDao extends BaseDaoImpl<FormsCategory, String> {

    public FormsCategoryDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, FormsCategory.class);
    }

    public List<FormsCategory> querySubCategoryByCategoryId(final String id) throws SQLException {

        QueryBuilder<FormsCategory, String> qb = queryBuilder();
        Where<FormsCategory, String> where = qb.where();

        where.eq("category", id);

        qb.orderBy("sort", true);

        return qb.query();
    }
}
