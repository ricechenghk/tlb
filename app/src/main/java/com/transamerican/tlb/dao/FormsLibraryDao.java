package com.transamerican.tlb.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.transamerican.tlb.model.FormsLibrary;

import java.sql.SQLException;
import java.util.List;

public class FormsLibraryDao extends BaseDaoImpl<FormsLibrary, String> {

    public FormsLibraryDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, FormsLibrary.class);
    }

    public List<FormsLibrary> queryByCategoryIds(final List<String> subCatIds) throws SQLException {

        QueryBuilder<FormsLibrary, String> qb = queryBuilder();
        Where<FormsLibrary, String> where = qb.where();

        where.in("category", subCatIds);
        qb.orderBy("sort", true);

        return qb.query();
    }

}
