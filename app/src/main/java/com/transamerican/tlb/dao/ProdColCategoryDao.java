package com.transamerican.tlb.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.transamerican.tlb.model.ProdColCategory;

import java.sql.SQLException;
import java.util.List;

public class ProdColCategoryDao extends BaseDaoImpl<ProdColCategory, String> {

    public ProdColCategoryDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, ProdColCategory.class);
    }

    public List<ProdColCategory> querySubCategoryByCategoryId(final String id) throws SQLException {

        QueryBuilder<ProdColCategory, String> qb = queryBuilder();
        Where<ProdColCategory, String> where = qb.where();

        where.eq("category", id);

        qb.orderBy("sort", true);

        return qb.query();
    }
}
