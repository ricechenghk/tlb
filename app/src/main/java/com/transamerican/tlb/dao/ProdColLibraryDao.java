package com.transamerican.tlb.dao;
import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.transamerican.tlb.model.ProdColLibrary;

public class ProdColLibraryDao extends BaseDaoImpl<ProdColLibrary, String> {

	public ProdColLibraryDao(ConnectionSource connectionSource) throws SQLException {
		super(connectionSource, ProdColLibrary.class);
	}
	
	public List<ProdColLibrary> queryByCategoryIds(final List<String> subCatIds) throws SQLException {

		QueryBuilder<ProdColLibrary, String> qb = queryBuilder();
		Where<ProdColLibrary, String> where = qb.where();
		
		where.in("category", subCatIds);
		qb.orderBy("sort", true);

		return qb.query();
	}
	
}
