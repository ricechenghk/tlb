package com.transamerican.tlb.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.transamerican.tlb.model.FieldCommLibrary;

import java.sql.SQLException;
import java.util.List;

public class FieldCommLibraryDao extends BaseDaoImpl<FieldCommLibrary, String> {

    public FieldCommLibraryDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, FieldCommLibrary.class);
    }

    public List<FieldCommLibrary> queryByCategoryIds(final List<String> subCatIds) throws SQLException {

        QueryBuilder<FieldCommLibrary, String> qb = queryBuilder();
        Where<FieldCommLibrary, String> where = qb.where();

        where.in("category", subCatIds);
        qb.orderBy("sort", true);

        return qb.query();
    }

}
