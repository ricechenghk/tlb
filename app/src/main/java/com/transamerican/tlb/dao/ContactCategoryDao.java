package com.transamerican.tlb.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.transamerican.tlb.model.ContactCategory;

import java.sql.SQLException;
import java.util.List;

public class ContactCategoryDao extends BaseDaoImpl<ContactCategory, String> {

    public ContactCategoryDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, ContactCategory.class);
    }

    public List<ContactCategory> queryForAllWithSort() throws SQLException {

        QueryBuilder<ContactCategory, String> qb = queryBuilder();
        qb.orderBy("sort", true);

        return qb.query();
    }
}
