package com.transamerican.tlb.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.transamerican.tlb.model.UnderwritingGuideCategory;

import java.sql.SQLException;
import java.util.List;

public class UnderwritingGuideCategoryDao extends BaseDaoImpl<UnderwritingGuideCategory, String> {

    public UnderwritingGuideCategoryDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, UnderwritingGuideCategory.class);
    }

    public List<UnderwritingGuideCategory> querySubCategoryByCategoryId(final String id) throws SQLException {

        QueryBuilder<UnderwritingGuideCategory, String> qb = queryBuilder();
        Where<UnderwritingGuideCategory, String> where = qb.where();

        where.eq("category", id);

        qb.orderBy("sort", true);

        return qb.query();
    }
}
