package com.transamerican.tlb.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.transamerican.tlb.model.Contact;

import java.sql.SQLException;
import java.util.List;

public class ContactDao extends BaseDaoImpl<Contact, String> {

    public ContactDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, Contact.class);
    }

    public List<Contact> queryByCategoryId(final String id) throws SQLException {

        QueryBuilder<Contact, String> qb = queryBuilder();
        Where<Contact, String> where = qb.where();

        where.eq("category", id);

//        qb.orderBy("sort", true);

        return qb.query();
    }
}
