package com.transamerican.tlb.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.transamerican.tlb.model.TranswareCategory;

import java.sql.SQLException;
import java.util.List;

public class TranswareCategoryDao extends BaseDaoImpl<TranswareCategory, String> {

    public TranswareCategoryDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, TranswareCategory.class);
    }

    public List<TranswareCategory> querySubCategoryByCategoryId(final String id) throws SQLException {

        QueryBuilder<TranswareCategory, String> qb = queryBuilder();
        Where<TranswareCategory, String> where = qb.where();

        where.eq("category", id);

        qb.orderBy("sort", true);

        return qb.query();
    }
}
