package com.transamerican.tlb.about.page;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.artifex.mupdfdemo.MuPDFActivity;
import com.transamerican.tlb.R;
import com.transamerican.tlb.model.OurStrength;
import com.transamerican.tlb.sync.AppDataSource;
import com.transamerican.tlb.sync.LibraryDownloadTask;
import com.transamerican.tlb.sync.PDFDownloadTask;
import com.transamerican.tlb.util.LibraryExternalStorageHelper;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Rice on 18/1/15.
 */
public class AboutUsStrengthPage1Fragment extends AboutUsStrengthBaseFragment {

    public static final String BROADCAST_PDF_DOWNLOAD = "BROADCAST_PDF_DOWNLOAD";

    private OurStrength item;
    private String lastDownloadLink;

    private ProgressDialog mLoadingDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_about_strength_page1, container, false);
        ButterKnife.inject(this, v);

        item = AppDataSource.getInstance().getOurStrength(getActivity());

        return v;
    }

    @Override
    public void onPageSelected() {

    }

    @Override
    public void onPageExited() {

    }

    @Override
    public int getTitle() {
        return R.string.about_us_title_our_strength;
    }

    @Override
    public int getSlider() {
        return R.drawable.about_us_slide7_1;
    }

    @Override
    public boolean isVideoExist() {
        return false;
    }

    @Override
    public void onReplayClicked() {

    }

    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                mMessageReceiver, new IntentFilter(BROADCAST_PDF_DOWNLOAD));
    }

    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(
                mMessageReceiver);
    }

    @OnClick(R.id.pdf_en)
    public void onPDFEnClicked() {
        if (item != null) {
            lastDownloadLink = item.getPdfEn();
            downloadPDF();
        }
    }

    @OnClick(R.id.pdf_zh)
    public void onPDFZhClicked() {
        if (item != null) {
            lastDownloadLink = item.getPdfZh();
            downloadPDF();
        }
    }

    @OnClick(R.id.pdf_cn)
    public void onPDFCnClicked() {
        if (item != null) {
            lastDownloadLink = item.getPdfCn();
            downloadPDF();
        }
    }

    public void downloadPDF() {
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            mLoadingDialog.dismiss();
        }
        mLoadingDialog = new ProgressDialog(getActivity());
        mLoadingDialog.setMessage(getString(R.string.loading));
        mLoadingDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mLoadingDialog.setIndeterminate(false);
        mLoadingDialog.setCancelable(false);
        mLoadingDialog.show();

        PDFDownloadTask task = new PDFDownloadTask(getActivity(), lastDownloadLink, BROADCAST_PDF_DOWNLOAD);
        task.execute();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            String state = intent.getStringExtra(LibraryDownloadTask.BROADCAST_STATE);
            if (state == LibraryDownloadTask.STATE_DOWNLOADING) {
                int progress = intent.getIntExtra(LibraryDownloadTask.BROADCAST_PROGRESS, 0);
                if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
                    mLoadingDialog.setProgress(progress);
                }
            } else if (state == LibraryDownloadTask.STATE_FINISH) {
                String error = intent.getStringExtra(LibraryDownloadTask.BROADCAST_ERROR);
                if (error == null || error.trim().isEmpty()) {
                    Log.i("", "File is downloaded Success");
                    LibraryExternalStorageHelper storageHelper = new LibraryExternalStorageHelper();
                    Uri uri = Uri.fromFile(storageHelper.getDownloadTargetFile(getActivity(), lastDownloadLink));

                    //Default Intent
//                    Intent target = new Intent(Intent.ACTION_VIEW);
//                    target.setDataAndType(uri, "application/pdf");
//                    try {
//                        startActivity(target);
//                    } catch (ActivityNotFoundException e) {
//                        e.printStackTrace();
//                        // Instruct the user to install a PDF reader here, or something
//                    }

                    try {

                        Intent pdfIntent = new Intent(getActivity(), MuPDFActivity.class);
                        pdfIntent.setAction(Intent.ACTION_VIEW);
                        pdfIntent.setData(uri);
                        startActivity(pdfIntent);

                    } catch (ActivityNotFoundException e) {
                        // Instruct the user to install a PDF reader here, or something
                        e.printStackTrace();
                    }

                } else {
                    Log.i("", "File is downloaded Fail");
                }

                if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
                    mLoadingDialog.dismiss();
                }
            }
        }
    };


}
