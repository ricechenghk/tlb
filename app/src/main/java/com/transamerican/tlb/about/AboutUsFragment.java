package com.transamerican.tlb.about;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transamerican.tlb.R;
import com.transamerican.tlb.TitleView;
import com.transamerican.tlb.about.page.AboutUsContactFragment;
import com.transamerican.tlb.about.page.AboutUsStrengthPage1Fragment;
import com.transamerican.tlb.about.page.AboutUsStrengthPage2Fragment;
import com.transamerican.tlb.about.page.AboutUsStrengthPage3Fragment;
import com.transamerican.tlb.about.page.AboutUsStrengthPage4Fragment;
import com.transamerican.tlb.about.page.AboutUsStrengthPage5Fragment;
import com.transamerican.tlb.about.page.AboutUsStrengthPage6Fragment;
import com.transamerican.tlb.about.page.AboutUsStrengthPage7Fragment;
import com.transamerican.tlb.about.page.AboutUsWhoPage1Fragment;
import com.transamerican.tlb.about.page.AboutUsWhoPage2Fragment;
import com.transamerican.tlb.about.page.AboutUsWhoPage3Fragment;
import com.transamerican.tlb.about.page.AboutUsWhoPage4Fragment;
import com.transamerican.tlb.about.page.AboutUsWhoPage5Fragment;
import com.transamerican.tlb.base.PagerFragment;
import com.transamerican.tlb.base.SectionBaseFragment;
import com.transamerican.tlb.sync.AppDataSource;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class AboutUsFragment extends SectionBaseFragment {

    public static final String TAG = AboutUsFragment.class.getName();

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        ButterKnife.inject(this, view);

        //Query PDF Data from Server
        AppDataSource.getInstance().getOurStrengthFromServer(getActivity(), this, null);

        initScreen();

        if (getArguments() != null) {
            int sectionId = getArguments().getInt("section");
            switch (sectionId) {
                case TitleView.INDEX_ABOUT_STRENGTH:
                    goToPage(6);
                    break;
                case TitleView.INDEX_ABOUT_CONTACT:
                    goToPage(13);
                    break;
            }
        }
        return view;
    }


    @OnClick(R.id.sub_menu_btn_1)
    public void onSubMenuBtn1Clicked() {
        goToPage(1);
    }

    @OnClick(R.id.sub_menu_btn_2)
    public void onSubMenuBtn2Clicked() {
        goToPage(6);
    }

    @OnClick(R.id.sub_menu_btn_3)
    public void onSubMenuBtn3Clicked() {
        goToPage(13);
    }


    @Override
    public PagerFragment getPagerFragment(int pos) {

        PagerFragment fr = null;
        switch (pos) {

            case 0:
                fr = new AboutUsWhoPage1Fragment();
                break;
            case 1:
                fr = new AboutUsWhoPage2Fragment();
                break;
            case 2:
                fr = new AboutUsWhoPage3Fragment();
                break;
            case 3:
                fr = new AboutUsWhoPage4Fragment();
                break;
            case 4:
                fr = new AboutUsWhoPage5Fragment();
                break;
            case 5:
                fr = new AboutUsStrengthPage1Fragment();
                break;
            case 6:
                fr = new AboutUsStrengthPage2Fragment();
                break;
            case 7:
                fr = new AboutUsStrengthPage3Fragment();
                break;
            case 8:
                fr = new AboutUsStrengthPage4Fragment();
                break;
            case 9:
                fr = new AboutUsStrengthPage5Fragment();
                break;
            case 10:
                fr = new AboutUsStrengthPage6Fragment();
                break;
            case 11:
                fr = new AboutUsStrengthPage7Fragment();
                break;
            case 12:
                fr = new AboutUsContactFragment();
                break;
            default:
                fr = new AboutUsWhoPage1Fragment();
                break;
        }

        return fr;
    }

    @Override
    public int getPagerCount() {
        return 13;
    }
}
