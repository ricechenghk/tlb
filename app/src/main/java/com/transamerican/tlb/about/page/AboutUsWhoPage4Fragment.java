package com.transamerican.tlb.about.page;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transamerican.tlb.R;
import com.transamerican.tlb.base.PagerFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Rice on 18/1/15.
 */
public class AboutUsWhoPage4Fragment extends PagerFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_about_who_page4, container, false);
        ButterKnife.inject(this, v);

        return v;
    }

    @Override
    public void onPageSelected() {

    }

    @Override
    public void onPageExited() {

    }

    @Override
    public int getTitle() {
        return R.string.about_us_title_who_we_are;
    }

    @Override
    public int getSlider() {
        return R.drawable.about_us_slide5_4;
    }

    @Override
    public boolean isVideoExist() {
        return false;
    }

    @Override
    public void onReplayClicked() {

    }

    @OnClick(R.id.section_transamerica)
    public void onSectionTransamericaClicked() {
        if (listener != null) {
            listener.goToPage(2);
        }
    }

    @OnClick(R.id.section_bermuda)
    public void onSectionBermudaClicked() {
        if (listener != null) {
            listener.goToPage(3);
        }
    }

    @OnClick(R.id.section_aegon)
    public void onSectionAegonClicked() {
        if (listener != null) {
            listener.goToPage(4);
        }
    }
}
