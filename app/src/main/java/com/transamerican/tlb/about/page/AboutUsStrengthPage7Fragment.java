package com.transamerican.tlb.about.page;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transamerican.tlb.R;

import butterknife.ButterKnife;

/**
 * Created by Rice on 18/1/15.
 */
public class AboutUsStrengthPage7Fragment extends AboutUsStrengthBaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_about_strength_page7, container, false);
        ButterKnife.inject(this, v);

        return v;
    }

    @Override
    public void onPageSelected() {

    }

    @Override
    public void onPageExited() {

    }

    @Override
    public int getTitle() {
        return R.string.about_us_title_our_strength;
    }

    @Override
    public int getSlider() {
        return R.drawable.about_us_slide7_7;
    }

    @Override
    public boolean isVideoExist() {
        return false;
    }

    @Override
    public void onReplayClicked() {

    }

}
