package com.transamerican.tlb.about.page;

import com.transamerican.tlb.R;
import com.transamerican.tlb.base.PagerFragment;

import butterknife.OnClick;

/**
 * Created by Rice on 18/1/15.
 */
public abstract class AboutUsStrengthBaseFragment extends PagerFragment {

    @OnClick(R.id.about_us_strength_confidence)
    public void onConfidenceClicked() {
        if (listener != null) {
            listener.goToPage(7);
        }
    }

    @OnClick(R.id.about_us_strength_selected_distribution)
    public void onSelectedDistributionClicked() {
        if (listener != null) {
            listener.goToPage(8);
        }
    }

    @OnClick(R.id.about_us_strength_underwriting)
    public void onUnderwritingClicked() {
        if (listener != null) {
            listener.goToPage(9);
        }
    }

    @OnClick(R.id.about_us_strength_investment)
    public void onInvestmentClicked() {
        if (listener != null) {
            listener.goToPage(10);
        }
    }

    @OnClick(R.id.about_us_strength_financial_strength)
    public void onFinancialStrengthClicked() {
        if (listener != null) {
            listener.goToPage(11);
        }
    }
}
