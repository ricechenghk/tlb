package com.transamerican.tlb;

public class TutorialActivity extends TutorialBaseActivity {
    private static final String TAG = TutorialActivity.class.getSimpleName();

    @Override
    public int getLayoutResId() {
        return R.layout.activity_tutorial;
    }

    @Override
    public int[] getTutorialResId() {
        return new int[]{
                R.drawable.tutorial_01,
                R.drawable.tutorial_02,
                R.drawable.tutorial_03,
                R.drawable.tutorial_04,
                R.drawable.tutorial_05,
                R.drawable.tutorial_06,
                R.drawable.tutorial_07,
                R.drawable.tutorial_08
        };
    }
}
