package com.transamerican.tlb.library;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;
import com.transamerican.tlb.MainApplication;
import com.transamerican.tlb.MainBaseActivity;
import com.transamerican.tlb.R;
import com.transamerican.tlb.library.adapter.FirstLevelCategoryListAdapter;
import com.transamerican.tlb.library.adapter.InterestRateListAdapter;
import com.transamerican.tlb.library.adapter.LibraryGridAdapter;
import com.transamerican.tlb.library.adapter.SubLevelCategoryListAdapter;
import com.transamerican.tlb.library.listener.LibraryActionListener;
import com.transamerican.tlb.model.Library;
import com.transamerican.tlb.model.LibraryCategory;
import com.transamerican.tlb.model.ProdColCategory;
import com.transamerican.tlb.sync.AppDataSource;
import com.transamerican.tlb.sync.LibraryDownloadTask;
import com.transamerican.tlb.util.LibraryAnimation;
import com.transamerican.tlb.util.LibraryExternalStorageHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.InjectViews;
import butterknife.OnClick;
import butterknife.OnItemClick;
import hk.com.playmore.syncframework.util.RunnableArgument;
import hk.com.playmore.syncframework.util.SyncFragment;

public class LibraryFragment extends SyncFragment implements LibraryActionListener, AdapterView.OnItemClickListener {

    public static final String BROADCAST_LIBRARY_DOWNLOAD = "BROADCAST_LIBRARY_DOWNLOAD";

    @InjectView(R.id.btn_product_collaterals)
    public ImageView btnProductCollaterals;

    @InjectViews({R.id.btn_product_collaterals, R.id.btn_field_communications, R.id.btn_forms, R.id.btn_underwriting_guide, R.id.btn_transware})
    List<ImageView> sections;

    @InjectView(R.id.all_doc_count)
    public TextView allDocCount;
    @InjectView(R.id.list_category)
    public ListView categoryList;
    public FirstLevelCategoryListAdapter firstLevelMenuAdapter;

    @InjectView(R.id.grid_library)
    public StickyGridHeadersGridView libraryGrid;
    public LibraryGridAdapter mLibraryGridAdapter;

    @InjectView(R.id.lib_root_menu)
    public LinearLayout rootMenu;
    @InjectView(R.id.lib_sub_menu)
    public LinearLayout subMenu;

    @InjectView(R.id.lib_sub_menu_category_title)
    public TextView subMenuTitle;
    @InjectView(R.id.lib_sub_menu_category_count)
    public TextView subMenuCount;
    @InjectView(R.id.list_sub_menu_category)
    public ListView subMenuCategoryList;
    public SubLevelCategoryListAdapter subLevelMenuAdapter;

    @InjectView(R.id.lib_interest_rate_txt)
    public TextView firstLevelInterestRateTxt;
    @InjectView(R.id.lib_interest_rate_list)
    public ListView firstLevelInterestRateList;
    public InterestRateListAdapter firstLevelInterestRateListAdapter;

    @InjectView(R.id.sub_lib_interest_rate_txt)
    public TextView subLevelInterestRateTxt;
    @InjectView(R.id.sub_lib_interest_rate_list)
    public ListView subLevelInterestRateList;
    public InterestRateListAdapter subLevelInterestRateListAdapter;

    private int currentSection = LibraryCategory.PRODUCT_COLLATERALS; //Default start is Product Collaterals

    private ProgressDialog mLoadingDialog;
    private LibraryExternalStorageHelper storageHelper = new LibraryExternalStorageHelper();

	public View onCreateView(LayoutInflater inflater,
		      ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_library, container, false);
        ButterKnife.inject(this, view);

        firstLevelMenuAdapter = new FirstLevelCategoryListAdapter(getActivity());
        categoryList.setAdapter(firstLevelMenuAdapter);

        mLibraryGridAdapter = new LibraryGridAdapter(getActivity(), currentSection, new ArrayList<LibraryCategory>(), new ArrayList<Library>(), this);
        libraryGrid.setAdapter(mLibraryGridAdapter);
        libraryGrid.setOnItemClickListener(this);

        subLevelMenuAdapter = new SubLevelCategoryListAdapter(getActivity());
        subMenuCategoryList.setAdapter(subLevelMenuAdapter);

        firstLevelInterestRateListAdapter = new InterestRateListAdapter(getActivity());
        firstLevelInterestRateList.setAdapter(firstLevelInterestRateListAdapter);

        subLevelInterestRateListAdapter = new InterestRateListAdapter(getActivity());
        subLevelInterestRateList.setAdapter(subLevelInterestRateListAdapter);

        btnProductCollaterals.performClick();

        return view;
	}

    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                mMessageReceiver, new IntentFilter(BROADCAST_LIBRARY_DOWNLOAD));
    }

    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(
                mMessageReceiver);
    }

    public void resetMenu() {
        rootMenu.setVisibility(View.VISIBLE);
        subMenu.setVisibility(View.GONE);
    }

    public void resetSectionsButtonState() {
        for (ImageView view : sections) {
            view.setSelected(false);
        }
    }

    @OnClick(R.id.btn_product_collaterals)
    public void onProductCollateralsClicked(View view) {
        resetSectionsButtonState();
        view.setSelected(true);

        ((MainApplication) getActivity().getApplication()).sendTrackerEvent(R.string.ga_prod_col);

        ((MainBaseActivity)getActivity()).showLoading();
        //Get Category from Server
        AppDataSource.getInstance().getProdColCategoryFromServer(getActivity(), this, new RunnableArgument() {

            @Override
            public void run(String object) {

                //Get Library From Server
                AppDataSource.getInstance().getProdColLibraryFromServer(getActivity(), LibraryFragment.this, new RunnableArgument() {

                    @Override
                    public void run(String object) {
                        ((MainBaseActivity)getActivity()).hideLoading();

                        currentSection = LibraryCategory.PRODUCT_COLLATERALS;
                        constructLibraryTreeByRootId("");
                        resetMenu();
                    }
                });
            }
        });
    }

    @OnClick(R.id.btn_field_communications)
    public void onFieldCommunicationsClicked(View view) {
        resetSectionsButtonState();
        view.setSelected(true);

        ((MainApplication) getActivity().getApplication()).sendTrackerEvent(R.string.ga_field_comm);

        ((MainBaseActivity)getActivity()).showLoading();
        //Get Category from Server
        AppDataSource.getInstance().getFieldCommCategoryFromServer(getActivity(), this, new RunnableArgument() {

            @Override
            public void run(String object) {

                //Get Library From Server
                AppDataSource.getInstance().getFieldCommLibraryFromServer(getActivity(), LibraryFragment.this, new RunnableArgument() {

                    @Override
                    public void run(String object) {
                        ((MainBaseActivity)getActivity()).hideLoading();
                        currentSection = LibraryCategory.FIELD_COMMUNICATIONS;
                        constructLibraryTreeByRootId("");
                        resetMenu();
                    }
                });
            }
        });
    }

    @OnClick(R.id.btn_forms)
    public void onFormsClicked(View view) {
        resetSectionsButtonState();
        view.setSelected(true);

        ((MainApplication) getActivity().getApplication()).sendTrackerEvent(R.string.ga_app_forms);

        ((MainBaseActivity)getActivity()).showLoading();
        //Get Category from Server
        AppDataSource.getInstance().getFormsCategoryFromServer(getActivity(), this, new RunnableArgument() {

            @Override
            public void run(String object) {

                //Get Library From Server
                AppDataSource.getInstance().getFormsLibraryFromServer(getActivity(), LibraryFragment.this, new RunnableArgument() {

                    @Override
                    public void run(String object) {
                        ((MainBaseActivity)getActivity()).hideLoading();
                        currentSection = LibraryCategory.FORMS;
                        constructLibraryTreeByRootId("");
                        resetMenu();
                    }
                });
            }
        });
    }

    @OnClick(R.id.btn_underwriting_guide)
    public void onUnderwritingGuideClicked(View view) {
        resetSectionsButtonState();
        view.setSelected(true);

        ((MainApplication) getActivity().getApplication()).sendTrackerEvent(R.string.ga_under_guide);

        ((MainBaseActivity)getActivity()).showLoading();
        //Get Category from Server
        AppDataSource.getInstance().getUnderwritingGuideCategoryFromServer(getActivity(), this, new RunnableArgument() {

            @Override
            public void run(String object) {

                //Get Library From Server
                AppDataSource.getInstance().getUnderwritingGuideLibraryFromServer(getActivity(), LibraryFragment.this, new RunnableArgument() {

                    @Override
                    public void run(String object) {
                        ((MainBaseActivity)getActivity()).hideLoading();
                        currentSection = LibraryCategory.UNDERWRITING_GUIDE;
                        constructLibraryTreeByRootId("");
                        resetMenu();
                    }
                });
            }
        });
    }

    @OnClick(R.id.btn_transware)
    public void onTranswareClicked(View view) {
        resetSectionsButtonState();
        view.setSelected(true);

        ((MainApplication) getActivity().getApplication()).sendTrackerEvent(R.string.ga_transware);

        ((MainBaseActivity)getActivity()).showLoading();
        //Get Category from Server
        AppDataSource.getInstance().getTranswareCategoryFromServer(getActivity(), this, new RunnableArgument() {

            @Override
            public void run(String object) {

                //Get Library From Server
                AppDataSource.getInstance().getTranswareLibraryFromServer(getActivity(), LibraryFragment.this, new RunnableArgument() {

                    @Override
                    public void run(String object) {
                        ((MainBaseActivity)getActivity()).hideLoading();
                        currentSection = LibraryCategory.TRANSWARE;
                        constructLibraryTreeByRootId("");
                        resetMenu();
                    }
                });
            }
        });
    }


    public void constructLibraryTreeByRootId(String id) {
        constructLibraryTreeByRootId(id, null);
    }

    public void constructLibraryTreeByRootId(String id, LibraryCategory category) {
        int count = 0;
        List<ProdColCategory> prodColCatListWithLib = new ArrayList<>();
        List<LibraryCategory> catListWithLib = new ArrayList<>();
        List<Library> allLibList = new ArrayList<>();
        List<? extends LibraryCategory> list = AppDataSource.getInstance().getLibraryCategoryBySection(getActivity(), currentSection, id);
        for (LibraryCategory item : list) {
			List<String> subCatIds = getAllSubCategoryID(item);
            List<? extends Library> libList = AppDataSource.getInstance().getAllLibraryInSubCategoryBySection(getActivity(), currentSection, subCatIds);
            Log.i("", item.getTitle()+": "+subCatIds.toString()+" , Count: "+libList.size());
			item.setCount(libList.size());
			count +=libList.size();

            if(libList.size()>0)
            {
                allLibList.addAll(libList);
                catListWithLib.add(item);
            }

            if (currentSection == LibraryCategory.PRODUCT_COLLATERALS) {
                ProdColCategory prodColCategory = (ProdColCategory) item;
                if (prodColCategory.getShortForm() != null && !prodColCategory.getShortForm().isEmpty()) {
                    prodColCatListWithLib.add((ProdColCategory) item);
                }
            }
        }

        //If no sub category, get root library
        if(category!=null)
        {
            if(list==null || list.size()==0)
            {
                List<String> subCatIds = new ArrayList<>();
                subCatIds.add(id);
                List<? extends Library> libList = AppDataSource.getInstance().getAllLibraryInSubCategoryBySection(getActivity(), currentSection, subCatIds);
                count +=libList.size();

                if(libList.size()>0)
                {
                    allLibList.addAll(libList);

                    category.setCount(count);
                    catListWithLib.add(category);
                }
            }
        }

        firstLevelInterestRateListAdapter.replaceWith(new ArrayList<ProdColCategory>());
        if (id == null || id.isEmpty()) {
            allDocCount.setText(Integer.toString(count));
            firstLevelMenuAdapter.replaceWith(list);
            mLibraryGridAdapter.replaceWith(currentSection, catListWithLib, allLibList);

            if (currentSection == LibraryCategory.PRODUCT_COLLATERALS) {
                firstLevelInterestRateTxt.setVisibility(View.VISIBLE);
                firstLevelInterestRateList.setVisibility(View.VISIBLE);
                firstLevelInterestRateListAdapter.replaceWith(prodColCatListWithLib);
            } else {
                firstLevelInterestRateTxt.setVisibility(View.INVISIBLE);
                firstLevelInterestRateList.setVisibility(View.INVISIBLE);
            }

        } else {
            subMenuCount.setText(Integer.toString(count));
            subLevelMenuAdapter.replaceWith(list);
            mLibraryGridAdapter.replaceWith(currentSection, catListWithLib, allLibList);
        }

	}

    public List<String> getAllSubCategoryID(LibraryCategory item) {
		List<String> subCatIds = new ArrayList<String>();
		subCatIds.add(item.getId());
		getSubCategoryID(item, subCatIds);
		return subCatIds;
	}

    public void getSubCategoryID(LibraryCategory root, List<String> subCatIds) {
        List<? extends LibraryCategory> list = AppDataSource.getInstance().getLibraryCategoryBySection(getActivity(), currentSection, root.getId());
        for (LibraryCategory item : list) {
			subCatIds.add(item.getId());
			getSubCategoryID(item, subCatIds);
		}
	}

    @OnItemClick(R.id.list_category)
    public void onRootCategoryClicked(AdapterView<?> parent, View view, int position, long id) {
        subMenu.setVisibility(View.VISIBLE);
        subMenu.setAnimation(LibraryAnimation.getInFromRight());
        rootMenu.setAnimation(LibraryAnimation.getOutToLeft());
        rootMenu.setVisibility(View.GONE);

        LibraryCategory cate = (LibraryCategory) parent.getItemAtPosition(position);
        subMenuTitle.setText(cate.getTitle());

        subLevelInterestRateTxt.setVisibility(View.INVISIBLE);
        subLevelInterestRateList.setVisibility(View.INVISIBLE);

        subLevelInterestRateListAdapter.replaceWith(new ArrayList<ProdColCategory>());
        if (currentSection == LibraryCategory.PRODUCT_COLLATERALS) {

            ProdColCategory prodColCategory = (ProdColCategory) cate;
            if (prodColCategory.getShortForm() != null && !prodColCategory.getShortForm().isEmpty()) {

                subLevelInterestRateTxt.setVisibility(View.VISIBLE);
                subLevelInterestRateList.setVisibility(View.VISIBLE);

                List<ProdColCategory> list = new ArrayList<>();
                list.add(prodColCategory);
                subLevelInterestRateListAdapter.replaceWith(list);

            }
        }

        constructLibraryTreeByRootId(cate.getId(), cate);
    }

    @OnClick(R.id.lib_sub_menu_back)
    public void onSubMenuBackCliced() {
        rootMenu.setVisibility(View.VISIBLE);
        rootMenu.setAnimation(LibraryAnimation.getInFromLeft());
        subMenu.setAnimation(LibraryAnimation.getOutToRight());
        subMenu.setVisibility(View.GONE);

        constructLibraryTreeByRootId("");
    }

    @OnItemClick(R.id.list_sub_menu_category)
    public void onSubMenuCategoryClicked(AdapterView<?> parent, View view, int position, long id) {
        LibraryCategory cate = (LibraryCategory) parent.getItemAtPosition(position);
        List<String> cateIds = new ArrayList<>();
        cateIds.add(cate.getId());
        List<LibraryCategory> catListWithLib = new ArrayList<>();
        List<Library> allLibList = new ArrayList<>();
        List<? extends Library> libList = AppDataSource.getInstance().getAllLibraryInSubCategoryBySection(getActivity(), currentSection, cateIds);
        if (libList.size() > 0) {
            allLibList.addAll(libList);
            catListWithLib.add(cate);
            mLibraryGridAdapter.replaceWith(currentSection, catListWithLib, allLibList);
        }
    }

    @Override
    public void onLibraryDownloadClicked(Library library) {

        mLoadingDialog = new ProgressDialog(getActivity());
        mLoadingDialog.setMessage(getString(R.string.loading));
        mLoadingDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mLoadingDialog.setIndeterminate(false);
        mLoadingDialog.setCancelable(false);
        mLoadingDialog.show();

        LibraryDownloadTask task = new LibraryDownloadTask(getActivity(), currentSection, library.getId(), BROADCAST_LIBRARY_DOWNLOAD);
        task.execute();
    }

    @Override
    public void onLibraryDeleteClicked(final Library library) {

        final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setMessage(R.string.library_delete_confirm_msg);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                storageHelper.deleteLibrary(getActivity(), currentSection, library.getId());
                mLibraryGridAdapter.notifyDataSetChanged();

            }
        });
        alert.setNegativeButton(R.string.cancel, null);
        alert.show();

    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            String state = intent.getStringExtra(LibraryDownloadTask.BROADCAST_STATE);
            if (state == LibraryDownloadTask.STATE_DOWNLOADING) {
                int progress = intent.getIntExtra(LibraryDownloadTask.BROADCAST_PROGRESS, 0);
                if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
                    mLoadingDialog.setProgress(progress);
                }
            } else if (state == LibraryDownloadTask.STATE_FINISH) {
                String error = intent.getStringExtra(LibraryDownloadTask.BROADCAST_ERROR);
                if (error == null || error.trim().isEmpty()) {
                    Log.i("", "File is downloaded Success");
                } else {
                    Log.i("", "File is downloaded Fail");
                }

                if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
                    mLoadingDialog.dismiss();
                }
                mLibraryGridAdapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Library library = mLibraryGridAdapter.getItem(position);
        boolean downloaded = mLibraryGridAdapter.isLibraryDownloaded(currentSection, library);
        if (downloaded) {
            Intent intent = new Intent(getActivity(), LibraryPDFActivity.class);
            intent.putExtra(LibraryPDFActivity.EXTRA_LIBRARY_SECTION, currentSection);
            intent.putExtra(LibraryPDFActivity.EXTRA_LIBRARY_ID, library.getId());
            getActivity().startActivity(intent);
        }
    }
}
