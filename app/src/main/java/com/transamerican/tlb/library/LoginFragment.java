package com.transamerican.tlb.library;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.transamerican.tlb.MainApplication;
import com.transamerican.tlb.MainBaseActivity;
import com.transamerican.tlb.R;
import com.transamerican.tlb.listener.MainSectionChangeListener;
import com.transamerican.tlb.sync.AppDataSource;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import hk.com.playmore.syncframework.util.RunnableArgument;
import hk.com.playmore.syncframework.util.SyncFragment;

/**
 * Created by Rice on 24/1/15.
 */
public class LoginFragment extends SyncFragment {

    protected MainSectionChangeListener mainSectionChangeListener;

    @InjectView(R.id.login_username)
    EditText username;

    @InjectView(R.id.login_pwd)
    EditText password;

    @InjectView(R.id.btn_login)
    TextView btnLogin;

    public int getResId() {
        return R.layout.fragment_login;
    }

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getResId(), container, false);
        ButterKnife.inject(this, view);

        username.setText(AppDataSource.getInstance().getLoginUser(getActivity()));
        password.setText(AppDataSource.getInstance().getLoginPwd(getActivity()));

        ((MainApplication) getActivity().getApplication()).sendTrackerEvent(R.string.ga_login);

        return view;
    }

    @OnClick(R.id.main)
    public void onMainClicked() {

        btnLogin.requestFocus(); //jump the focus first, to make EditText value there

        final String name = username.getText().toString();
        final String pwd = password.getText().toString();
        AppDataSource.getInstance().setLoginUser(getActivity(), name);
        AppDataSource.getInstance().setLoginPwd(getActivity(), pwd);

        getFragmentManager().popBackStack();
    }

    public void setMainSectionChangeListener(MainSectionChangeListener mainSectionChangeListener) {
        this.mainSectionChangeListener = mainSectionChangeListener;
    }

    @OnClick(R.id.btn_login)
    public void onLoginClicked() {
        final String name = username.getText().toString();
        final String pwd = password.getText().toString();

        ((MainBaseActivity)getActivity()).showLoading();
        AppDataSource.getInstance().login(getActivity(), name, pwd, LoginFragment.this, new RunnableArgument() {
            @Override
            public void run(String object) {
                ((MainBaseActivity)getActivity()).hideLoading();
                if (object == null) {
                    //Success
                    if (mainSectionChangeListener != null) {
                        mainSectionChangeListener.onSectionChange(true);
                    }
                    getFragmentManager().popBackStack();
                    goToLibrary();
                } else {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                    dialogBuilder.setTitle(R.string.error);

                    //Fail
                    int errorStrRes = R.string.login_error_invalid;
                    if ("2".equals(object)
                            || "3".equals(object)
                            || "5".equals(object)
                            || "6".equals(object)) {
                        errorStrRes = R.string.login_error_contact;
                    } else if ("4".equals(object)) {
                        errorStrRes = R.string.login_error_expire;
                        dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String url = "https://transactstg2.transamerica.com/forgot_password.aspx?RequestType=I&UserId=" + name;
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                startActivity(browserIntent);
                            }
                        });
                    }
                    dialogBuilder.setNegativeButton(R.string.cancel, null);

                    dialogBuilder.setMessage(errorStrRes);
                    dialogBuilder.show();
                }
            }
        });
    }

    public void goToLibrary() {
        LibraryFragment fr = new LibraryFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fr);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.login_forgot)
    public void onForgetPwdClicked()
    {
        String url = "https://transactrls.transamerica.com/forgot_password.aspx?RequestType=F";
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }
}
