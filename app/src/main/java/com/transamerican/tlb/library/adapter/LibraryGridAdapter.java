package com.transamerican.tlb.library.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tonicartos.widget.stickygridheaders.StickyGridHeadersBaseAdapter;
import com.transamerican.tlb.R;
import com.transamerican.tlb.library.listener.LibraryActionListener;
import com.transamerican.tlb.model.FieldCommLibrary;
import com.transamerican.tlb.model.Library;
import com.transamerican.tlb.model.LibraryCategory;
import com.transamerican.tlb.model.UnderwritingGuideLibrary;
import com.transamerican.tlb.util.LibraryExternalStorageHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class LibraryGridAdapter extends BaseAdapter implements StickyGridHeadersBaseAdapter {

    public List<String> labelSet = new ArrayList<>();

    private Context context;
    private int sectionId;
    private List<LibraryCategory> mListHeaders;
    private List<Library> lista;
    private LayoutInflater inflater;
    private LibraryActionListener listener;
    private LibraryExternalStorageHelper storageHelper = new LibraryExternalStorageHelper();

    public LibraryGridAdapter(Context context, int sectionId, List<LibraryCategory> mList, List<Library> lista, LibraryActionListener listener) {
        this.context = context;
        this.sectionId = sectionId;
        this.mListHeaders = mList;
        this.lista = lista;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    public void replaceWith(int sectionId, List<LibraryCategory> mList, List<Library> lista) {
        this.sectionId = sectionId;
        this.mListHeaders = mList;
        this.lista = lista;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Library getItem(int i) {
        return lista.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {

            boolean tabletSize = context.getResources().getBoolean(R.bool.isTablet);
            view = inflater.inflate(tabletSize?R.layout.grid_item_library:R.layout.grid_item_p_library, viewGroup, false);

            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view.findViewById(R.id.library_title);
            viewHolder.language = (TextView) view.findViewById(R.id.library_language);
            viewHolder.fileSizeTxt = (TextView) view.findViewById(R.id.library_file_size_txt);
            viewHolder.fileSize = (TextView) view.findViewById(R.id.library_file_size);
            viewHolder.label = (TextView) view.findViewById(R.id.library_label);
            viewHolder.download = (ImageView) view.findViewById(R.id.library_download);
            viewHolder.delete = (ImageView) view.findViewById(R.id.library_delete);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        final Library library = getItem(position);

        viewHolder.title.setText(library.getTitle());
        viewHolder.language.setText(library.getLanguage());


        int fileSizeValue = Integer.parseInt(library.getFilesize());
        int kb = fileSizeValue / 1024;
        if (kb < 1024) {
            viewHolder.fileSize.setText(kb + " KB");
        } else {
            int mb = kb / 1024;
            viewHolder.fileSize.setText(mb + " MB");
        }

        viewHolder.label.setVisibility(View.INVISIBLE);
        boolean isOdd = true;
        if (sectionId == LibraryCategory.FIELD_COMMUNICATIONS) {
            FieldCommLibrary tmp = (FieldCommLibrary) library;
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
            viewHolder.label.setVisibility(View.VISIBLE);
            viewHolder.label.setText(sdf.format(tmp.getDate()));

            SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
            String year = yearFormat.format(tmp.getDate());
            if (!labelSet.contains(year)) {
                labelSet.add(year);
            }

            int index = labelSet.indexOf(year);
            if (index % 2 != 0) {
                isOdd = false;
            }

        } else if (sectionId == LibraryCategory.UNDERWRITING_GUIDE) {
            UnderwritingGuideLibrary tmp = (UnderwritingGuideLibrary) library;
            if (tmp.getLocation() != null && !tmp.getLocation().isEmpty()) {
                viewHolder.label.setVisibility(View.VISIBLE);
                if (tmp.getLocation().equals("0")) {
                    viewHolder.label.setText(R.string.underwriting_hk);
                } else if (tmp.getLocation().equals("1")) {
                    viewHolder.label.setText(R.string.underwriting_sg);
                }
            }
        }


        int labelOddResId = context.getResources().getColor(R.color.library_item_label_bg_red);
        int labelEvenResId = context.getResources().getColor(R.color.library_item_label_bg_green);
        int labelLocationId = context.getResources().getColor(R.color.library_item_label_bg_light_green);

        int colorCode = context.getResources().getColor(R.color.black);
        if (isLibraryDownloaded(sectionId, library)) {
            viewHolder.delete.setVisibility(View.VISIBLE);
            viewHolder.download.setVisibility(View.INVISIBLE);
            viewHolder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onLibraryDeleteClicked(library);
                }
            });
        } else {
            viewHolder.delete.setVisibility(View.INVISIBLE);
            viewHolder.download.setVisibility(View.VISIBLE);
            viewHolder.download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onLibraryDownloadClicked(library);
                }
            });

            colorCode = context.getResources().getColor(R.color.gray);
            labelEvenResId = context.getResources().getColor(R.color.library_item_label_bg_light_green);
            labelOddResId = context.getResources().getColor(R.color.library_item_label_bg_light_red);
            labelLocationId = context.getResources().getColor(R.color.library_item_label_bg_light_light_green);
        }

        viewHolder.title.setTextColor(colorCode);
        viewHolder.language.setTextColor(colorCode);
        viewHolder.fileSizeTxt.setTextColor(colorCode);
        viewHolder.fileSize.setTextColor(colorCode);

        if (sectionId == LibraryCategory.UNDERWRITING_GUIDE) {
            //Underwriting Guide
//            viewHolder.label.setBackgroundResource(labelLocationId);
            viewHolder.label.setBackgroundColor(labelLocationId);
        } else {
            //Field Communication
//            viewHolder.label.setBackgroundResource(isOdd ? labelOddResId : labelEvenResId);
            viewHolder.label.setBackgroundColor(isOdd ? labelOddResId : labelEvenResId);
        }


        return view;
    }

    static class ViewHolder {
        TextView title;
        TextView language;
        TextView fileSizeTxt;
        TextView fileSize;
        TextView label;
        ImageView download;
        ImageView delete;
    }

    @Override
    public int getCountForHeader(int i) {
        return mListHeaders.get(i).getCount();
    }

    @Override
    public int getNumHeaders() {
        return mListHeaders.size();
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.section_library, parent, false);
        }

        //EDIT YOUR VIEW HEADER HERE
        TextView title = (TextView) convertView.findViewById(R.id.title);
        title.setText(mListHeaders.get(position).getTitle());

        return convertView;
    }

    public boolean isLibraryDownloaded(int sectionId, Library library) {
        if (storageHelper.isLibraryLayoutJSONFileExist(context, sectionId, library.getId())) {
            return true;
        } else {

            return false;
        }
    }

    public static int getIndex(Set<? extends Object> set, Object value) {
        int result = 0;
        for (Object entry : set) {
            if (entry.equals(value)) return result;
            result++;
        }
        return -1;
    }
}
