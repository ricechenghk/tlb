package com.transamerican.tlb.library.listener;

import com.transamerican.tlb.model.Library;

/**
 * Created by Rice on 16/1/15.
 */
public interface LibraryActionListener {

    public void onLibraryDownloadClicked(Library library);

    public void onLibraryDeleteClicked(Library library);
}
