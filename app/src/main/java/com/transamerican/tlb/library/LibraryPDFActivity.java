package com.transamerican.tlb.library;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aphidmobile.flip.FlipViewController;
import com.aphidmobile.flip.FlipViewController.ViewFlipListener;
import com.google.bitmapfun.ui.RecyclingImageView;
import com.google.bitmapfun.util.ImageCache;
import com.google.bitmapfun.util.ImageFetcher;
import com.google.bitmapfun.util.RecyclingBitmapDrawable;
import com.google.bitmapfun.util.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.transamerican.tlb.PrintDialogActivity;
import com.transamerican.tlb.R;
import com.transamerican.tlb.model.Library;
import com.transamerican.tlb.model.deserializer.PHPDateDeserializer;
import com.transamerican.tlb.model.pdf.LibraryLayout;
import com.transamerican.tlb.model.pdf.LibraryPage;
import com.transamerican.tlb.model.pdf.PMFlipViewController;
import com.transamerican.tlb.model.pdf.ZoomableViewGroup;
import com.transamerican.tlb.sync.AppDataSource;
import com.transamerican.tlb.sync.LibraryDownloadTask;
import com.transamerican.tlb.sync.PDFDownloadTask;
import com.transamerican.tlb.util.LibraryExternalStorageHelper;
import com.transamerican.tlb.util.SimpleLoadingDialog;

import java.io.File;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import hk.com.playmore.util.DisplayUtils;


/**
 * Created by Rice on 16/1/15.
 */

public class LibraryPDFActivity extends FragmentActivity {

    public static final String BROADCAST_PDF_DOWNLOAD = "BROADCAST_PDF_DOWNLOAD";

    public static final String EXTRA_LIBRARY_SECTION = "EXTRA_LIBRARY_SECTION";
    public static final String EXTRA_LIBRARY_ID = "EXTRA_LIBRARY_ID";
    public static String GOTO_PAGE_KEY = "GOTO_PAGE_KEY";

    PMFlipViewController flipView;
    LibraryExternalStorageHelper mExternalStorageHelper = new LibraryExternalStorageHelper();

    //    private Library mCurrentLibrary;
    private int currentPage;

    private int sectionId;
    private String mLibraryId;
    private Library mLibrary;

    @InjectView(R.id.library_detail_reader_content_layout)
    RelativeLayout mContentLayout;
    @InjectView(R.id.library_detail_top_menu)
    RelativeLayout mTopMenuLayout;
    @InjectView(R.id.library_detail_number_page_layout)
    FrameLayout mNumberPageLayout;
    @InjectView(R.id.library_detail_thumbnail_scroll)
    HorizontalScrollView thumbnailScroll;
    @InjectView(R.id.library_detail_thumbnail_area)
    LinearLayout thumbnail;

    public ImageCache mImageCache;
    private LibraryFlipAdapter flipAdapter;

    int flipViewWidth = 0;
    int flipViewHeight = 0;

    public static int PDF_DOWNLOAD_OPEN = 0;
    public static int PDF_DOWNLOAD_SEND = 1;
    public static int PDF_DOWNLOAD_PRINT = 2;

    public int action = -1;

    private ProgressDialog mLoadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sectionId = getIntent().getIntExtra(EXTRA_LIBRARY_SECTION, 0);
        mLibraryId = getIntent().getStringExtra(EXTRA_LIBRARY_ID);

        if (savedInstanceState != null) {
            currentPage = savedInstanceState.getInt(GOTO_PAGE_KEY);
            sectionId = getIntent().getIntExtra(EXTRA_LIBRARY_SECTION, 0);
            mLibraryId = savedInstanceState.getString(EXTRA_LIBRARY_ID);
        }

        setContentView(R.layout.activity_library_detail);
        ButterKnife.inject(this);

        //For Title
        TextView title = (TextView) findViewById(R.id.library_detail_title);
        mLibrary = AppDataSource.getInstance().getLibraryBySectionAndId(this, sectionId, mLibraryId);
        title.setText(mLibrary.getTitle());

        flipView = new PMFlipViewController(this, FlipViewController.HORIZONTAL);

        if (android.os.Build.VERSION.SDK_INT >= 11) {
            flipView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        flipView.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
        flipView.setOverFlipEnabled(false);
        flipView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

            @SuppressLint("NewApi")
            @Override
            public void onGlobalLayout() {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    flipView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    flipView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }

                reloadFlipAdapter();

                reloadAdapter();
                if (currentPage > 0) {
                    flipView.setSelection(currentPage - 1);
                }
                updatePageNumberText();
            }
        });

        mContentLayout.addView(flipView, -1, -1);

        mContentLayout.bringChildToFront(mNumberPageLayout);
        mContentLayout.bringChildToFront(mTopMenuLayout);
        mContentLayout.bringChildToFront(thumbnailScroll);

        flipView.setOnViewFlipListener(new ViewFlipListener() {

            @Override
            public void onViewFlipped(View paramView, int paramInt) {
                updatePageNumberText();
                hideDetailView();
            }
        });
    }

    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(BROADCAST_PDF_DOWNLOAD));
    }

    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mMessageReceiver);
    }

    public void reloadFlipAdapter() {
        ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams(
                this, ".bc_library_images");

        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of
        // app memory
        mImageCache = ImageCache.getInstance(this.getSupportFragmentManager(), cacheParams);

        flipViewHeight = flipView.getHeight();
        flipViewWidth = flipView.getWidth();

        final LibraryLayout layout = mExternalStorageHelper.getLibraryLayout(this, sectionId, mLibraryId, getGsonForCreateLayout());
        if (layout == null || layout.getPages() == null) {
            flipAdapter = new LibraryFlipAdapter(this, null);
            flipView.setAdapter(flipAdapter);
            flipView.setVisibility(View.GONE);
            return;
        }

        prepareThumbnail(layout);

        flipAdapter = new LibraryFlipAdapter(this, layout);
        flipView.setAdapter(flipAdapter);
    }

    private void reloadAdapter() {
        this.flipView.setAdapter(this.flipAdapter);
    }

    protected class LibraryFlipAdapter extends BaseAdapter {

        private Context context;
        private LibraryLayout libraryLayout;
        LayoutInflater mLayoutInflater;

        public LibraryFlipAdapter(Context context, LibraryLayout layout) {
            this.context = context;
            this.libraryLayout = layout;

            this.mLayoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            if (libraryLayout == null) {
                return 0;
            }
            return libraryLayout.getPages().size();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            FlipViewHolder viewHolder = new FlipViewHolder();
            convertView = mLayoutInflater.inflate(R.layout.library_view_page,
                    parent, false);

            viewHolder.bgImageView = (RecyclingImageView) convertView
                    .findViewById(R.id.view_mkt_zone_background);
            viewHolder.overlayObjectsContatiner = (RelativeLayout) convertView
                    .findViewById(R.id.view_mkt_zone_overlay_obj_container);
            viewHolder.parentZoomLayout = (ZoomableViewGroup) convertView
                    .findViewById(R.id.view_mkt_zone_zoomable_layout);

            viewHolder.parentZoomLayout
                    .setSingleTapListener(new ZoomableViewGroup.OnZoomableViewSingleTapListener() {

                        @Override
                        public void onSingleTapConfirmed(MotionEvent e) {
                            toggleDetailView();
                        }
                    });

            viewHolder.parentZoomLayout
                    .setOnZoomListener(new ZoomableViewGroup.OnZoomableViewZoomListener() {

                        @Override
                        public void onZoomToScaleFactor(ZoomableViewGroup zvg,
                                                        float scaleFactor) {
                            if (scaleFactor == zvg.getMinZoomScale()) {
                                flipView.setCanFlip(true);
                            } else {
                                flipView.setCanFlip(false);
                            }
                        }
                    });

            LibraryPage page = (LibraryPage) libraryLayout.getPages().get(position);
            String parentPath = mExternalStorageHelper.getLibraryLayoutFolderPath(LibraryPDFActivity.this, sectionId, mLibraryId);
            String bgPath = parentPath + page.getBackground();

            DisplayMetrics metrics = DisplayUtils.getScreenDisplayMetrics(LibraryPDFActivity.this);

            int imgWidth = metrics.widthPixels;
            int imgHeight = metrics.heightPixels;

            Log.d("bgPath", bgPath);
            Bitmap bm = null;
            BitmapDrawable drawable = null;
            if (mImageCache != null) {
                drawable = mImageCache.getBitmapFromMemCache(bgPath);
            }

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            // Returns null, sizes are in the options variable
            bm = BitmapFactory.decodeFile(bgPath, options);
            imgWidth = options.outWidth;
            imgHeight = options.outHeight;

            if (drawable != null) {
                viewHolder.bgImageView.setImageDrawable(drawable);

            } else {
//				int reqWidth = (int) flipView.getMeasuredWidth();
//				int reqHeight = (int) flipView.getMeasuredHeight();
                int reqWidth = (int) flipViewWidth;
                int reqHeight = (int) flipViewHeight;

//                bm = ImageFetcher.decodeSampledBitmapFromFile(bgPath,
//                        imgWidth, imgHeight, mImageCache);
                bm = ImageFetcher.decodeSampledBitmapFromFile(bgPath,
                        reqWidth, reqHeight, mImageCache);

                if (bm != null) {
                    if (Utils.hasHoneycomb()) {
                        // Running on Honeycomb or newer, so wrap in a standard
                        // BitmapDrawable
                        drawable = new BitmapDrawable(getResources(), bm);
                    } else {
                        // Running on Gingerbread or older, so wrap in a
                        // RecyclingBitmapDrawable
                        // which will recycle automagically
                        drawable = new RecyclingBitmapDrawable(getResources(), bm);

                    }
                    if (mImageCache != null) {
                        mImageCache.addBitmapToCache(bgPath, drawable);
                    }

                    viewHolder.bgImageView.setImageDrawable(drawable);

                } else {
                    // Background image file not exists
                    viewHolder.bgImageView.setImageDrawable(null);
                }
            }

            float widthScaleRatio = (float) imgWidth / (float) flipViewWidth;
            float heightScaleRatio = (float) imgHeight
                    / (float) flipViewHeight;
            float useRatio = 1;

            if (widthScaleRatio > heightScaleRatio) {
                // use height ratio
                useRatio = widthScaleRatio;

            } else {
                // use width ratio
                useRatio = heightScaleRatio;
            }

            // Overlay objects
//            if (page.getOverlay_objects() != null) {
//                for (int i = 0; i < page.getOverlay_objects().size(); i++) {
//                    final OverlayObject obj = page.getOverlay_objects()
//                            .get(i);
//
//                    final ImageButton overlayImageButton = OverlayObjectCreator
//                            .createOverlayObjectButton(LibraryPDFActivity.this, obj,
//                                    useRatio, flipViewWidth, flipViewHeight,
//                                    imgWidth, imgHeight);
//                    int imageResId = -1;
//
//                    //If no action is set, just simply no show for the overlay
//                    if(obj.getUrl()==null || obj.getUrl().equals(""))
//                    {
//                        continue;
//                    }
//
//                    if (obj.getType().equals("url")) {
//                        imageResId = R.drawable.marketing_zone_detail_page_bu_site;
//                        overlayImageButton.setTag(obj.getType());
//
//                    } else if (obj.getType().equals("page")) {
//                        imageResId = R.drawable.marketing_zone_detail_page_bu_line;
//                        overlayImageButton.setTag(obj.getType());
//
//                    } else if (obj.getType().equals("video")) {
//                        imageResId = R.drawable.marketing_zone_detail_page_bu_video;
//                        overlayImageButton.setTag(obj.getType());
//
//                    } else if (obj.getType().equals("ticket")) {
//                        final Button overlayButton = BCOverlayObjectCreator
//                                .getOverlayObjectButton(this, obj,
//                                        useRatio, flipViewWidth, flipViewHeight,
//                                        imgWidth, imgHeight);
//
//                        overlayButton.setTag(obj.getType());
//                        overlayButton.setBackgroundResource(R.drawable.library_ticket_stamp);
////						overlayButton.setText(this.getString(R.string.bc_eticket_redeem_now));
//                        overlayButton.setTextColor(Color.WHITE);
//                        overlayButton.setTextSize(12);
//                        overlayButton.setOnClickListener(new OnClickListener() {
//
//                            @Override
//                            public void onClick(View paramView) {
//                                onClickRedeemTicket(obj);
//                            }
//                        });
//                        viewHolder.overlayObjectsContatiner.addView(overlayButton);
//                        continue;
//
//                    } else {
//                        continue;
//                    }
//
//
//                    overlayImageButton.setImageResource(imageResId);
////					overlayBtn.setScaleType(ScaleType.CENTER_INSIDE);
//                    overlayImageButton.setOnClickListener(new OnClickListener() {
//
//                        @Override
//                        public void onClick(View paramView) {
//
//                            // Link
//                            if (overlayImageButton.getTag().equals("url")) {
//                                if (obj.getUrl() != null) {
//                                    String urlString = obj.getUrl();
//                                    if (!urlString.contains("http://")){
//                                        urlString = "http://" + urlString;
//                                    }
//                                    Log.d(null,
//                                            "Open URL : " + obj.getUrl());
//                                    Intent browserIntent = new Intent(
//                                            Intent.ACTION_VIEW, Uri.parse(urlString));
//                                    startActivity(browserIntent);
//                                }
//
//                                // goto page
//                            } else if (overlayImageButton.getTag().equals("page")) {
//                                int page = Integer.valueOf(obj.getUrl())
//                                        .intValue() - 1;
//                                if (page < 0
//                                        || page >= getCount()) {
//                                    Log.d(null, "Cannot go to page : "
//                                            + page);
//                                } else {
//                                    flipView.setSelection(page);
//                                    updatePageNumberText();
//                                }
//
//                                // Video
//                            } else if (overlayImageButton.getTag().equals("video")) {
//                                String parentPath = mExternalStorageHelper.getLibraryLayoutFolderPath(this, mLibraryId);
//                                String videoPath = parentPath + obj.getUrl();
//
////								Uri intentUri = Uri.parse(new File(videoPath).getAbsolutePath());
////								Intent intent = new Intent();
////							    intent.setAction(Intent.ACTION_VIEW);
////							    intent.setDataAndType(intentUri, "video/*");
////							    startActivity(intent);
//
//                                Intent videoPlayIntent = new Intent(Intent.ACTION_VIEW);
//                                videoPlayIntent.setDataAndType(Uri.fromFile(new File(videoPath)), "video/*");
//                                PackageManager manager = this.getPackageManager();
//                                List<ResolveInfo> infos = manager.queryIntentActivities(videoPlayIntent,
//                                        0);
//                                if (infos.size() > 0) {
//                                    startActivity(videoPlayIntent);
//                                } else {
//                                    Toast.makeText(this, "No player found. Please install a media player to play this video.", Toast.LENGTH_LONG).show();
//                                }
//                            }
//                        }
//                    });
//                    viewHolder.overlayObjectsContatiner.addView(overlayImageButton);
//                }
//            }
            return convertView;
        }


        class FlipViewHolder {
            RecyclingImageView bgImageView;
            ZoomableViewGroup parentZoomLayout;
            RelativeLayout overlayObjectsContatiner;

        }

        @Override
        public Object getItem(int paramInt) {
            return libraryLayout.getPages().get(paramInt);
        }

        @Override
        public long getItemId(int paramInt) {
            // TODO Auto-generated method stub
            return paramInt;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(EXTRA_LIBRARY_SECTION, sectionId);
        outState.putSerializable(EXTRA_LIBRARY_ID, mLibraryId);
        outState.putInt(GOTO_PAGE_KEY, getCurrentPage());
        super.onSaveInstanceState(outState);
    }

    public int getCurrentPage() {
        return flipView.getSelectedItemPosition() + 1;
    }

    public void updatePageNumberText() {
        TextView numTextView = (TextView) mNumberPageLayout.findViewById(R.id.library_detail_number_page_text_view);
        String pageNumText = getString(R.string.library_page_number_text);
        pageNumText = pageNumText.replace("{page_number}", String.valueOf(getCurrentPage()));
        pageNumText = pageNumText.replace("{total}", String.valueOf(flipAdapter.getCount()));
        numTextView.setText(pageNumText);
    }

    public void showGotoDialog() {
        AlertDialog.Builder gotoDialog = new AlertDialog.Builder(this);
        gotoDialog.setTitle(getString(R.string.library_page_goto_dialog_text));

        final EditText editText = new EditText(this);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        gotoDialog.setView(editText);

        gotoDialog.setPositiveButton(
                R.string.library_page_goto_dialog_ok_text,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        int value = Integer.valueOf(editText.getText().toString());
                        gotoPage(value);
                        arg0.dismiss();
                    }
                });
        gotoDialog.setNegativeButton(
                R.string.library_page_goto_dialog_cancel_text,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });
        gotoDialog.show();
    }

    protected void gotoPage(int page) {
        if (page < 1 || page > this.flipAdapter.getCount()) {
            Toast.makeText(this, R.string.invalid_page_num_text, Toast.LENGTH_LONG).show();
        } else {
            flipView.setSelection(page - 1);
            updatePageNumberText();
        }
    }

    public void showDetailView() {
        mTopMenuLayout.setVisibility(View.VISIBLE);
        thumbnailScroll.setVisibility(View.VISIBLE);
        mNumberPageLayout.setVisibility(View.VISIBLE);

    }

    public void hideDetailView() {
        mTopMenuLayout.setVisibility(View.INVISIBLE);
        thumbnailScroll.setVisibility(View.INVISIBLE);
        mNumberPageLayout.setVisibility(View.INVISIBLE);
    }

    public void toggleDetailView() {
        if (mNumberPageLayout.getVisibility() == View.VISIBLE) {
            hideDetailView();
        } else {
            showDetailView();
        }
    }

    private void onEmailClick() {
        final LibraryLayout layout = mExternalStorageHelper.getLibraryLayout(this, sectionId, mLibraryId, getGsonForCreateLayout());
        LibraryPage page = (LibraryPage) layout.getPages().get(getCurrentPage() - 1);
        String bgPath = mExternalStorageHelper.getHiddenNewsletterContentFileStringPath(this, mLibraryId, page.getBackground());

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + bgPath));
        startActivity(Intent.createChooser(emailIntent, getString(R.string.prompt_send_email_title)));
    }

    public Gson getGsonForCreateLayout() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new PHPDateDeserializer());
        gsonBuilder.registerTypeAdapter(Boolean.class,
                new PHPDateDeserializer());
        return gsonBuilder.create();
    }

    public void prepareThumbnail(LibraryLayout libraryLayout) {
        Resources r = getResources();
        int heightPx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150, r.getDisplayMetrics());
        int widthPx = (int) (heightPx / 1.4142);

        int pageNum = 1;
        String parentPath = mExternalStorageHelper.getLibraryLayoutFolderPath(LibraryPDFActivity.this, sectionId, mLibraryId);
        for (LibraryPage page : libraryLayout.getPages()) {
            ImageView view = new ImageView(this);
            view.setTag(pageNum);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoPage((Integer) v.getTag());
                }
            });

            LinearLayout.LayoutParams layoutParam =
                    new LinearLayout.LayoutParams(
                            widthPx,
                            heightPx);
            view.setPadding(10, 10, 10, 10);
            String bgPath = parentPath + page.getBackground();
            Picasso.with(this).load(new File(bgPath)).resize(widthPx, heightPx).into(view);
            thumbnail.addView(view, layoutParam);

            pageNum++;
        }
    }

    @OnClick(R.id.library_detail_back)
    public void onBackClicked() {
        finish();
    }

    @OnClick(R.id.library_detail_open)
    public void onOpenInClicked() {
        LibraryExternalStorageHelper storageHelper = new LibraryExternalStorageHelper();
        File target = storageHelper.getDownloadTargetFile(this, mLibrary.getDocUrl());
        if (target.exists()) {
            openPDF(target);
        } else {
            showLoading();
            action = PDF_DOWNLOAD_OPEN;
            PDFDownloadTask task = new PDFDownloadTask(this, mLibrary.getDocUrl(), BROADCAST_PDF_DOWNLOAD);
            task.execute();
        }

    }

    public void openPDF(File file) {
//        Uri uri = Uri.parse(file.toString().toLowerCase());
        Intent target = new Intent(Intent.ACTION_VIEW);

        // Get URI and MIME type of file
        Uri uri = Uri.fromFile(file);
        String mime = getContentResolver().getType(uri);

        target.setDataAndType(uri, "application/pdf");
        try {
            startActivity(target);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            // Instruct the user to install a PDF reader here, or something
        }
    }

    @OnClick(R.id.library_detail_send)
    public void onSendClicked() {
        LibraryExternalStorageHelper storageHelper = new LibraryExternalStorageHelper();
        File target = storageHelper.getDownloadTargetFile(this, mLibrary.getDocUrl());
        if (target.exists()) {
            sendPDF(target);
        } else {
            showLoading();
            action = PDF_DOWNLOAD_SEND;
            PDFDownloadTask task = new PDFDownloadTask(this, mLibrary.getDocUrl(), BROADCAST_PDF_DOWNLOAD);
            task.execute();
        }
    }

    public void sendPDF(File file) {
        String subject = "";
//        String text = "";
//        ArrayList<Uri> attachments = new ArrayList<>();
//        attachments.add();
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("application/pdf");
        intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{});
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + file.toString()));

        try {
            startActivity(Intent.createChooser(intent, "Send mail..."));
//            startActivity(intent);
        } catch (ActivityNotFoundException anfe) {
            anfe.printStackTrace();
        }
    }

    @OnClick(R.id.library_detail_print)
    public void onPrintClicked() {

        LibraryExternalStorageHelper storageHelper = new LibraryExternalStorageHelper();
        File target = storageHelper.getDownloadTargetFile(this, mLibrary.getDocUrl());
        if (target.exists()) {
            printPDF(target);
        } else {
            showLoading();
            action = PDF_DOWNLOAD_PRINT;
            PDFDownloadTask task = new PDFDownloadTask(this, mLibrary.getDocUrl(), BROADCAST_PDF_DOWNLOAD);
            task.execute();
        }
    }

    public void printPDF(File file) {
        Intent printIntent = new Intent(this, PrintDialogActivity.class);
        printIntent.setDataAndType(Uri.parse("file://" + file.toString()), "application/pdf");
        printIntent.putExtra("title", mLibrary.getTitle());
        startActivity(printIntent);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            String state = intent.getStringExtra(LibraryDownloadTask.BROADCAST_STATE);
            if (state == LibraryDownloadTask.STATE_DOWNLOADING) {
//                int progress = intent.getIntExtra(LibraryDownloadTask.BROADCAST_PROGRESS, 0);
//                if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
//                    mLoadingDialog.setProgress(progress);
//                }
            } else if (state == LibraryDownloadTask.STATE_FINISH) {
                String error = intent.getStringExtra(LibraryDownloadTask.BROADCAST_ERROR);
                if (error == null || error.trim().isEmpty()) {
                    Log.i("", "File is downloaded Success");
                    LibraryExternalStorageHelper storageHelper = new LibraryExternalStorageHelper();
                    File target = storageHelper.getDownloadTargetFile(LibraryPDFActivity.this, mLibrary.getDocUrl());
                    if (action == PDF_DOWNLOAD_OPEN) {
                        openPDF(target);
                    } else if (action == PDF_DOWNLOAD_SEND) {
                        sendPDF(target);
                    } else if (action == PDF_DOWNLOAD_PRINT) {
                        printPDF(target);
                    }

                } else {
                    Log.i("", "File is downloaded Fail");
                }

//                if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
//                    mLoadingDialog.dismiss();
//                }
                hideLoading();
            }
        }
    };

    public void showLoading() {
        try {
            hideLoading();
            mLoadingDialog = SimpleLoadingDialog.show(this, R.string.loading_text);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideLoading() {
        try {
            if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
                mLoadingDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
