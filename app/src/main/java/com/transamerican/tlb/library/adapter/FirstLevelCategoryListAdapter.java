package com.transamerican.tlb.library.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.transamerican.tlb.R;
import com.transamerican.tlb.model.LibraryCategory;
import com.transamerican.tlb.model.ProdColCategory;

import java.util.ArrayList;
import java.util.List;

public class FirstLevelCategoryListAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<? extends LibraryCategory> libraryCategory = new ArrayList<LibraryCategory>();

    public FirstLevelCategoryListAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void replaceWith(List<? extends LibraryCategory> libraryCategory) {
        this.libraryCategory = libraryCategory;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return libraryCategory.size();
    }

    @Override
    public LibraryCategory getItem(int position) {
        return libraryCategory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.list_item_library_category_first_level, viewGroup, false);

            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view.findViewById(R.id.category_title);
            viewHolder.count = (TextView) view.findViewById(R.id.category_count);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        LibraryCategory category = getItem(position);

        viewHolder.title.setText(category.getTitle());
        viewHolder.count.setText(Integer.toString(category.getCount()));

        if (category instanceof ProdColCategory) {
            //TODO do special handling for special cate
        }

        return view;
    }

    static class ViewHolder {
        TextView title;
        TextView count;
    }
}
