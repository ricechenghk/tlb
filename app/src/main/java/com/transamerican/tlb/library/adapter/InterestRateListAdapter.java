package com.transamerican.tlb.library.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.transamerican.tlb.R;
import com.transamerican.tlb.model.ProdColCategory;

import java.util.ArrayList;
import java.util.List;

public class InterestRateListAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<? extends ProdColCategory> prodColCategory = new ArrayList<ProdColCategory>();

    public InterestRateListAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void replaceWith(List<? extends ProdColCategory> ProdColCategory) {
        this.prodColCategory = ProdColCategory;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return prodColCategory.size();
    }

    @Override
    public ProdColCategory getItem(int position) {
        return prodColCategory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.list_item_library_interest_rate, viewGroup, false);

            viewHolder = new ViewHolder();
            viewHolder.shortForm = (TextView) view.findViewById(R.id.interest_rate_short_form);
            viewHolder.rate = (TextView) view.findViewById(R.id.interest_rate_short);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        ProdColCategory category = getItem(position);

        viewHolder.shortForm.setText(category.getShortForm());
        viewHolder.rate.setText(category.getInterestRate() + "%");
        return view;

    }

    static class ViewHolder {
        TextView shortForm;
        TextView rate;
    }
}
