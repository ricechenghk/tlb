package com.transamerican.tlb.term;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transamerican.tlb.R;
import com.transamerican.tlb.TitleView;
import com.transamerican.tlb.base.PagerFragment;
import com.transamerican.tlb.base.SectionBaseFragment;
import com.transamerican.tlb.term.page.TermLifeRopPage1Fragment;
import com.transamerican.tlb.term.page.TermLifeRopPage2Fragment;
import com.transamerican.tlb.term.page.TermLifeSuperPage1Fragment;
import com.transamerican.tlb.term.page.TermLifeSuperPage2Fragment;
import com.transamerican.tlb.term.page.TermLifeWhatFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class TermLifeFragment extends SectionBaseFragment {

    public static final String TAG = TermLifeFragment.class.getName();

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_term, container, false);
        ButterKnife.inject(this, view);

        initScreen();

        if (getArguments() != null) {
            int sectionId = getArguments().getInt("section");
            switch (sectionId) {
                case TitleView.INDEX_TERM_SUPER:
                    goToPage(2);
                    break;
                case TitleView.INDEX_TERM_ROP:
                    goToPage(4);
                    break;
            }
        }

        return view;
    }


    @OnClick(R.id.sub_menu_btn_1)
    public void onSubMenuBtn1Clicked() {
        goToPage(1);
    }

    @OnClick(R.id.sub_menu_btn_2)
    public void onSubMenuBtn2Clicked() {
        goToPage(2);
    }

    @OnClick(R.id.sub_menu_btn_3)
    public void onSubMenuBtn3Clicked() {
        goToPage(4);
    }


    @Override
    public PagerFragment getPagerFragment(int pos) {

        PagerFragment fr = null;
        switch (pos) {

            case 0:
                fr = new TermLifeWhatFragment();
                break;
            case 1:
                fr = new TermLifeSuperPage1Fragment();
                break;
            case 2:
                fr = new TermLifeSuperPage2Fragment();
                break;
            case 3:
                fr = new TermLifeRopPage1Fragment();
                break;
            case 4:
                fr = new TermLifeRopPage2Fragment();
                break;
            default:
                fr = new TermLifeWhatFragment();
                break;
        }

        return fr;
    }

    @Override
    public int getPagerCount() {
        return 5;
    }
}
