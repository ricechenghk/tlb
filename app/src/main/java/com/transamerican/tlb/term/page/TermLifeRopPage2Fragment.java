package com.transamerican.tlb.term.page;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.transamerican.tlb.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Rice on 18/1/15.
 */
public class TermLifeRopPage2Fragment extends TermVideoPagerFragment {

    @InjectView(R.id.detail_video_end_img)
    ImageView videoEndImage;

    protected boolean isSecondVideoPlayed = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_term_rop_page2, container, false);
        ButterKnife.inject(this, v);

        initVideo();

        return v;
    }

    public String getVideo() {
        return "convertibility_option_demoA2_Retina.mp4";
    }


    @Override
    public int getTitle() {
        return R.string.term_life_title_plan;
    }

    @Override
    public int getSlider() {
        return R.drawable.about_us_slide5_5;
    }

    @Override
    public void handleAspectRatio() {
        super.handleAspectRatio();
        videoEndImage.setLayoutParams(layoutParams);
    }

    @Override
    public void onCompletion(MediaPlayer arg0) {
        super.onCompletion(arg0);
        if (!isSecondVideoPlayed) {
            videoEndImage.setImageResource(R.drawable.video_convertibility_option_demoa2_end);
            videoEndImage.setVisibility(View.VISIBLE);
        } else {
            videoEndImage.setImageResource(R.drawable.video_convertibility_option_demob2_end);
            videoEndImage.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPageSelected() {
        isSecondVideoPlayed = false;
        prepareVideo(getVideo());
        super.onPageSelected();
        videoEndImage.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.detail_video_end_img)
    public void onVideoImageClicked() {
        if (!isSecondVideoPlayed) {
            isSecondVideoPlayed = true;
            videoEndImage.setVisibility(View.INVISIBLE);
            prepareVideo("convertibility_option_demoB2_Retina.mp4");
        }

    }

    public void onReplayClicked() {
        onPageSelected();
    }


}
