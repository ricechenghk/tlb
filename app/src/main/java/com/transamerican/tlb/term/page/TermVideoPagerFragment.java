package com.transamerican.tlb.term.page;

import com.transamerican.tlb.R;
import com.transamerican.tlb.base.VideoPagerFragment;

import butterknife.OnClick;

/**
 * Created by Rice on 22/1/15.
 */
public abstract class TermVideoPagerFragment extends VideoPagerFragment {

    @OnClick(R.id.term_life_what)
    public void onSectionWhatClicked() {
        if (listener != null) {
            listener.goToPage(1);
        }
    }

    @OnClick(R.id.term_life_trendsetter_super)
    public void onSectionSuperClicked() {
        if (listener != null) {
            listener.goToPage(2);
        }
    }

    @OnClick(R.id.term_life_trendsetter_rop)
    public void onSectionRopClicked() {
        if (listener != null) {
            listener.goToPage(4);
        }
    }
}
