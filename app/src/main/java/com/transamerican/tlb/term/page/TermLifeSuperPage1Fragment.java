package com.transamerican.tlb.term.page;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.transamerican.tlb.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectViews;
import butterknife.OnClick;

/**
 * Created by Rice on 18/1/15.
 */
public class TermLifeSuperPage1Fragment extends TermPagerFragment {

    @InjectViews({R.id.detail_sub_img, R.id.detail_sub_img2, R.id.detail_sub_img3, R.id.detail_sub_img4})
    List<ImageView> images;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_term_super_page1, container, false);
        ButterKnife.inject(this, v);

        return v;
    }

    @Override
    public void onPageSelected() {
        resetImages();
    }

    @Override
    public void onPageExited() {

    }

    @Override
    public int getTitle() {
        return R.string.term_life_title_plan;
    }

    @Override
    public int getSlider() {
        return R.drawable.about_us_slide5_2;
    }

    @Override
    public boolean isVideoExist() {
        return false;
    }

    @Override
    public void onReplayClicked() {

    }

    public void resetImages() {
        for (ImageView image : images) {
            image.setSelected(false);
        }
    }

    public void selectImage(View v) {
        resetImages();
        v.setSelected(true);
    }

    @OnClick(R.id.detail_sub_img)
    public void onImage1Clicked(View v) {
        selectImage(v);
    }

    @OnClick(R.id.detail_sub_img2)
    public void onImage2Clicked(View v) {
        selectImage(v);
    }

    @OnClick(R.id.detail_sub_img3)
    public void onImage3Clicked(View v) {
        selectImage(v);
    }

    @OnClick(R.id.detail_sub_img4)
    public void onImage4Clicked(View v) {
        selectImage(v);
    }

}
