package com.transamerican.tlb.term.page;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transamerican.tlb.R;

import butterknife.ButterKnife;

/**
 * Created by Rice on 18/1/15.
 */
public class TermLifeRopPage1Fragment extends TermVideoPagerFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_term_rop_page1, container, false);
        ButterKnife.inject(this, v);

        isPageSelected = true; //temp fix for video not play when it is clicked from Home Menu
        initVideo();

        return v;
    }

    public String getVideo() {
        return "rop_30_02_demo_retina.mp4";
    }


    @Override
    public int getTitle() {
        return R.string.term_life_title_plan;
    }

    @Override
    public int getSlider() {
        return R.drawable.about_us_slide5_4;
    }

}
