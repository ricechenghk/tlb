package com.transamerican.tlb.term.page;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.transamerican.tlb.R;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Rice on 18/1/15.
 */
public class TermLifeSuperPage2Fragment extends TermVideoPagerFragment {

    @InjectView(R.id.detail_video_end_img)
    ImageView videoEndImage;

    protected boolean isSecondVideoPlayed = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_term_super_page2, container, false);
        ButterKnife.inject(this, v);

        initVideo();

        return v;
    }

    public String getVideo() {
        return "convertibility_option_demoA1_Retina.mp4";
    }


    @Override
    public int getTitle() {
        return R.string.term_life_title_plan;
    }

    @Override
    public int getSlider() {
        return R.drawable.about_us_slide5_3;
    }

    @Override
    public void handleAspectRatio() {
        super.handleAspectRatio();
        videoEndImage.setLayoutParams(layoutParams);
    }

    @Override
    public void onCompletion(MediaPlayer arg0) {
        super.onCompletion(arg0);
        if (!isSecondVideoPlayed) {
            videoEndImage.setImageResource(R.drawable.video_convertibility_option_demoa1_end);
            videoEndImage.setVisibility(View.VISIBLE);
        } else {
            videoEndImage.setImageResource(R.drawable.video_convertibility_option_demob1_end);
            videoEndImage.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPageSelected() {
        isSecondVideoPlayed = false;
        prepareVideo(getVideo());
        super.onPageSelected();
        videoEndImage.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.detail_video_end_img)
    public void onVideoImageClicked() {
        if (!isSecondVideoPlayed) {
            isSecondVideoPlayed = true;
            videoEndImage.setVisibility(View.INVISIBLE);
            prepareVideo("convertibility_option_demoB1_Retina.mp4");
        }

    }

    public void onReplayClicked() {
        onPageSelected();
    }

    public void prepareVideo(String videoName) {
        try {
            mediaPlayer.reset();
            AssetFileDescriptor descriptor = getActivity().getAssets().openFd(videoName);
            mediaPlayer.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();

            mediaPlayer.prepare();

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.start();
    }
}
