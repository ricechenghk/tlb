package com.transamerican.tlb.term.page;

import com.transamerican.tlb.R;
import com.transamerican.tlb.base.PagerFragment;

import butterknife.OnClick;

/**
 * Created by Rice on 19/1/15.
 */
public abstract class TermPagerFragment extends PagerFragment {

    @OnClick(R.id.term_life_what)
    public void onSectionWhatClicked() {
        if (listener != null) {
            listener.goToPage(1);
        }
    }

    @OnClick(R.id.term_life_trendsetter_super)
    public void onSectionSuperClicked() {
        if (listener != null) {
            listener.goToPage(2);
        }
    }

    @OnClick(R.id.term_life_trendsetter_rop)
    public void onSectionRopClicked() {
        if (listener != null) {
            listener.goToPage(4);
        }
    }
}
