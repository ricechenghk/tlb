package com.transamerican.tlb.term.page;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transamerican.tlb.R;

import butterknife.ButterKnife;

/**
 * Created by Rice on 18/1/15.
 */
public class TermLifeWhatFragment extends TermPagerFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_term_what_page1, container, false);
        ButterKnife.inject(this, v);

        return v;
    }

    @Override
    public void onPageSelected() {

    }

    @Override
    public void onPageExited() {

    }

    @Override
    public int getTitle() {
        return R.string.term_life_title_plan;
    }

    @Override
    public int getSlider() {
        return R.drawable.about_us_slide5_1;
    }

    @Override
    public boolean isVideoExist() {
        return false;
    }

    @Override
    public void onReplayClicked() {

    }

}
