package com.transamerican.tlb;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;
import android.widget.TextView;

import com.transamerican.tlb.sync.AppDataSource;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class TutorialBaseActivity extends Activity implements ViewPager.OnPageChangeListener {
    private static final String TAG = TutorialBaseActivity.class.getSimpleName();

    @InjectView(R.id.tutorial_show_next_time_checkbox)
    ImageView checkbox;

    @InjectView(R.id.tutorial_skip)
    TextView skip;

    TutorialPageAdapter adapter;

    public abstract int getLayoutResId();

    public abstract int[] getTutorialResId();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        ButterKnife.inject(this);

        ViewPager viewPager = (ViewPager) findViewById(R.id.tutorial_pager);
        adapter = new TutorialPageAdapter(this);
        adapter.setTutorialResId(getTutorialResId());
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(this);

        if (AppDataSource.getInstance().isShowTutorial(this)) {
            checkbox.setSelected(true);
        }
    }

    protected void onResume() {
        super.onResume();
    }

    @OnClick(R.id.tutorial_show_next_time)
    public void onShowNextTimeClicked() {
        checkbox.setSelected(!checkbox.isSelected());
    }

    @OnClick(R.id.tutorial_skip)
    public void onSkipClicked() {
        AppDataSource.getInstance().setShowTutorial(this, checkbox.isSelected());
        finish();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        int textResId = R.string.tutorial_skip;
        if (position == (adapter.getCount() - 1)) {
            textResId = R.string.tutorial_begin;
        }
        skip.setText(textResId);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
