package com.transamerican.tlb.model.pdf;

import java.util.ArrayList;

/**
 * Created by Rice on 16/1/15.
 */
public class LibraryPage {


    private String background;

    private ArrayList<OverlayObject> overlay_objects;

    private ArrayList<?> images;

    private String parentPath;

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public ArrayList<?> getImages() {
        return images;
    }

    public void setImages(ArrayList<?> images) {
        this.images = images;
    }

    public String getParentPath() {
        return parentPath;
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }

    public ArrayList<OverlayObject> getOverlay_objects() {
        return overlay_objects;
    }

    public void setOverlay_objects(ArrayList<OverlayObject> overlay_objects) {
        this.overlay_objects = overlay_objects;
    }
}
