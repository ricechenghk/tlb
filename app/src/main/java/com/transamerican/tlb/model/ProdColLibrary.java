package com.transamerican.tlb.model;

import com.j256.ormlite.table.DatabaseTable;
import com.transamerican.tlb.dao.ProdColLibraryDao;

@DatabaseTable(tableName = "prodcollibrary", daoClass = ProdColLibraryDao.class)
public class ProdColLibrary extends Library {

}
