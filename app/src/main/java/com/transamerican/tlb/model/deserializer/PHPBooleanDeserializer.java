package com.transamerican.tlb.model.deserializer;
import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class PHPBooleanDeserializer implements JsonDeserializer<Boolean> {
	public Boolean deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext context) throws JsonParseException {
		if (json.getAsJsonPrimitive().isString() || json.getAsJsonPrimitive().isNumber()) {
			int state = json.getAsJsonPrimitive().getAsInt();
			return (state == 1);
		} else {
			return json.getAsJsonPrimitive().getAsBoolean();
		}
	}
}
