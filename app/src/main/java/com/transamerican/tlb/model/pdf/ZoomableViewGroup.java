package com.transamerican.tlb.model.pdf;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewParent;
import android.widget.RelativeLayout;

public class ZoomableViewGroup extends RelativeLayout {

    public interface OnZoomableViewZoomListener {
        public void onZoomToScaleFactor(ZoomableViewGroup zvg, float scaleFactor);
    }

    public interface OnZoomableViewSingleTapListener {
        public void onSingleTapConfirmed(MotionEvent e);
    }

    private OnZoomableViewZoomListener onZoomListener;
    private OnZoomableViewSingleTapListener mSingleTapListener;

    private float mMaxScale = 3;
    private float mMinScale = 1;

    private float currentScaleFactor = 1;

    private static final int INVALID_POINTER_ID = 1;
    private int mActivePointerId = INVALID_POINTER_ID;
    protected OnGestureListener mGestureListener;
    protected GestureDetector mGestureDetector;

    // private float mScaleFactor = 1;
    private ScaleGestureDetector mScaleDetector;
    // private Matrix mScaleMatrix = new Matrix();
    // private Matrix mScaleMatrixInverse = new Matrix();

    // private float mPosX;
    // private float mPosY;
    // private Matrix mTranslateMatrix = new Matrix();
    // private Matrix mTranslateMatrixInverse = new Matrix();

    boolean topReached = false;
    boolean leftReached = false;
    boolean rightReached = false;
    boolean bottomReached = false;

    private Matrix mTransformationMatrix = new Matrix();
    private Matrix mCanvasMatrix = new Matrix();

    private Matrix mSearchMatrix;

    private float mLastTouchX;
    private float mLastTouchY;

    private float mFocusY;

    private float mFocusX;

    private float[] mInvalidateWorkingArray = new float[6];
    private float[] mDispatchTouchEventWorkingArray = new float[2];
    private float[] mOnTouchEventWorkingArray = new float[2];

    private boolean lock = false;
    private boolean setValue = false;

    int mLockLeft;
    int mLockRight;
    int mLockTop;
    int mLockBottom;

    private int didDraw;

    public void scrollToPosition(RectF rect) {
        float currentScaleFactor = getCurrentScaleFactor();

        Log.e("zoomScale", "didDraw " + didDraw);

        mSearchMatrix = new Matrix();
        mSearchMatrix.setTranslate(0, 0);
        mSearchMatrix.setScale(currentScaleFactor,
                currentScaleFactor, rect.centerX(), rect.centerY());

        if (didDraw > 0) {
            invalidate();
            requestLayout();
        }
    }

    public void resetDraw() {
        didDraw = 0;
    }

    public void preZoomScale(float scale, int centerX, int centerY) {
//   	scale = scale + 1;
//       if (scale > MAX_SCALE) {
//       	MAX_SCALE = scale + 3;
//       }

        lock = true;
        setValue = true;
        setMIN_SCALE(scale);
        mMaxScale = scale + 4;
        setCurrentScaleFactor(getMIN_SCALE());
        mTransformationMatrix.preScale(getCurrentScaleFactor(),
                getCurrentScaleFactor(), centerY, centerX);
        invalidate();
        requestLayout();
    }

    public void resetZoom() {
        init();

        setCurrentScaleFactor(1);
        setMIN_SCALE(1);
        mMaxScale = 3;

        mTransformationMatrix.preScale(getMIN_SCALE(),
                getMIN_SCALE(), 0, 0);
        invalidate();
        requestLayout();
    }

    public float getCurrentZoomScale() {
        return getCurrentScaleFactor();
    }

    public float getMinZoomScale() {
        return getMIN_SCALE();
    }

    public ZoomableViewGroup(Context context) {
        super(context);
        init();
    }

    public ZoomableViewGroup(Context context, AttributeSet attr) {
        super(context, attr);
        init();
    }

    public ZoomableViewGroup(Context context, AttributeSet attr, int def) {
        super(context, attr, def);
        init();
    }

    private void init() {
        mScaleDetector = new ScaleGestureDetector(getContext(),
                new ScaleListener());
        // mTranslateMatrix.setTranslate(0, 0);
        // mScaleMatrix.setScale(1, 1);
        mTransformationMatrix.setTranslate(0, 0);

        mGestureListener = new GestureListener();
        mGestureDetector = new GestureDetector(getContext(), mGestureListener,
                null, true);

        lock = false;
        mLockBottom = getHeight();
        mLockLeft = 0;
        mLockRight = getWidth();
        mLockTop = 0;

    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);

      /* int childCount = getChildCount();
       for (int i = 0; i < childCount; i++) {
           View child = getChildAt(i);
           if (child.getVisibility() != GONE) {
//           	int left = (int) (l + (mLeftRightPadding / 2));
//           	int left = l;
//           	int right = (int) (l + child.getMeasuredWidth() + (mLeftRightPadding / 2));
//           	child.layout(left, t, right,
//                     t + child.getMeasuredHeight());
               child.layout(l, t, l + child.getMeasuredWidth(),
                       t + child.getMeasuredHeight());
           }
       }*/
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                measureChild(child, widthMeasureSpec, heightMeasureSpec);
                // Log.e(null, "bfg  " + child.getLeft());
            }
        }
    }


    @Override
    protected void dispatchDraw(Canvas canvas) {
        // if (mScaleFactor < 1){
        // mScaleFactor = 1;
        // }
        // Log.i(null, " mScaleFactor   " + mScaleFactor);
        // Log.e(null, "mPosX  " + mPosX + " mPosY " + mPosY);
        // Log.e(null, "mFocusX  " + mFocusX + " mFocusY " + mFocusY);
        // Log.e(null, "getWidth  " + getWidth() + " getHeight " + getHeight());

        Log.e("zoomScale", "dispatchDraw called");

        RectF rect = new RectF(0, 0, getWidth(), getHeight());

        mCanvasMatrix = canvas.getMatrix();
        if (mSearchMatrix != null && didDraw > 0) {
            mTransformationMatrix = mSearchMatrix;
            mSearchMatrix = null;
        }
        mCanvasMatrix.preConcat(mTransformationMatrix);

        // Log.w(null, "top  " + rect.top + " left " + rect.left + "\n right " +
        // rect.right +" centerX " + rect.centerX());

        // float dx = mPosX * mScaleFactor, dy = mPosY * mScaleFactor;
        // float tx = dx - getWidth()/2 * mScaleFactor, ty = dy - getHeight()/2
        // * mScaleFactor;

        // Log.w(null, "pBounds()   " + canvas.getClipBounds());
        canvas.save();

        canvas.setMatrix(mCanvasMatrix);

        mLockBottom = getHeight();
        mLockTop = 0;
        if (setValue) {
            mLockLeft = canvas.getClipBounds().left;
            mLockRight = canvas.getClipBounds().right;
            setValue = false;
        }

//       Log.e("", "s " + canvas.getClipBounds().left + "\n s " + mLockLeft);
//       Log.e("", "Right " + canvas.getClipBounds().right + "\n s " + mLockRight);
//       Log.e("", "currentScaleFactor "  + getCurrentScaleFactor());
//       Log.e("", "min ScaleFactor "  + getMIN_SCALE());

        int leftBound = lock ? mLockLeft : 0;
        int topBound = 0;
        int rightBound = lock ? mLockRight : getWidth();
        int bottomBound = getHeight();

        if (canvas.getClipBounds().left <= leftBound) {
            leftReached = true;
            mTransformationMatrix.preTranslate(canvas.getClipBounds().left - leftBound, 0);
            mCanvasMatrix.preTranslate(canvas.getClipBounds().left - leftBound, 0);
        } else {
            leftReached = false;
        }

        if (canvas.getClipBounds().top <= 0) {
            topReached = true;
            mTransformationMatrix.preTranslate(0, canvas.getClipBounds().top);
            mCanvasMatrix.preTranslate(0, canvas.getClipBounds().top);
        } else {
            topReached = false;
        }

        if (canvas.getClipBounds().bottom >= getHeight()) {
            bottomReached = true;
            mTransformationMatrix.preTranslate(0, canvas.getClipBounds().bottom
                    - getHeight());
            mCanvasMatrix.preTranslate(0, canvas.getClipBounds().bottom
                    - getHeight());
        } else {
            bottomReached = false;
        }

        if (canvas.getClipBounds().right >= rightBound) {
            rightReached = true;
            mTransformationMatrix.preTranslate(canvas.getClipBounds().right
                    - rightBound, 0);
            mCanvasMatrix.preTranslate(canvas.getClipBounds().right
                    - rightBound, 0);
        } else {
            rightReached = false;
        }

        canvas.setMatrix(mCanvasMatrix);

        // canvas.setMatrix(mTransformationMatrix);

        // Log.w(null, ")   " + canvas.getClipBounds());

        super.dispatchDraw(canvas);
        canvas.restore();

        didDraw++;

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        mDispatchTouchEventWorkingArray[0] = ev.getX();
        mDispatchTouchEventWorkingArray[1] = ev.getY();
        mDispatchTouchEventWorkingArray = screenPointsToScaledPoints(mDispatchTouchEventWorkingArray);
        ev.setLocation(mDispatchTouchEventWorkingArray[0],
                mDispatchTouchEventWorkingArray[1]);
        return super.dispatchTouchEvent(ev);
    }

    /**
     * Although the docs say that you shouldn't override this, I decided to do
     * so because it offers me an easy way to change the invalidated area to my
     * likening.
     */
    @Override
    public ViewParent invalidateChildInParent(int[] location, Rect dirty) {

        mInvalidateWorkingArray[0] = dirty.left;
        mInvalidateWorkingArray[1] = dirty.top;
        mInvalidateWorkingArray[2] = dirty.right;
        mInvalidateWorkingArray[3] = dirty.bottom;

        mInvalidateWorkingArray = scaledPointsToScreenPoints(mInvalidateWorkingArray);
        dirty.set(Math.round(mInvalidateWorkingArray[0]),
                Math.round(mInvalidateWorkingArray[1]),
                Math.round(mInvalidateWorkingArray[2]),
                Math.round(mInvalidateWorkingArray[3]));

        float[] matVals = new float[9];
        mTransformationMatrix.getValues(matVals);
        float scaleFactor = matVals[0];
        location[0] *= scaleFactor;
        location[1] *= scaleFactor;
        return super.invalidateChildInParent(location, dirty);
    }

    private float[] scaledPointsToScreenPoints(float[] a) {
        // mScaleMatrix.mapPoints(a);
        // mTranslateMatrix.mapPoints(a);
        mTransformationMatrix.mapPoints(a);

        return a;
    }

    private float[] screenPointsToScaledPoints(float[] a) {
        // mTranslateMatrixInverse.mapPoints(a);
        // mScaleMatrixInverse.mapPoints(a);
        Matrix inverse = new Matrix();
        boolean result = mTransformationMatrix.invert(inverse);
        inverse.mapPoints(a);
        return a;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        mOnTouchEventWorkingArray[0] = ev.getX();
        mOnTouchEventWorkingArray[1] = ev.getY();

        mOnTouchEventWorkingArray = scaledPointsToScreenPoints(mOnTouchEventWorkingArray);

        ev.setLocation(mOnTouchEventWorkingArray[0],
                mOnTouchEventWorkingArray[1]);
        mScaleDetector.onTouchEvent(ev);

        if (!mScaleDetector.isInProgress()) {
            mGestureDetector.onTouchEvent(ev);
        }

        final int action = ev.getAction();
        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: {
                final float x = ev.getX();
                final float y = ev.getY();

                mLastTouchX = x;
                mLastTouchY = y;

                // Save the ID of this pointer
                mActivePointerId = ev.getPointerId(0);
                break;
            }

            case MotionEvent.ACTION_MOVE: {
                // Find the index of the active pointer and fetch its position
                final int pointerIndex = ev.findPointerIndex(mActivePointerId);
                final float x = ev.getX(pointerIndex);
                final float y = ev.getY(pointerIndex);

                float dx = x - mLastTouchX;
                float dy = y - mLastTouchY;

                // finger up  -dy , content move down
                // finger down + dy , content move up
                // finger left +dx , content move right
                // finger right -dx , content move left

                if (topReached && dy > 0) {
                    dy = 0;
                } else {
                    topReached = false;
                }

                if (leftReached && dx > 0) {
                    dx = 0;
                } else {
                    leftReached = false;
                }

                if (rightReached && dx < 0) {
                    dx = 0;
                } else {
                    rightReached = false;
                }

                if (bottomReached && dy < 0) {
                    dy = 0;
                } else {
                    bottomReached = false;
                }

                mTransformationMatrix.preTranslate(dx / getCurrentScaleFactor(), dy / getCurrentScaleFactor());

           /*
            * mPosX += dx; mPosY += dy;
            * 
            * mTranslateMatrix.preTranslate(dx, dy);
            * mTranslateMatrix.invert(mTranslateMatrixInverse);
            */

                mLastTouchX = x;
                mLastTouchY = y;

                invalidate();
                break;
            }

            case MotionEvent.ACTION_UP: {
                mActivePointerId = INVALID_POINTER_ID;

                break;
            }

            case MotionEvent.ACTION_CANCEL: {
                mActivePointerId = INVALID_POINTER_ID;
                break;
            }

            case MotionEvent.ACTION_POINTER_UP: {
                // Extract the index of the pointer that left the touch sensor
                final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                final int pointerId = ev.getPointerId(pointerIndex);
                if (pointerId == mActivePointerId) {
                    // This was our active p ointer going up. Choose a new
                    // active pointer and adjust accordingly.
                    final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                    mLastTouchX = ev.getX(newPointerIndex);
                    mLastTouchY = ev.getY(newPointerIndex);
                    mActivePointerId = ev.getPointerId(newPointerIndex);
                }
                break;
            }
        }
        return true;
    }

    public OnZoomableViewZoomListener getOnZoomListener() {
        return onZoomListener;
    }

    public void setOnZoomListener(OnZoomableViewZoomListener onZoomListener) {
        this.onZoomListener = onZoomListener;
    }

    public void setSingleTapListener(
            OnZoomableViewSingleTapListener singleTapListener) {
        this.mSingleTapListener = singleTapListener;
    }

    public float getMIN_SCALE() {
        return mMinScale;
    }

    public void setMIN_SCALE(float mIN_SCALE) {
        mMinScale = mIN_SCALE;
    }

    public float getCurrentScaleFactor() {
        return currentScaleFactor;
    }

    public void setCurrentScaleFactor(float currentScaleFactor) {
        this.currentScaleFactor = currentScaleFactor;
    }

    private class ScaleListener extends
            ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            // mScaleFactor *= detector.getScaleFactor();
            if (detector.isInProgress()) {
                mFocusX = detector.getFocusX();
                mFocusY = detector.getFocusY();
            }
            mTransformationMatrix.preScale(detector.getScaleFactor(),
                    detector.getScaleFactor(), mFocusX, mFocusY);
            float[] matVals = new float[9];
            mTransformationMatrix.getValues(matVals);
            float scaleFactor = matVals[0];
            setCurrentScaleFactor(Math.min(mMaxScale,
                    Math.max(getMIN_SCALE(), scaleFactor)));
            mTransformationMatrix.preScale(getCurrentScaleFactor() / scaleFactor,
                    getCurrentScaleFactor() / scaleFactor, mFocusX, mFocusY);

            if (getOnZoomListener() != null) {
                getOnZoomListener().onZoomToScaleFactor(ZoomableViewGroup.this, getCurrentScaleFactor());
            }

            // mTransformationMatrix.
            // mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 3.0f));
            // mScaleMatrix.setScale(mScaleFactor, mScaleFactor, mFocusX,
            // mFocusY);
            // mScaleMatrix.invert(mScaleMatrixInverse);
            invalidate();
            requestLayout();

            return true;
        }
    }

    public class GestureListener extends
            GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {

            if (null != mSingleTapListener) {
                mSingleTapListener.onSingleTapConfirmed(e);
            }

            return super.onSingleTapConfirmed(e);
        }
    }

}