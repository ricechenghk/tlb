package com.transamerican.tlb.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.transamerican.tlb.dao.FieldCommLibraryDao;

import java.util.Date;

@DatabaseTable(tableName = "fieldcommlibrary", daoClass = FieldCommLibraryDao.class)
public class FieldCommLibrary extends Library {

    @DatabaseField
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
