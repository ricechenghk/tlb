package com.transamerican.tlb.model.deserializer;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;

public class PHPDateDeserializer implements JsonDeserializer<Date> {
	public Date deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext context) {
		Date date = new Date();
		try {
			date.setTime(json.getAsJsonPrimitive().getAsLong() * 1000);
		} catch (Exception e) {
//			e.printStackTrace();
			
			// try parse as String "2013-12-27 17:15:58"
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
			try {
				date = sdf.parse(json.getAsJsonPrimitive().getAsString()); 
			} catch (Exception e1) {
			    System.err.println("There's an error in the Date!");
				e.printStackTrace();
			}   
		}
		return date;
	}
}
