package com.transamerican.tlb.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.transamerican.tlb.dao.ProdColCategoryDao;

@DatabaseTable(tableName = "prodcolcategory", daoClass = ProdColCategoryDao.class)
public class ProdColCategory extends LibraryCategory {

    @DatabaseField
    @SerializedName("short_form")
    private String shortForm;

    @DatabaseField
    @SerializedName("interest_rate")
    private String interestRate;

    public String getShortForm() {
        return shortForm;
    }

    public void setShortForm(String shortForm) {
        this.shortForm = shortForm;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }
}
