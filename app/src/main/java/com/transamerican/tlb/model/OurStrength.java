package com.transamerican.tlb.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.transamerican.tlb.dao.OurStrengthDao;

import java.io.Serializable;
import java.util.Date;

import hk.com.playmore.syncframework.util.DatabaseModel;

@DatabaseTable(tableName = "ourstrength", daoClass = OurStrengthDao.class)
public class OurStrength implements DatabaseModel, Serializable {

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    @SerializedName("pdf_en")
    private String pdfEn;

    @DatabaseField
    @SerializedName("pdf_cn")
    private String pdfCn;

    @DatabaseField
    @SerializedName("pdf_zh")
    private String pdfZh;

    @DatabaseField
    @SerializedName("create_time")
    private Date createTime;

    @DatabaseField
    @SerializedName("modify_time")
    private Date modifyTime;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPdfEn() {
        return pdfEn;
    }

    public void setPdfEn(String pdfEn) {
        this.pdfEn = pdfEn;
    }

    public String getPdfCn() {
        return pdfCn;
    }

    public void setPdfCn(String pdfCn) {
        this.pdfCn = pdfCn;
    }

    public String getPdfZh() {
        return pdfZh;
    }

    public void setPdfZh(String pdfZh) {
        this.pdfZh = pdfZh;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public long getUserReadTime() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setUserReadTime(long userReadTime) {
        // TODO Auto-generated method stub

    }

}