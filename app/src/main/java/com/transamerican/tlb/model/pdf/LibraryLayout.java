package com.transamerican.tlb.model.pdf;

import java.util.List;

public class LibraryLayout {

    private List<? extends LibraryPage> pages;
    private List<String> files;
    private String dpi;

    private String parentPath;

    public List<? extends LibraryPage> getPages() {
        return pages;
    }

    public void setPages(List<LibraryPage> pages) {
        this.pages = pages;
    }

    public List<String> getFiles() {
        return files;
    }

    public void setFiles(List<String> files) {
        this.files = files;
    }

    public String getDpi() {
        return dpi;
    }

    public void setDpi(String dpi) {
        this.dpi = dpi;
    }

    public String getParentPath() {
        return parentPath;
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }
}
