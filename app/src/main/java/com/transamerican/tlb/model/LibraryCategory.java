package com.transamerican.tlb.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.util.Date;

import hk.com.playmore.syncframework.util.DatabaseModel;

public class LibraryCategory implements DatabaseModel {

    public static int PRODUCT_COLLATERALS = 0;
    public static int FIELD_COMMUNICATIONS = 1;
    public static int FORMS = 2;
    public static int UNDERWRITING_GUIDE = 3;
    public static int TRANSWARE = 4;

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String category;

    @DatabaseField
    private String title;

    @DatabaseField
    private int sort;

    @DatabaseField
    @SerializedName("create_time")
    private Date createTime;

    @DatabaseField
    @SerializedName("modify_time")
    private Date modifyTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public long getUserReadTime() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setUserReadTime(long userReadTime) {
        // TODO Auto-generated method stub

    }


    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


}
