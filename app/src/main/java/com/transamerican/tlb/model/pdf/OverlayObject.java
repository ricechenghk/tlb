package com.transamerican.tlb.model.pdf;


public class OverlayObject {

    private Integer origin_x;
    private Integer origin_y;
    private Integer size_width;
    private Integer size_height;
    private String type;
    private String url;

    public Integer getOrigin_x() {
        return origin_x;
    }

    public void setOrigin_x(Integer origin_x) {
        this.origin_x = origin_x;
    }

    public Integer getOrigin_y() {
        return origin_y;
    }

    public void setOrigin_y(Integer origin_y) {
        this.origin_y = origin_y;
    }

    public Integer getSize_width() {
        return size_width;
    }

    public void setSize_width(Integer size_width) {
        this.size_width = size_width;
    }

    public Integer getSize_height() {
        return size_height;
    }

    public void setSize_height(Integer size_height) {
        this.size_height = size_height;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
