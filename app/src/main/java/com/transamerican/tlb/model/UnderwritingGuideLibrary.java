package com.transamerican.tlb.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.transamerican.tlb.dao.UnderwritingGuideLibraryDao;

@DatabaseTable(tableName = "underwritingguidelibrary", daoClass = UnderwritingGuideLibraryDao.class)
public class UnderwritingGuideLibrary extends Library {

    @DatabaseField
    private String location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
