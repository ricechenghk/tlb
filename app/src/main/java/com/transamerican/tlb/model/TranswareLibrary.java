package com.transamerican.tlb.model;

import com.j256.ormlite.table.DatabaseTable;
import com.transamerican.tlb.dao.TranswareLibraryDao;

@DatabaseTable(tableName = "transwarelibrary", daoClass = TranswareLibraryDao.class)
public class TranswareLibrary extends Library {

}
