package com.transamerican.tlb.model;

import com.j256.ormlite.table.DatabaseTable;
import com.transamerican.tlb.dao.FormsLibraryDao;

@DatabaseTable(tableName = "formslibrary", daoClass = FormsLibraryDao.class)
public class FormsLibrary extends Library {

}
