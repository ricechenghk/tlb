package com.transamerican.tlb.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;
import java.util.Date;

import hk.com.playmore.syncframework.util.DatabaseModel;

/**
 * Created by Rice on 13/1/15.
 */
public class Library implements DatabaseModel, Serializable {

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String title;

    @DatabaseField
    private String category;

    @DatabaseField
    private String filesize;

    @DatabaseField
    private String language;

    @DatabaseField
    private String doc;

    @DatabaseField
    @SerializedName("doc_url")
    private String docUrl;

    @DatabaseField
    private String status;

    @DatabaseField
    private int sort;

    @DatabaseField
    @SerializedName("create_time")
    private Date createTime;

    @DatabaseField
    @SerializedName("modify_time")
    private Date modifyTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFilesize() {
        return filesize;
    }

    public void setFilesize(String filesize) {
        this.filesize = filesize;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public String getDocUrl() {
        return docUrl;
    }

    public void setDocUrl(String docUrl) {
        this.docUrl = docUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public long getUserReadTime() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setUserReadTime(long userReadTime) {
        // TODO Auto-generated method stub

    }

}
