package com.transamerican.tlb.model.pdf;

import android.content.Context;
import android.graphics.Color;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;

public class OverlayObjectCreator {


    public static RelativeLayout.LayoutParams calculateItemLayoutParams(OverlayObject _obj, int screenWidth, int screenHeight, float useRatio, int imageWidth, int imageHeight) {
        float imageSizeWidth = imageWidth / useRatio;
        float imageSizeHeight = imageHeight / useRatio;

        float resultWidth = _obj.getSize_width() / useRatio;
        float resultHeight = _obj.getSize_height() / useRatio;
        float resultX = (_obj.getOrigin_x() / useRatio) + ((screenWidth - imageSizeWidth) / 2);
        float resultY = (_obj.getOrigin_y() / useRatio) + ((screenHeight - imageSizeHeight) / 2);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) resultWidth, (int) resultHeight);
        params.leftMargin = (int) resultX;
        params.topMargin = (int) resultY;
        return params;
    }

    public static ImageButton createOverlayObjectButton(Context ctx, OverlayObject _item, float useRatio, int screenWidth, int screenHeight, int magazinePageWidth, int magazinePageHeight) {
        ImageButton btn = new ImageButton(ctx);
        btn.setScaleType(ScaleType.CENTER_INSIDE);
        btn.setBackgroundColor(Color.TRANSPARENT);
        btn.setLayoutParams(calculateItemLayoutParams(_item, screenWidth, screenHeight, useRatio, magazinePageWidth, magazinePageHeight));

        return btn;
    }

}
