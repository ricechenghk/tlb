package com.transamerican.tlb.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.transamerican.tlb.dao.ContactDao;

import java.io.Serializable;
import java.util.Date;

import hk.com.playmore.syncframework.util.DatabaseModel;

@DatabaseTable(tableName = "contact", daoClass = ContactDao.class)
public class Contact implements DatabaseModel, Serializable {

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String name;

    @DatabaseField
    private String position;

    @DatabaseField
    private String category;

    @DatabaseField
    private String tel;

    @DatabaseField
    private String email;

    @DatabaseField
    private String status;

    @DatabaseField
    private String active;

    @DatabaseField
    @SerializedName("create_time")
    private Date createTime;

    @DatabaseField
    @SerializedName("modify_time")
    private Date modifyTime;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public long getUserReadTime() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setUserReadTime(long userReadTime) {
        // TODO Auto-generated method stub

    }

}