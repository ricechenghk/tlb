package com.transamerican.tlb.model.pdf;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.aphidmobile.flip.FlipViewController;

public class PMFlipViewController extends FlipViewController {

    private boolean canFlip = true;
    private onGetMeasureSizeListener onGetMeasureSizeListener;

    public interface onGetMeasureSizeListener {
        public void onGetMeasureSize(int parentWidth, int parentHeight);
    }

    public void setCanFlip(boolean _canFlip) {
        this.canFlip = _canFlip;
    }

    public PMFlipViewController(Context context, int flipOrientation) {
        super(context, flipOrientation);
        // TODO Auto-generated constructor stub
    }

    public PMFlipViewController(Context context) {
        super(context);
    }

    public PMFlipViewController(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PMFlipViewController(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub

        if (event.getPointerCount() < 2 && this.canFlip) {

            return super.onTouchEvent(event);
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub

        if (event.getPointerCount() < 2 && this.canFlip) {
            return super.onInterceptTouchEvent(event);
        }
        return false;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // TODO Auto-generated method stub
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        onGetMeasureSize(parentWidth, parentHeight);
    }

    private void onGetMeasureSize(int parentWidth, int parentHeight) {
        if (getOnGetMeasureSizeListener() != null) {
            getOnGetMeasureSizeListener().onGetMeasureSize(parentWidth, parentHeight);
        }
    }

    public onGetMeasureSizeListener getOnGetMeasureSizeListener() {
        return onGetMeasureSizeListener;
    }

    public void setOnGetMeasureSizeListener(onGetMeasureSizeListener onGetMeasureSizeListener) {
        this.onGetMeasureSizeListener = onGetMeasureSizeListener;
    }

    public void removeOnGetMeasureSizeListener() {
        this.onGetMeasureSizeListener = null;
    }
}
