package com.transamerican.tlb;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.transamerican.tlb.sync.AppDataSource;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Rice on 25/1/15.
 */
public class MainApplication extends Application {

    private static final String PROPERTY_ID = "UA-60686848-1";

    public static int GENERAL_TRACKER = 0;

    public enum TrackerName {
        APP_TRACKER, GLOBAL_TRACKER, ECOMMERCE_TRACKER,
    }

    public HashMap<TrackerName, Tracker> mTrackers = new HashMap<>();

    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/helvetica-neue-55-regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );

    }

//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(base);
////        MultiDex.install(this);
//    }

    synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
                    : analytics.newTracker(R.xml.ecommerce_tracker);
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }

    public void sendTrackerEvent(int categoryId) {
        // Get tracker.
        Tracker t = getTracker(TrackerName.APP_TRACKER);
// Build and send an Event.
        t.send(new HitBuilders.EventBuilder()
                .setCategory(getString(categoryId))
                .setAction(getString(categoryId))
                .setLabel("View - " + AppDataSource.getInstance().getLocation(this))
                .build());
    }
}
