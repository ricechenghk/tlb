package com.transamerican.tlb.sync;

import android.content.Context;

import com.transamerican.tlb.model.Contact;
import com.transamerican.tlb.model.ContactCategory;
import com.transamerican.tlb.model.FieldCommCategory;
import com.transamerican.tlb.model.FieldCommLibrary;
import com.transamerican.tlb.model.FormsCategory;
import com.transamerican.tlb.model.FormsLibrary;
import com.transamerican.tlb.model.OurStrength;
import com.transamerican.tlb.model.ProdColCategory;
import com.transamerican.tlb.model.ProdColLibrary;
import com.transamerican.tlb.model.TranswareCategory;
import com.transamerican.tlb.model.TranswareLibrary;
import com.transamerican.tlb.model.UnderwritingGuideCategory;
import com.transamerican.tlb.model.UnderwritingGuideLibrary;

import java.util.Arrays;
import java.util.List;

import hk.com.playmore.syncframework.helper.PMDatabaseHelper;


public class AppDatabaseHelper extends PMDatabaseHelper {

    private static final String DATABASE_NAME = "basf.db";
    private static final int DATABASE_VERSION = 1;

    public AppDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, DATABASE_VERSION);
    }

    @Override
    protected List<Class<?>> getClasses() {
        Class<?> classes[] = {
                ProdColCategory.class,
                ProdColLibrary.class,
                FieldCommCategory.class,
                FieldCommLibrary.class,
                FormsCategory.class,
                FormsLibrary.class,
                UnderwritingGuideCategory.class,
                UnderwritingGuideLibrary.class,
                TranswareCategory.class,
                TranswareLibrary.class,
                OurStrength.class,
                ContactCategory.class,
                Contact.class

        };
        return Arrays.asList(classes);
    }

}
