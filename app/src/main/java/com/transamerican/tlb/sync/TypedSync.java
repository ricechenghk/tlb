package com.transamerican.tlb.sync;

import hk.com.playmore.syncframework.util.DatabaseModel;
import hk.com.playmore.syncframework.util.DatabaseSyncer;
import hk.com.playmore.syncframework.util.DatabaseSyncer.DatabaseSyncerListener;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import android.content.Context;

import com.google.gson.Gson;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

public class TypedSync <T extends DatabaseModel> {
	
	private Gson gson;
	
	public TypedSync(Gson gson) {
		this.gson = gson;
	}	

	public HashSet<T> sync(Context ctx, Class<T> cls, Class<T[]> arrayCls, String object, DatabaseSyncerListener mDatabaseSyncerListener) {
		return sync(ctx, cls, arrayCls, null, object, mDatabaseSyncerListener);
	}

	public HashSet<T> sync(Context ctx, Class<T> cls, Class<T[]> arrayCls, String object, List<T> locals, DatabaseSyncerListener mDatabaseSyncerListener) {
		return sync(ctx, cls, arrayCls, locals, object, mDatabaseSyncerListener);
	}
	
	public HashSet<T> sync(Context ctx, Class<T> cls, Class<T[]> arrayCls, List<T> locals, String object) {
		return sync(ctx, cls, arrayCls, locals, object, null);
	}
	
	private HashSet<T> sync(Context ctx, Class<T> cls,
			Class<T[]> arrayCls, List<T> locals, 
			String object, DatabaseSyncerListener mDatabaseSyncerListener) {
		try {
			AppDatabaseHelper helper = (AppDatabaseHelper) OpenHelperManager.getHelper(
					 ctx, AppDatabaseHelper.class);
			DatabaseSyncer<T> syncer = new DatabaseSyncer<T>();
			Dao<T, String> dao = helper.getDao(cls);
			if (locals == null) {
				locals = dao.queryForAll();
			}
			
			Gson gson = getGson();
			List<T> servers = (List<T>) Arrays.asList(gson.fromJson(
					object, arrayCls));

			return syncer.sync(dao, locals, servers, mDatabaseSyncerListener);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Gson getGson() {
		return gson;
	}

	public void setGson(Gson gson) {
		this.gson = gson;
	}
}
