package com.transamerican.tlb.sync;

import com.transamerican.tlb.BuildConfig;

import org.apache.http.HttpException;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import hk.com.playmore.syncframework.helper.PMDatabaseHelper;
import hk.com.playmore.syncframework.service.PMSyncService;
import hk.com.playmore.syncframework.util.HttpRequest;
import hk.com.playmore.util.AeSimpleSHA1;


public class AppSyncService extends PMSyncService {

    @Override
    protected PMDatabaseHelper getNewDatabaseHelper() {
        return new AppDatabaseHelper(this);
    }

    @Override
    public HttpRequest getNewHttpRequest() {
        HttpRequest request = new HttpRequest();
        request.setBaseURL(getBaseUrl());
        DefaultHttpClient httpClient = (DefaultHttpClient) request.getHttpClient();
        httpClient.addRequestInterceptor(new HttpRequestInterceptor() {
            @Override
            public void process(org.apache.http.HttpRequest request,
                                HttpContext context) throws HttpException, IOException {
                /*
                SharedPreferences prefs = getBaseContext().getSharedPreferences(AppDataSource.Prefs.TARGUS_PREFS, 0);
				String userId = prefs.getString(AppDataSource.Prefs.USER_ID, null);
				
				if (!request.containsHeader("X-APP-USER-ID") && userId != null) {
					request.addHeader("X-APP-USER-ID", userId);
					
				} else if (request.containsHeader("X-APP-USER-ID") && userId == null){
					request.removeHeaders("X-APP-USER-ID");
				}
			
				String userToken = prefs.getString(AppDataSource.Prefs.USER_TOKEN, null);
				if (!request.containsHeader("X-APP-USER-TOKEN") && userToken != null) {
					request.addHeader("X-APP-USER-TOKEN", userToken);
					
				} else if (request.containsHeader("X-APP-USER-DPI") && userToken == null){
					request.removeHeaders("X-APP-USER-DPI");
				}
				
				
				if (!request.containsHeader("X-KA-DEVICE-DPI")) {
					request.addHeader("X-KA-DEVICE-DPI", "1");
				}*/
            }
        });
        return request;
    }

    public static String getBaseUrl() {
//		return "http://demo.pm/transamerica/site/";
//        return "http://111.221.93.24/TLB/";
        return "http://111.221.94.160//TLB/";
    }

    public static String getCDNUrl() {
        if (BuildConfig.DEBUG) {
            //return "https://az438682.vo.msecnd.net/upload/";
            return null;
        } else {
            return null;
        }
    }

    public static String hashedUploadFile(String file) {
        if (file.length() <= 3) {
            return "";
        }
        String resultImagePath = null;
        try {
            resultImagePath = AeSimpleSHA1.SHA1(file);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String imagePathExtension = file.substring(file.lastIndexOf(".") + 1);
        return resultImagePath + "." + imagePathExtension.toLowerCase();
    }

    public static String hashedCDNFileURL(String file) {
        return getCDNUrl() + hashedUploadFile(file);
    }

    public static String baseUrlByPrepending(String file) {
        return getBaseUrl() + "/" + file;
    }

}
