package com.transamerican.tlb.sync;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import com.transamerican.tlb.model.LibraryCategory;
import com.transamerican.tlb.util.LibraryExternalStorageHelper;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class LibraryDownloadTask extends AsyncTask<String, Integer, String> {

    public static final String BROADCAST_ACTION = "DownloadFile.BROADCAST_ACTION";

    public static final String BROADCAST_TYPE = "DownloadFile.BROADCAST_TYPE";
    public static final String BROADCAST_ERROR = "DownloadFile.BROADCAST_ERROR";
    public static final String BROADCAST_PROGRESS = "DownloadFile.BROADCAST_PROGRESS";

    public static final String BROADCAST_STATE = "DownloadFile.BROADCAST_STATE";
    public static final String STATE_DOWNLOADING = "DownloadFile.STATE_DOWNLOADING";
    public static final String STATE_FINISH = "DownloadFile.STATE_FINISH";

    private LibraryExternalStorageHelper storageHelper = new LibraryExternalStorageHelper();

    private int section;
    private String libId;
    private String mBroadcastType;
    private Context mCtx;

    private Runnable onFinishRunnable;

    public LibraryDownloadTask(Context ctx, int section, String libId, String broadcast) {
        super();
        this.mCtx = ctx;
        this.section = section;
        this.libId = libId;
        this.mBroadcastType = broadcast;
    }

    @Override
    protected String doInBackground(String... infos) {

        String urlToDownload = resolveURLWithSection();
        File downloadFile = storageHelper.getLibraryZipFile(mCtx, section, libId);
        String storePath = downloadFile.getAbsolutePath();

        try {
            URL url = new URL(urlToDownload);
            URLConnection connection = url.openConnection();
            connection.connect();

            int fileLength = connection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(
                    (InputStream) connection.getContent());
            OutputStream output = new FileOutputStream(storePath);

            byte data[] = new byte[1024];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                Intent intent = new Intent(mBroadcastType);
                intent.putExtra(BROADCAST_TYPE, mBroadcastType);
                intent.putExtra(BROADCAST_STATE, STATE_DOWNLOADING);
                intent.putExtra(BROADCAST_PROGRESS,
                        (int) (total * 100 / fileLength));
                LocalBroadcastManager.getInstance(mCtx)
                        .sendBroadcast(intent);

                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();
        } catch (IOException e) {
            e.printStackTrace();

            Intent intent = new Intent(mBroadcastType);
            intent.putExtra(BROADCAST_TYPE, mBroadcastType);
            intent.putExtra(BROADCAST_STATE, STATE_FINISH);
            intent.putExtra(BROADCAST_ERROR, e.toString());

            LocalBroadcastManager.getInstance(mCtx)
                    .sendBroadcast(intent);
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);

        try {
            storageHelper.unzipLibrary(mCtx, section, libId);
        } catch (Exception e) {
            e.printStackTrace();
            Intent intent = new Intent(mBroadcastType);
            intent.putExtra(BROADCAST_TYPE, mBroadcastType);
            intent.putExtra(BROADCAST_STATE, STATE_FINISH);
            intent.putExtra(BROADCAST_ERROR, e.toString());

            LocalBroadcastManager.getInstance(mCtx)
                    .sendBroadcast(intent);
            return;
        }

        Intent intent = new Intent(mBroadcastType);
        intent.putExtra(BROADCAST_TYPE, mBroadcastType);
        intent.putExtra(BROADCAST_STATE, STATE_FINISH);
        LocalBroadcastManager.getInstance(mCtx)
                .sendBroadcast(intent);

        if (getOnFinishRunnable() != null) {
            getOnFinishRunnable().run();
        }

    }

    public Runnable getOnFinishRunnable() {
        return onFinishRunnable;
    }

    public void setOnFinishRunnable(Runnable onFinishRunnable) {
        this.onFinishRunnable = onFinishRunnable;
    }

    public String resolveURLWithSection() {
        String prefix = AppDataSource.PATH_LIB_PROD_COL;
        if (section == LibraryCategory.PRODUCT_COLLATERALS) {
            prefix = AppDataSource.PATH_LIB_PROD_COL;
        } else if (section == LibraryCategory.FIELD_COMMUNICATIONS) {
            prefix = AppDataSource.PATH_LIB_FIELD_COMM;
        } else if (section == LibraryCategory.FORMS) {
            prefix = AppDataSource.PATH_LIB_FORMS;
        } else if (section == LibraryCategory.UNDERWRITING_GUIDE) {
            prefix = AppDataSource.PATH_LIB_UNDERWRITEING_GUIDE;
        } else if (section == LibraryCategory.TRANSWARE) {
            prefix = AppDataSource.PATH_LIB_TRANSWARE;
        }

        String localPath = prefix + "/edition/id/" + libId;
        String fullPath = AppSyncService.getBaseUrl() + localPath;
        return fullPath;
    }
}
