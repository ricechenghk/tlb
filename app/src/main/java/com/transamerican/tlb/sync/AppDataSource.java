package com.transamerican.tlb.sync;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.transamerican.tlb.R;
import com.transamerican.tlb.dao.ContactCategoryDao;
import com.transamerican.tlb.dao.ContactDao;
import com.transamerican.tlb.dao.FieldCommCategoryDao;
import com.transamerican.tlb.dao.FieldCommLibraryDao;
import com.transamerican.tlb.dao.FormsCategoryDao;
import com.transamerican.tlb.dao.FormsLibraryDao;
import com.transamerican.tlb.dao.OurStrengthDao;
import com.transamerican.tlb.dao.ProdColCategoryDao;
import com.transamerican.tlb.dao.ProdColLibraryDao;
import com.transamerican.tlb.dao.TranswareCategoryDao;
import com.transamerican.tlb.dao.TranswareLibraryDao;
import com.transamerican.tlb.dao.UnderwritingGuideCategoryDao;
import com.transamerican.tlb.dao.UnderwritingGuideLibraryDao;
import com.transamerican.tlb.model.Contact;
import com.transamerican.tlb.model.ContactCategory;
import com.transamerican.tlb.model.FieldCommCategory;
import com.transamerican.tlb.model.FieldCommLibrary;
import com.transamerican.tlb.model.FormsCategory;
import com.transamerican.tlb.model.FormsLibrary;
import com.transamerican.tlb.model.Library;
import com.transamerican.tlb.model.LibraryCategory;
import com.transamerican.tlb.model.LoginResponse;
import com.transamerican.tlb.model.OurStrength;
import com.transamerican.tlb.model.ProdColCategory;
import com.transamerican.tlb.model.ProdColLibrary;
import com.transamerican.tlb.model.TranswareCategory;
import com.transamerican.tlb.model.TranswareLibrary;
import com.transamerican.tlb.model.UnderwritingGuideCategory;
import com.transamerican.tlb.model.UnderwritingGuideLibrary;
import com.transamerican.tlb.model.deserializer.PHPBooleanDeserializer;
import com.transamerican.tlb.model.deserializer.PHPDateDeserializer;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hk.com.playmore.syncframework.helper.PMDataSource;
import hk.com.playmore.syncframework.util.RunnableArgument;
import hk.com.playmore.syncframework.util.RunnableJSON;
import hk.com.playmore.syncframework.util.SerializableBasicNameValuePair;
import hk.com.playmore.syncframework.util.Syncable;
import hk.com.playmore.util.NetworkHelper;

public class AppDataSource extends PMDataSource {

    public static final String PREFS = "com.transamerican.tlb";
    public static final String PREFS_SHOW_PUSH_ALERT = "com.transamerican.tlb.show_push_alert";
    public static final String PREFS_SHOW_TUTORIAL = "com.transamerican.tlb.show_tutorial";
    //    public static final String PREFS_LAST_LOGIN_USER = "com.transamerican.tlb.last_login_user";
    public static final String PREFS_LAST_LOGIN_TIME = "com.transamerican.tlb.last_login_time";
    public static final String PREFS_IS_LOGIN = "com.transamerican.tlb.is_login";
    public static final String PREFS_LOCATION = "com.transamerican.tlb.location";
    public static final String PREFS_ENABLE_NOTIFICATION = "com.transamerican.tlb.enable_notification";
    public static final String PREFS_LOGIN_USER = "com.transamerican.tlb.login_user";
    public static final String PREFS_LOGIN_PWD = "com.transamerican.tlb.login_pwd";

    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";

    public static final String API_HAS_ERROR_KEY = "hasError";
    public static final String API_RESPONSE_OBJ_KEY = "ResponseObj";
    public static final String API_ITEMS_KEY = "Items";
    public static final String API_RESULT_OBJ_KEY = "result";

    public static final String LANG_EN = "en";
    public static final String LANG_ZH = "tc";

    public static final String PATH_LIB_PROD_COL = "libraryProdCol";
    public static final String PATH_LIB_FIELD_COMM = "libraryFieldComm";
    public static final String PATH_LIB_FORMS = "libraryForms";
    public static final String PATH_LIB_UNDERWRITEING_GUIDE = "libraryUnderwriting";
    public static final String PATH_LIB_TRANSWARE = "libraryTransware";

    private static AppDataSource _instance;

    protected AppDataSource() {
        super(AppSyncService.class);
    }

    public static AppDataSource getInstance() {
        if (_instance == null) {
            _instance = new AppDataSource();
        }
        return _instance;
    }

    protected RunnableArgument defaultFailureRunnable(final Context ctx, final RunnableArgument done) {
        return new RunnableArgument() {

            @Override
            public void run(String object) {
                if (!NetworkHelper.isOnline(ctx)) {
                    object = ctx.getString(R.string.offline_alert_message);
                } else if (object == null) {
                    object = "An error occured, please try again later";
                }
                if (done != null) {
                    done.run(object);
                }
            }
        };
    }

    //	protected DatabaseSyncerListener getDefaultDatabaseSyncerListener() {
    //		return new DatabaseSyncerListener() {
    //
    //			@Override
    //			public void willUpdateObject(DatabaseModel oldModel,
    //					DatabaseModel newModel) {
    //				updateDatabaseModelUserReadTime(oldModel, newModel);
    //			}
    //
    //			@Override
    //			public void didUpdateObject(DatabaseModel oldModel,
    //					DatabaseModel newModel) {
    //
    //			}
    //		};
    //	}

    public Gson getDefaultGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new PHPDateDeserializer());
        gsonBuilder.registerTypeAdapter(Boolean.class,
                new PHPBooleanDeserializer());
        return gsonBuilder.create();
    }


    protected JSONArray processJSONResponseObject(final Context ctx, final JSONObject jsonObject, RunnableArgument done) {
        if (jsonObject.optBoolean(API_HAS_ERROR_KEY) || !jsonObject.has(API_RESPONSE_OBJ_KEY)) {
            return null;
        }

        try {
            JSONObject responseObject = jsonObject.getJSONObject(API_RESPONSE_OBJ_KEY);
            JSONArray itemsArray = responseObject.getJSONArray(API_ITEMS_KEY);
            return itemsArray;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    ///////////////////////////////////////////////////
    //
    // Server Call for Our Strength
    //
    ///////////////////////////////////////////////////
    public void getOurStrengthFromServer(final Context ctx, final Syncable syn, final RunnableArgument done) {

        get(ctx, syn, "ourStrength/list", null, new RunnableJSON() {

            @Override
            public void run(JSONArray array) {

                if (array == null) {
                    defaultFailureRunnable(ctx, done).run(null);
                    return;
                }

                try {
                    AppDatabaseHelper helper = (AppDatabaseHelper) OpenHelperManager.getHelper(
                            ctx, AppDatabaseHelper.class);
                    OurStrengthDao dao = helper.getDao(OurStrength.class);
                    List<OurStrength> list = dao.queryForAll();
                    for (OurStrength item : list) {
                        dao.delete(item);
                    }
                } catch (Exception er) {
                }
                ;


                TypedSync<OurStrength> mTypedSync = new TypedSync<OurStrength>(getDefaultGson());
                mTypedSync.sync(ctx, OurStrength.class, OurStrength[].class, array.toString(), null);
                if (done != null) {
                    done.run(null);
                }
            }

            @Override
            public void run(JSONObject object) {
                defaultFailureRunnable(ctx, done).run(null);
            }

            @Override
            public void onJSONException(String e) {
                defaultFailureRunnable(ctx, done).run(null);
            }
        }, defaultFailureRunnable(ctx, done), AppDataSource.TERMINATE_NEVER);
    }

    public OurStrength getOurStrength(final Context ctx) {
        AppDatabaseHelper helper = (AppDatabaseHelper) OpenHelperManager.getHelper(
                ctx, AppDatabaseHelper.class);
        try {

            OurStrengthDao dao = helper.getDao(OurStrength.class);
            List<OurStrength> list = dao.queryForAll();
            if (list.size() > 0) {
                return list.get(0);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    ///////////////////////////////////////////////////
    //
    // Server Call for Contact Category
    //
    ///////////////////////////////////////////////////
    public void getContactCategoryFromServer(final Context ctx, final Syncable syn, final RunnableArgument done) {

        get(ctx, syn, "contact/category", null, new RunnableJSON() {

            @Override
            public void run(JSONArray array) {

                if (array == null) {
                    defaultFailureRunnable(ctx, done).run(null);
                    return;
                }

                TypedSync<ContactCategory> mTypedSync = new TypedSync<ContactCategory>(getDefaultGson());
                mTypedSync.sync(ctx, ContactCategory.class, ContactCategory[].class, array.toString(), null);
                if (done != null) {
                    done.run(null);
                }
            }

            @Override
            public void run(JSONObject object) {
                defaultFailureRunnable(ctx, done).run(null);
            }

            @Override
            public void onJSONException(String e) {
                defaultFailureRunnable(ctx, done).run(null);
            }
        }, defaultFailureRunnable(ctx, done), AppDataSource.TERMINATE_NEVER);
    }

    public List<ContactCategory> getContactCategory(final Context ctx) {
        AppDatabaseHelper helper = (AppDatabaseHelper) OpenHelperManager.getHelper(
                ctx, AppDatabaseHelper.class);
        try {

            ContactCategoryDao dao = helper.getDao(ContactCategory.class);
            List<ContactCategory> list = dao.queryForAllWithSort();
            return list;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    ///////////////////////////////////////////////////
    //
    // Server Call for Contact Category
    //
    ///////////////////////////////////////////////////
    public void getContactFromServer(final Context ctx, final Syncable syn, final RunnableArgument done) {

        get(ctx, syn, "contact/list", null, new RunnableJSON() {

            @Override
            public void run(JSONArray array) {

                if (array == null) {
                    defaultFailureRunnable(ctx, done).run(null);
                    return;
                }

                TypedSync<Contact> mTypedSync = new TypedSync<Contact>(getDefaultGson());
                mTypedSync.sync(ctx, Contact.class, Contact[].class, array.toString(), null);
                if (done != null) {
                    done.run(null);
                }
            }

            @Override
            public void run(JSONObject object) {
                defaultFailureRunnable(ctx, done).run(null);
            }

            @Override
            public void onJSONException(String e) {
                defaultFailureRunnable(ctx, done).run(null);
            }
        }, defaultFailureRunnable(ctx, done), AppDataSource.TERMINATE_NEVER);
    }

    public List<Contact> getContact(final Context ctx, String categoryId) {
        AppDatabaseHelper helper = (AppDatabaseHelper) OpenHelperManager.getHelper(
                ctx, AppDatabaseHelper.class);
        try {

            ContactDao dao = helper.getDao(Contact.class);
            List<Contact> list = dao.queryByCategoryId(categoryId);
            return list;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    ///////////////////////////////////////////////////
    //
    // Server Call for Product Collaterals
    //
    ///////////////////////////////////////////////////

    public void getProdColCategoryFromServer(final Context ctx, final Syncable syn, final RunnableArgument done) {

        get(ctx, syn, PATH_LIB_PROD_COL + "/category", null, new RunnableJSON() {

            @Override
            public void run(JSONArray array) {

                if (array == null) {
                    defaultFailureRunnable(ctx, done).run(null);
                    return;
                }

                TypedSync<ProdColCategory> mTypedSync = new TypedSync<ProdColCategory>(getDefaultGson());
                mTypedSync.sync(ctx, ProdColCategory.class, ProdColCategory[].class, array.toString(), null);
                if (done != null) {
                    done.run(null);
                }
            }

            @Override
            public void run(JSONObject object) {
                defaultFailureRunnable(ctx, done).run(null);
            }

            @Override
            public void onJSONException(String e) {
                defaultFailureRunnable(ctx, done).run(null);
            }
        }, defaultFailureRunnable(ctx, done), AppDataSource.TERMINATE_NEVER);
    }

    public void getProdColLibraryFromServer(final Context ctx, final Syncable syn, final RunnableArgument done) {

        get(ctx, syn, PATH_LIB_PROD_COL + "/list", null, new RunnableJSON() {

            @Override
            public void run(JSONArray array) {

                if (array == null) {
                    defaultFailureRunnable(ctx, done).run(null);
                    return;
                }

                TypedSync<ProdColLibrary> mTypedSync = new TypedSync<ProdColLibrary>(getDefaultGson());
                mTypedSync.sync(ctx, ProdColLibrary.class, ProdColLibrary[].class, array.toString(), null);
                if (done != null) {
                    done.run(null);
                }
            }

            @Override
            public void run(JSONObject object) {
                defaultFailureRunnable(ctx, done).run(null);
            }

            @Override
            public void onJSONException(String e) {
                defaultFailureRunnable(ctx, done).run(null);
            }
        }, defaultFailureRunnable(ctx, done), AppDataSource.TERMINATE_NEVER);
    }


    ///////////////////////////////////////////////////
    //
    // Server Call for Field Communications
    //
    ///////////////////////////////////////////////////

    public void getFieldCommCategoryFromServer(final Context ctx, final Syncable syn, final RunnableArgument done) {

        get(ctx, syn, PATH_LIB_FIELD_COMM + "/category", null, new RunnableJSON() {

            @Override
            public void run(JSONArray array) {

                if (array == null) {
                    defaultFailureRunnable(ctx, done).run(null);
                    return;
                }

                TypedSync<FieldCommCategory> mTypedSync = new TypedSync<FieldCommCategory>(getDefaultGson());
                mTypedSync.sync(ctx, FieldCommCategory.class, FieldCommCategory[].class, array.toString(), null);
                if (done != null) {
                    done.run(null);
                }
            }

            @Override
            public void run(JSONObject object) {
                defaultFailureRunnable(ctx, done).run(null);
            }

            @Override
            public void onJSONException(String e) {
                defaultFailureRunnable(ctx, done).run(null);
            }
        }, defaultFailureRunnable(ctx, done), AppDataSource.TERMINATE_NEVER);
    }

    public void getFieldCommLibraryFromServer(final Context ctx, final Syncable syn, final RunnableArgument done) {

        get(ctx, syn, PATH_LIB_FIELD_COMM + "/list", null, new RunnableJSON() {

            @Override
            public void run(JSONArray array) {

                if (array == null) {
                    defaultFailureRunnable(ctx, done).run(null);
                    return;
                }

                TypedSync<FieldCommLibrary> mTypedSync = new TypedSync<FieldCommLibrary>(getDefaultGson());
                mTypedSync.sync(ctx, FieldCommLibrary.class, FieldCommLibrary[].class, array.toString(), null);
                if (done != null) {
                    done.run(null);
                }
            }

            @Override
            public void run(JSONObject object) {
                defaultFailureRunnable(ctx, done).run(null);
            }

            @Override
            public void onJSONException(String e) {
                defaultFailureRunnable(ctx, done).run(null);
            }
        }, defaultFailureRunnable(ctx, done), AppDataSource.TERMINATE_NEVER);
    }

    ///////////////////////////////////////////////////
    //
    // Server Call for Forms
    //
    ///////////////////////////////////////////////////
    public void getFormsCategoryFromServer(final Context ctx, final Syncable syn, final RunnableArgument done) {

        get(ctx, syn, PATH_LIB_FORMS + "/category", null, new RunnableJSON() {

            @Override
            public void run(JSONArray array) {

                if (array == null) {
                    defaultFailureRunnable(ctx, done).run(null);
                    return;
                }

                TypedSync<FormsCategory> mTypedSync = new TypedSync<FormsCategory>(getDefaultGson());
                mTypedSync.sync(ctx, FormsCategory.class, FormsCategory[].class, array.toString(), null);
                if (done != null) {
                    done.run(null);
                }
            }

            @Override
            public void run(JSONObject object) {
                defaultFailureRunnable(ctx, done).run(null);
            }

            @Override
            public void onJSONException(String e) {
                defaultFailureRunnable(ctx, done).run(null);
            }
        }, defaultFailureRunnable(ctx, done), AppDataSource.TERMINATE_NEVER);
    }

    public void getFormsLibraryFromServer(final Context ctx, final Syncable syn, final RunnableArgument done) {

        get(ctx, syn, PATH_LIB_FORMS + "/list", null, new RunnableJSON() {

            @Override
            public void run(JSONArray array) {

                if (array == null) {
                    defaultFailureRunnable(ctx, done).run(null);
                    return;
                }

                TypedSync<FormsLibrary> mTypedSync = new TypedSync<FormsLibrary>(getDefaultGson());
                mTypedSync.sync(ctx, FormsLibrary.class, FormsLibrary[].class, array.toString(), null);
                if (done != null) {
                    done.run(null);
                }
            }

            @Override
            public void run(JSONObject object) {
                defaultFailureRunnable(ctx, done).run(null);
            }

            @Override
            public void onJSONException(String e) {
                defaultFailureRunnable(ctx, done).run(null);
            }
        }, defaultFailureRunnable(ctx, done), AppDataSource.TERMINATE_NEVER);
    }

    ///////////////////////////////////////////////////
    //
    // Server Call for Underwriting Guide
    //
    ///////////////////////////////////////////////////
    public void getUnderwritingGuideCategoryFromServer(final Context ctx, final Syncable syn, final RunnableArgument done) {

        get(ctx, syn, PATH_LIB_UNDERWRITEING_GUIDE + "/category", null, new RunnableJSON() {

            @Override
            public void run(JSONArray array) {

                if (array == null) {
                    defaultFailureRunnable(ctx, done).run(null);
                    return;
                }

                TypedSync<UnderwritingGuideCategory> mTypedSync = new TypedSync<UnderwritingGuideCategory>(getDefaultGson());
                mTypedSync.sync(ctx, UnderwritingGuideCategory.class, UnderwritingGuideCategory[].class, array.toString(), null);
                if (done != null) {
                    done.run(null);
                }
            }

            @Override
            public void run(JSONObject object) {
                defaultFailureRunnable(ctx, done).run(null);
            }

            @Override
            public void onJSONException(String e) {
                defaultFailureRunnable(ctx, done).run(null);
            }
        }, defaultFailureRunnable(ctx, done), AppDataSource.TERMINATE_NEVER);
    }

    public void getUnderwritingGuideLibraryFromServer(final Context ctx, final Syncable syn, final RunnableArgument done) {

        get(ctx, syn, PATH_LIB_UNDERWRITEING_GUIDE + "/list", null, new RunnableJSON() {

            @Override
            public void run(JSONArray array) {

                if (array == null) {
                    defaultFailureRunnable(ctx, done).run(null);
                    return;
                }

                TypedSync<UnderwritingGuideLibrary> mTypedSync = new TypedSync<UnderwritingGuideLibrary>(getDefaultGson());
                mTypedSync.sync(ctx, UnderwritingGuideLibrary.class, UnderwritingGuideLibrary[].class, array.toString(), null);
                if (done != null) {
                    done.run(null);
                }
            }

            @Override
            public void run(JSONObject object) {
                defaultFailureRunnable(ctx, done).run(null);
            }

            @Override
            public void onJSONException(String e) {
                defaultFailureRunnable(ctx, done).run(null);
            }
        }, defaultFailureRunnable(ctx, done), AppDataSource.TERMINATE_NEVER);
    }

    ///////////////////////////////////////////////////
    //
    // Server Call for Transware
    //
    ///////////////////////////////////////////////////
    public void getTranswareCategoryFromServer(final Context ctx, final Syncable syn, final RunnableArgument done) {

        get(ctx, syn, PATH_LIB_TRANSWARE + "/category", null, new RunnableJSON() {

            @Override
            public void run(JSONArray array) {

                if (array == null) {
                    defaultFailureRunnable(ctx, done).run(null);
                    return;
                }

                TypedSync<TranswareCategory> mTypedSync = new TypedSync<TranswareCategory>(getDefaultGson());
                mTypedSync.sync(ctx, TranswareCategory.class, TranswareCategory[].class, array.toString(), null);
                if (done != null) {
                    done.run(null);
                }
            }

            @Override
            public void run(JSONObject object) {
                defaultFailureRunnable(ctx, done).run(null);
            }

            @Override
            public void onJSONException(String e) {
                defaultFailureRunnable(ctx, done).run(null);
            }
        }, defaultFailureRunnable(ctx, done), AppDataSource.TERMINATE_NEVER);
    }

    public void getTranswareLibraryFromServer(final Context ctx, final Syncable syn, final RunnableArgument done) {

        get(ctx, syn, PATH_LIB_TRANSWARE + "/list", null, new RunnableJSON() {

            @Override
            public void run(JSONArray array) {

                if (array == null) {
                    defaultFailureRunnable(ctx, done).run(null);
                    return;
                }

                TypedSync<TranswareLibrary> mTypedSync = new TypedSync<TranswareLibrary>(getDefaultGson());
                mTypedSync.sync(ctx, TranswareLibrary.class, TranswareLibrary[].class, array.toString(), null);
                if (done != null) {
                    done.run(null);
                }
            }

            @Override
            public void run(JSONObject object) {
                defaultFailureRunnable(ctx, done).run(null);
            }

            @Override
            public void onJSONException(String e) {
                defaultFailureRunnable(ctx, done).run(null);
            }
        }, defaultFailureRunnable(ctx, done), AppDataSource.TERMINATE_NEVER);
    }


    ///////////////////////////////////////////////////
    //
    // Generic Call for Library
    //
    ///////////////////////////////////////////////////

    public List<? extends LibraryCategory> getLibraryCategoryBySection(final Context ctx, int section, String id) {
        AppDatabaseHelper helper = (AppDatabaseHelper) OpenHelperManager.getHelper(
                ctx, AppDatabaseHelper.class);
        try {
            List<? extends LibraryCategory> list = null;
            if (section == LibraryCategory.PRODUCT_COLLATERALS) {
                ProdColCategoryDao dao = helper.getDao(ProdColCategory.class);
                list = dao.querySubCategoryByCategoryId(id);
            } else if (section == LibraryCategory.FIELD_COMMUNICATIONS) {
                FieldCommCategoryDao dao = helper.getDao(FieldCommCategory.class);
                list = dao.querySubCategoryByCategoryId(id);
            } else if (section == LibraryCategory.FORMS) {
                FormsCategoryDao dao = helper.getDao(FormsCategory.class);
                list = dao.querySubCategoryByCategoryId(id);
            } else if (section == LibraryCategory.UNDERWRITING_GUIDE) {
                UnderwritingGuideCategoryDao dao = helper.getDao(UnderwritingGuideCategory.class);
                list = dao.querySubCategoryByCategoryId(id);
            } else if (section == LibraryCategory.TRANSWARE) {
                TranswareCategoryDao dao = helper.getDao(TranswareCategory.class);
                list = dao.querySubCategoryByCategoryId(id);
            }

            return list;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public LibraryCategory getRootLibraryCategoryByCategoryId(final Context ctx, int section, String id) {
        AppDatabaseHelper helper = (AppDatabaseHelper) OpenHelperManager.getHelper(
                ctx, AppDatabaseHelper.class);
        try {
            LibraryCategory cate = null;
            if (section == LibraryCategory.PRODUCT_COLLATERALS) {
                ProdColCategoryDao dao = helper.getDao(ProdColCategory.class);
                cate = dao.queryForId(id);
            } else if (section == LibraryCategory.FIELD_COMMUNICATIONS) {
                FieldCommCategoryDao dao = helper.getDao(FieldCommCategory.class);
                cate = dao.queryForId(id);
            } else if (section == LibraryCategory.FORMS) {
                FormsCategoryDao dao = helper.getDao(FormsCategory.class);
                cate = dao.queryForId(id);
            } else if (section == LibraryCategory.UNDERWRITING_GUIDE) {
                UnderwritingGuideCategoryDao dao = helper.getDao(UnderwritingGuideCategory.class);
                cate = dao.queryForId(id);
            } else if (section == LibraryCategory.TRANSWARE) {
                TranswareCategoryDao dao = helper.getDao(TranswareCategory.class);
                cate = dao.queryForId(id);
            }

            return cate;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<? extends Library> getAllLibraryInSubCategoryBySection(final Context ctx, int section, List<String> subCatIds) {
        AppDatabaseHelper helper = (AppDatabaseHelper) OpenHelperManager.getHelper(
                ctx, AppDatabaseHelper.class);
        try {


            List<? extends Library> list = null;
            if (section == LibraryCategory.PRODUCT_COLLATERALS) {
                ProdColLibraryDao dao = helper.getDao(ProdColLibrary.class);
                list = dao.queryByCategoryIds(subCatIds);
            } else if (section == LibraryCategory.FIELD_COMMUNICATIONS) {
                FieldCommLibraryDao dao = helper.getDao(FieldCommLibrary.class);
                list = dao.queryByCategoryIds(subCatIds);
            } else if (section == LibraryCategory.FORMS) {
                FormsLibraryDao dao = helper.getDao(FormsLibrary.class);
                list = dao.queryByCategoryIds(subCatIds);
            } else if (section == LibraryCategory.UNDERWRITING_GUIDE) {
                UnderwritingGuideLibraryDao dao = helper.getDao(UnderwritingGuideLibrary.class);
                list = dao.queryByCategoryIds(subCatIds);
            } else if (section == LibraryCategory.TRANSWARE) {
                TranswareLibraryDao dao = helper.getDao(TranswareLibrary.class);
                list = dao.queryByCategoryIds(subCatIds);
            }


            return list;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Library getLibraryBySectionAndId(final Context ctx, int section, String id) {
        AppDatabaseHelper helper = (AppDatabaseHelper) OpenHelperManager.getHelper(
                ctx, AppDatabaseHelper.class);
        try {


            Library library = null;
            if (section == LibraryCategory.PRODUCT_COLLATERALS) {
                ProdColLibraryDao dao = helper.getDao(ProdColLibrary.class);
                library = dao.queryForId(id);
            } else if (section == LibraryCategory.FIELD_COMMUNICATIONS) {
                FieldCommLibraryDao dao = helper.getDao(FieldCommLibrary.class);
                library = dao.queryForId(id);
            } else if (section == LibraryCategory.FORMS) {
                FormsLibraryDao dao = helper.getDao(FormsLibrary.class);
                library = dao.queryForId(id);
            } else if (section == LibraryCategory.UNDERWRITING_GUIDE) {
                UnderwritingGuideLibraryDao dao = helper.getDao(UnderwritingGuideLibrary.class);
                library = dao.queryForId(id);
            } else if (section == LibraryCategory.TRANSWARE) {
                TranswareLibraryDao dao = helper.getDao(TranswareLibrary.class);
                library = dao.queryForId(id);
            }

            return library;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setShowPushAlert(final Context ctx, boolean isUpdated) {
        SharedPreferences prefs = ctx.getSharedPreferences(PREFS, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREFS_SHOW_PUSH_ALERT, isUpdated);
        editor.commit();
    }

    public boolean isShowPushAlert(final Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppDataSource.PREFS, 0);
        return prefs.getBoolean(PREFS_SHOW_PUSH_ALERT, true);
    }


    public void setShowTutorial(final Context ctx, boolean isUpdated) {
        SharedPreferences prefs = ctx.getSharedPreferences(PREFS, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREFS_SHOW_TUTORIAL, isUpdated);
        editor.commit();
    }

    public boolean isShowTutorial(final Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppDataSource.PREFS, 0);
        return prefs.getBoolean(PREFS_SHOW_TUTORIAL, true);
    }

    public void setLogin(final Context ctx, boolean isLogin, String location) {
        SharedPreferences prefs = ctx.getSharedPreferences(PREFS, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREFS_IS_LOGIN, isLogin);
        editor.putString(PREFS_LOCATION, isLogin ? location : "");
        editor.commit();
    }

    public boolean isLogin(final Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppDataSource.PREFS, 0);
        return prefs.getBoolean(PREFS_IS_LOGIN, false);
    }

    public String getLocation(final Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppDataSource.PREFS, 0);
        return prefs.getString(PREFS_LOCATION, "");
    }

//    public void setLastLoginUser(final Context ctx, String user) {
//        SharedPreferences prefs = ctx.getSharedPreferences(PREFS, 0);
//        SharedPreferences.Editor editor = prefs.edit();
//        editor.putString(PREFS_LAST_LOGIN_USER, user);
//        editor.commit();
//    }
//
//    public String getLastLoginUser(final Context ctx) {
//        SharedPreferences prefs = ctx.getSharedPreferences(AppDataSource.PREFS, 0);
//        return prefs.getString(PREFS_LAST_LOGIN_USER, "");
//    }

    public void setLastLoginTime(final Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(PREFS, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(PREFS_LAST_LOGIN_TIME, System.currentTimeMillis());
        editor.commit();
    }

    public boolean isSessionExpired(final Context ctx) {
        boolean isSessionExpired = false;
        SharedPreferences prefs = ctx.getSharedPreferences(AppDataSource.PREFS, 0);
        long lastLoginTime = prefs.getLong(PREFS_LAST_LOGIN_TIME, 0);

        long validTime = System.currentTimeMillis() - (24 * 60 * 60 * 1000);
        if (lastLoginTime < validTime) {
            isSessionExpired = true;
        }

        return isSessionExpired;
    }

    //Push
    public String getRegistrationId(final Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppDataSource.PREFS, 0);
        return prefs.getString(PROPERTY_REG_ID, "");
    }

    public void storeRegistrationId(Context context, String regId, int appVersion) {
        SharedPreferences prefs = context.getSharedPreferences(AppDataSource.PREFS, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    public int getRegistrationVersion(final Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppDataSource.PREFS, 0);
        return prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
    }

    public void setEnableNotification(final Context ctx, boolean enable) {
        SharedPreferences prefs = ctx.getSharedPreferences(PREFS, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREFS_ENABLE_NOTIFICATION, enable);
        editor.commit();
    }

    public boolean isEnableNotification(final Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppDataSource.PREFS, 0);
        return prefs.getBoolean(PREFS_ENABLE_NOTIFICATION, false);
    }

    public void setLoginUser(final Context ctx, String user) {
        SharedPreferences prefs = ctx.getSharedPreferences(PREFS, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREFS_LOGIN_USER, user);
        editor.commit();
    }

    public String getLoginUser(final Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppDataSource.PREFS, 0);
        return prefs.getString(PREFS_LOGIN_USER, "");
    }

    public void setLoginPwd(final Context ctx, String pwd) {
        SharedPreferences prefs = ctx.getSharedPreferences(PREFS, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREFS_LOGIN_PWD, pwd);
        editor.commit();
    }

    public String getLoginPwd(final Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppDataSource.PREFS, 0);
        return prefs.getString(PREFS_LOGIN_PWD, "");
    }

    public void login(final Context ctx, String username, String password, final Syncable syn, final RunnableArgument done) {

        ArrayList<NameValuePair> params = new ArrayList<>();
        params.add(new SerializableBasicNameValuePair("devicetype", "android"));
        params.add(new SerializableBasicNameValuePair("deviceid", getRegistrationId(ctx)));
        params.add(new SerializableBasicNameValuePair("username", username));
        params.add(new SerializableBasicNameValuePair("pwd", password));

        get(ctx, syn, "user/login", params, new RunnableJSON() {

            @Override
            public void run(JSONArray array) {
                defaultFailureRunnable(ctx, done).run(null);
            }

            @Override
            public void run(JSONObject object) {
                LoginResponse response = getDefaultGson().fromJson(object.toString(), LoginResponse.class);
                if (response.getSuccess() == 1) {
                    AppDataSource.getInstance().setLogin(ctx, true, response.getLocation());
                    AppDataSource.getInstance().setEnableNotification(ctx, true);
                    AppDataSource.getInstance().setLastLoginTime(ctx);
                    if (done != null) {
                        done.run(null);
                    }
                } else {
                    if (done != null) {
                        done.run(response.getErrorCode());
                    }
                }
            }

            @Override
            public void onJSONException(String e) {
                defaultFailureRunnable(ctx, done).run(null);
            }
        }, defaultFailureRunnable(ctx, done), AppDataSource.TERMINATE_NEVER);
    }

    //Register Push only
    public void regPushToServer(final Context ctx) {

        ArrayList<NameValuePair> params = new ArrayList<>();
        params.add(new SerializableBasicNameValuePair("devicetype", "android"));
        params.add(new SerializableBasicNameValuePair("deviceid", getRegistrationId(ctx)));

        get(ctx, null, "user/register", params, null , null, AppDataSource.TERMINATE_NEVER);
    }
}
