package com.transamerican.tlb;

public class LauncherActivity extends LauncherBaseActivity {

    @Override
    protected int getLayout() {
        return R.layout.activity_launch;
    }

    @Override
    protected Class getMainActivity() {
        return MainActivity.class;
    }
}
