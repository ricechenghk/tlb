package com.transamerican.tlb.base;


import android.annotation.SuppressLint;
import android.graphics.Point;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.transamerican.tlb.R;
import com.transamerican.tlb.listener.PageChangeListener;

import butterknife.InjectView;
import butterknife.OnClick;
import hk.com.playmore.syncframework.util.SyncFragment;

/**
 * Created by Rice on 23/1/15.
 */
public abstract class SectionBaseFragment extends SyncFragment implements ViewPager.OnPageChangeListener, PageChangeListener {

    public static final String TAG = SectionBaseFragment.class.getName();
    protected static final int DEFAULT_CLOSE_MENU_TIME = 10000;

    @InjectView(R.id.section_image)
    ImageView sectionImage;

    @InjectView(R.id.detail_title)
    TextView title;

    @InjectView(R.id.slider)
    ImageView slider;

    @InjectView(R.id.replay_divider)
    View replayDivider;

    @InjectView(R.id.video_replay)
    ImageView replay;

    @InjectView(R.id.viewPager)
    ViewPager viewPager;
    MyPagerAdapter adapter;

    @InjectView(R.id.sub_menu)
    RelativeLayout subMenu;

    @InjectView(R.id.sub_menu_bar)
    ImageView subMenuBar;

    @InjectView(R.id.btn_left)
    ImageView leftPageBtn;

    @InjectView(R.id.btn_right)
    ImageView rightPageBtn;

    int screenWidth;
    int screenHeight;
    int sectionImageWidth;
    int sectionImageHeight;
    int subMenuWidth;

    protected boolean isMenuOpen = true;
    protected boolean isShowAnimationPlaying = false;
    protected boolean isHideAnimationPlaying = false;

    protected void initScreen() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
        screenHeight = size.y;

        sectionImage.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                //now we can retrieve the width and height
                sectionImageWidth = sectionImage.getWidth();
                sectionImageHeight = sectionImage.getHeight();

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN)
                    sectionImage.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                else
                    sectionImage.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        subMenu.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                //now we can retrieve the width and height
                subMenuWidth = subMenu.getWidth();

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN)
                    subMenu.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                else
                    subMenu.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        adapter = new MyPagerAdapter(this.getChildFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(this);

        hideSubMenu(DEFAULT_CLOSE_MENU_TIME);

        if (adapter.getCount() > 0) {
            leftPageBtn.setSelected(true);
            leftPageBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Log.i("", "Page: " + position + "is selected");
        Fragment page = getChildFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + viewPager.getCurrentItem());
        if (page != null) {
            PagerFragment fr = ((PagerFragment) page);
            fr.onPageSelected();
            resetInfo(fr.getTitle(), fr.getSlider(), fr.isVideoExist());
        }

        if (position == 0) {
            leftPageBtn.setSelected(true);
            leftPageBtn.setEnabled(false);
            leftPageBtn.setVisibility(View.GONE);
        } else {
            leftPageBtn.setSelected(false);
            leftPageBtn.setEnabled(true);
            leftPageBtn.setVisibility(View.VISIBLE);
        }

        if (position == adapter.getCount() - 1) {
            rightPageBtn.setSelected(true);
            rightPageBtn.setEnabled(false);
            rightPageBtn.setVisibility(View.GONE);
        } else {
            rightPageBtn.setSelected(false);
            rightPageBtn.setEnabled(true);
            rightPageBtn.setVisibility(View.VISIBLE);
        }

        hideSubMenu(0);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void goToPage(int page) {
        int pos = page - 1;
        if (pos != viewPager.getCurrentItem()) {
            viewPager.setCurrentItem(pos);
        }
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public PagerFragment getItem(int pos) {

            PagerFragment fr = getPagerFragment(pos);
            fr.setListener(SectionBaseFragment.this);

            return fr;
        }

        @Override
        public int getCount() {
            return getPagerCount();
        }

    }

    public void resetInfo(int titleResId, int sliderResId, boolean isVideoExist) {
        title.setText(titleResId);
        if (sliderResId == 0) {
            slider.setVisibility(View.INVISIBLE);
        } else {
            slider.setVisibility(View.VISIBLE);
            slider.setImageResource(sliderResId);
        }
        replayDivider.setVisibility(isVideoExist ? View.VISIBLE : View.INVISIBLE);
        replay.setVisibility(isVideoExist ? View.VISIBLE : View.INVISIBLE);
    }

    @OnClick(R.id.video_replay)
    public void onReplayClicked() {
        Fragment page = getChildFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + viewPager.getCurrentItem());
        if (page != null) {
            PagerFragment fr = ((PagerFragment) page);
            fr.onReplayClicked();
        }
    }


    @OnClick(R.id.sub_menu)
    public void onSubMenuClicked() {
        showSubMenu();
    }

    public void hideSubMenu(int time) {

        if(isHideAnimationPlaying)
        {
            return;
        }

        if (isMenuOpen) {
            subMenu.removeCallbacks(hideSubMenuRunnable);
            subMenu.postDelayed(hideSubMenuRunnable, time);
        }
    }

    public void showSubMenu() {

        if(isShowAnimationPlaying)
        {
            return;
        }

        if (!isMenuOpen) {
            float over = ((subMenuWidth - sectionImageWidth) / (float) subMenuWidth);

            Animation outtoLeft = new TranslateAnimation(
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, over,
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f);
            outtoLeft.setDuration(1000);
            outtoLeft.setFillAfter(true);
            outtoLeft.setInterpolator(new AccelerateInterpolator());
            outtoLeft.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    int overMargin = (subMenuWidth - sectionImageWidth) * -1;
                    subMenu.clearAnimation();
                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(subMenu.getWidth(), subMenu.getHeight());
                    lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    lp.setMargins(0, 0, 0, 0);
                    subMenu.setLayoutParams(lp);

                    isMenuOpen = true;
                    isShowAnimationPlaying = false;

                    subMenuBar.setImageResource(R.drawable.sub_menu_close);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            subMenu.startAnimation(outtoLeft);

            isShowAnimationPlaying = true;

            hideSubMenu(DEFAULT_CLOSE_MENU_TIME);
        }
        else
        {
            hideSubMenu(0);
        }
    }

    Runnable hideSubMenuRunnable = new Runnable() {
        public void run() {

            float over = ((subMenuWidth - sectionImageWidth) / (float) subMenuWidth) * -1;

            Animation outtoLeft = new TranslateAnimation(
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, over,
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f);
            outtoLeft.setDuration(1000);
            outtoLeft.setFillAfter(true);
            outtoLeft.setInterpolator(new AccelerateInterpolator());
            outtoLeft.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    int overMargin = (subMenuWidth - sectionImageWidth) * -1;
                    subMenu.clearAnimation();
                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(subMenu.getWidth(), subMenu.getHeight());
                    lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    lp.setMargins(overMargin, 0, 0, 0);
                    subMenu.setLayoutParams(lp);

                    isMenuOpen = false;
                    isHideAnimationPlaying = false;

                    subMenuBar.setImageResource(R.drawable.sub_menu_open);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            subMenu.startAnimation(outtoLeft);
            isHideAnimationPlaying = true;
        }
    };

    @OnClick(R.id.btn_left)
    public void onLeftPageBtnClicked() {
        goToPage(viewPager.getCurrentItem());
    }

    @OnClick(R.id.btn_right)
    public void onRightPageBtnClicked() {
        goToPage(viewPager.getCurrentItem() + 2);
    }

    public abstract PagerFragment getPagerFragment(int pos);

    public abstract int getPagerCount();
}