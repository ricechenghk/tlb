package com.transamerican.tlb.base;

import android.content.res.AssetFileDescriptor;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;

import com.transamerican.tlb.R;

import java.io.IOException;

import butterknife.InjectView;

/**
 * Created by Rice on 22/1/15.
 */
public abstract class VideoPagerFragment extends PagerFragment implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {


    protected MediaPlayer mediaPlayer;

    @InjectView(R.id.detail_video)
    SurfaceView layout;

    SurfaceHolder surfaceHolder;

    protected boolean isVideoPrepared = false;
    protected boolean isPageSelected = false;

    protected boolean isStartWhenPrepared = true;

    public ViewGroup.LayoutParams layoutParams;

    public abstract String getVideo();

    public void initVideo() {
        getActivity().getWindow().setFormat(PixelFormat.UNKNOWN);

        surfaceHolder = layout.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        mediaPlayer = new MediaPlayer();

        try {

            AssetFileDescriptor descriptor = getActivity().getAssets().openFd(getVideo());
            mediaPlayer.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();

            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnCompletionListener(this);

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        // TODO Auto-generated method stub
//        Log.i("", "Surface is changed!!!!");

    }


    @Override
    public void surfaceCreated(SurfaceHolder arg0) {

        getPrepare();

    }

    public void getPrepare() {

        if (mediaPlayer.isPlaying()) {
            mediaPlayer.reset();
        }

        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setDisplay(surfaceHolder);


        try {

            mediaPlayer.prepare();

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void surfaceDestroyed(SurfaceHolder arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPrepared(MediaPlayer arg0) {
        // TODO Auto-generated method stub
        Log.i("", "Media is prepared!!!!");
        isVideoPrepared = true;
        mediaPlayer.seekTo(1);

        if (isPageSelected) {
            handleAspectRatio();
            mediaPlayer.start();
        }
    }

    @Override
    public void onCompletion(MediaPlayer arg0) {
        // TODO Auto-generated method stub
        Log.i("", "Media is completed!!!!");
//        preview.setImageResource(R.drawable.video_who_we_are_demoa_end);
//        preview.setVisibility(View.VISIBLE);
//        layout.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onPageSelected() {
        Log.i("", "Selected and Start Prepare!!!!");
//        preview.setVisibility(View.INVISIBLE);
        isPageSelected = true;
        if (isVideoPrepared) {
            handleAspectRatio();
            mediaPlayer.start();
        }
    }

    @Override
    public void onDestroyView() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }

        isPageSelected = false;
        isVideoPrepared = false;

        super.onDestroyView();
    }

    @Override
    public void onPageExited() {
//        mediaPlayer.release();
    }


    public void handleAspectRatio() {
             /*
       *  Handle aspect ratio
       */
        int surfaceView_Width = layout.getWidth();
        int surfaceView_Height = layout.getHeight();

        float video_Width = mediaPlayer.getVideoWidth();
        float video_Height = mediaPlayer.getVideoHeight();

        float ratio_width = surfaceView_Width / video_Width;
        float ratio_height = surfaceView_Height / video_Height;
        float aspectratio = video_Width / video_Height;

        layoutParams = layout.getLayoutParams();

        if (ratio_width > ratio_height) {
            layoutParams.width = (int) (surfaceView_Height * aspectratio);
            layoutParams.height = surfaceView_Height;
        } else {
            layoutParams.width = surfaceView_Width;
            layoutParams.height = (int) (surfaceView_Width / aspectratio);
        }

        layout.setLayoutParams(layoutParams);
    }

    @Override
    public boolean isVideoExist() {
        return true;
    }

    public void onReplayClicked() {
        mediaPlayer.start();
    }

    public void prepareVideo(String videoName) {
        try {
            mediaPlayer.reset();
            AssetFileDescriptor descriptor = getActivity().getAssets().openFd(videoName);
            mediaPlayer.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();

            mediaPlayer.prepare();

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (isStartWhenPrepared) {
            mediaPlayer.start();
        }

    }
}
