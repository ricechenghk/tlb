package com.transamerican.tlb.base;

import android.support.v4.app.Fragment;

import com.transamerican.tlb.listener.PageChangeListener;

/**
 * Created by Rice on 19/1/15.
 */
public abstract class PagerFragment extends Fragment {

    public abstract void onPageSelected();

    public abstract void onPageExited();

    public abstract int getTitle();

    public abstract int getSlider();

    public abstract boolean isVideoExist();

    public abstract void onReplayClicked();

    protected PageChangeListener listener;

    public void setListener(PageChangeListener listener) {
        this.listener = listener;
    }
}
