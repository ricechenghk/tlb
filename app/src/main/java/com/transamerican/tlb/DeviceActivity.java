package com.transamerican.tlb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.transamerican.tlb.phone.LauncherPActivity;


public class DeviceActivity extends Activity {
    private static final String TAG = DeviceActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        Log.i("", "Tablet: " + tabletSize);

        Intent intent = new Intent(this, LauncherActivity.class);
        if (!tabletSize) {
            intent = new Intent(this, LauncherPActivity.class);
        }
        startActivity(intent);
        finish();
    }

    protected void onResume() {
        super.onResume();
    }

}
