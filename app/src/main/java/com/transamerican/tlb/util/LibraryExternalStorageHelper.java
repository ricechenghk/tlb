package com.transamerican.tlb.util;

import android.content.Context;

import com.google.gson.Gson;
import com.transamerican.tlb.model.pdf.LibraryLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import hk.com.playmore.util.ExternalStorageHelper;
import hk.com.playmore.util.StringUtils;

public class LibraryExternalStorageHelper extends ExternalStorageHelper {

    private static String JSON_FILE_EXTENSION = ".json";
    private static String ZIP_FILE_EXTENSION = ".zip";
    private static String TLB_LIBRARY_PATH = "tlb_library";

    private String getCustomCacheDir(Context ctx) {
        if (ctx.getExternalFilesDir(null) != null) {
            return ctx.getExternalFilesDir(null).getAbsolutePath();
        } else {
            return ctx.getFilesDir().getAbsolutePath();
        }
    }

    public File createLibraryZipFile(final Context ctx, final InputStream is, final int sectionId, final String libraryID) throws Exception {
        final File zipFile = getLibraryZipFile(ctx, sectionId, libraryID);
        if (zipFile.exists()) {
            zipFile.delete();
        }

        try {
            final OutputStream output = new FileOutputStream(zipFile);
            try {
                final byte[] buffer = new byte[1024];
                int read;

                while ((read = is.read(buffer)) != -1) {
                    output.write(buffer, 0, read);
                }

                output.flush();
            } finally {
                output.close();
            }

        } finally {
            is.close();
        }
        return zipFile;
    }

    public void unzipLibrary(Context ctx, int sectionId, String libraryID) throws IOException {
        final File zipFile = getLibraryZipFile(ctx, sectionId, libraryID);
        String outPath = getLibraryLayoutFolderPath(ctx, sectionId, libraryID);
        new File(outPath).mkdirs();
        unpackTarZip(outPath, zipFile.getAbsolutePath());
    }

    public File getLibraryZipFile(Context context, int sectionId, String libraryId) {
        String parentPath = getLibraryPath(context, sectionId, libraryId);
        File parentFile = new File(parentPath);
        parentFile.mkdirs();
        String fileName = libraryId + ZIP_FILE_EXTENSION;
        File file = new File(parentPath, fileName);
        return file;
    }

    public String getLibraryLayoutFolderPath(Context context, int sectionId, String libraryId) {
        String parentPath = getLibraryPath(context, sectionId, libraryId);
        String targetPath = parentPath + "content" + File.separator;
        return targetPath;
    }

    public boolean isLibraryZipFileExist(Context context, int sectionId, String libraryId) {
        String parentPath = getLibraryPath(context, sectionId, libraryId);
        String fileName = libraryId + ZIP_FILE_EXTENSION;
        File file = new File(parentPath, fileName);
        return file.exists();
    }

    public boolean isLibraryLayoutJSONFileExist(Context context, int sectionId, String libraryId) {
        File file = new File(getLibraryLayoutJSONPath(context, sectionId, libraryId));
        return file.exists();
    }

    public String getLibraryLayoutJSONPath(Context context, int sectionId, String libraryId) {
        String libraryPath = getLibraryLayoutFolderPath(context, sectionId, libraryId);
        String fileName = "layout" + JSON_FILE_EXTENSION;
        return libraryPath + fileName;
    }

    // Create librarylayout object
    public LibraryLayout getLibraryLayout(final Context ctx, int sectionId, String libraryId, Gson gson) {
        String jsonPath = getLibraryLayoutJSONPath(ctx, sectionId, libraryId);
        String jsonString = null;
        try {
            jsonString = StringUtils.getStringFromFile(jsonPath);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (jsonString == null) {
            return null;
        }

        LibraryLayout layout = gson.fromJson(jsonString, LibraryLayout.class);
        return layout;
    }

    public void deleteLibrary(Context context, int sectionId, String libraryId) {
        String parentPath = getLibraryPath(context, sectionId, libraryId);
        File deleteFile = new File(parentPath);
        if (deleteFile.exists()) {
            deleteRecursive(deleteFile);
        }
    }

    public String getLibraryPath(Context context, int sectionId, String libraryId) {
        String parentPath = getCustomCacheDir(context) + File.separator + TLB_LIBRARY_PATH + File.separator +
                sectionId + File.separator +
                "bc_library_layout_" + libraryId + File.separator;
        return parentPath;
    }

    private File getDownloadDirectory(Context context) {
        String target = getCustomCacheDir(context) + File.separator + TLB_LIBRARY_PATH + File.separator + "download";
        File g = new File(target);
        if (!g.exists()) {
            g.mkdirs();
        }
        return g;
    }

    public File getDownloadTargetFile(Context context, String url) {
        String filename = convertToFilename(url).trim();
        File base = getDownloadDirectory(context);

        String path = base.getAbsolutePath().concat("/").concat(filename).concat(".PDF");
        File target = new File(path);
        return target;
    }

    private String convertToFilename(String i) {
        if (i == null) {
            return null;
        }
        return i.replaceAll("[:punct:]", "-");
    }

}
