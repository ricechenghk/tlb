package com.transamerican.tlb.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by Rice on 4/9/15.
 */
public class SimpleLoadingDialog {

    public static ProgressDialog show(Context a, int loadingResId) {

        ProgressDialog dialog = ProgressDialog.show(a, "",

                a.getString(loadingResId), true, false);

        dialog.show();

        return dialog;

    }



    public static ProgressDialog show(Context a, String loadingText) {

        ProgressDialog dialog = ProgressDialog.show(a, "",

                loadingText, true, false);

        dialog.show();

        return dialog;

    }
}
