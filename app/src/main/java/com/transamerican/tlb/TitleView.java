package com.transamerican.tlb;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.LinearInterpolator;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class TitleView extends AbstractAnimatedView implements OnTouchListener {

    public static final int SECTION_ABOUT_US = 0;
    public static final int SECTION_UNIVERSAL_LIFE = 1;
    public static final int SECTION_TERM_LIFE = 2;
    public static final int SECTION_LIBRARY = 3;

    private static final String TAG = TitleView.class.getSimpleName();
    private static final boolean DEBUG = false;
    private static final int DEBUG_ALPHA = 100;

    private static final int DESIGN_WIDTH = 2560, DESIGN_HEIGHT = 1286;  //DESIGN_HEIGHT = 1536
    private float screenScale, translateScale;

    private Vector<Sprite> mainMenuItemSprites;
    private Vector<Sprite> zSortedSprites;

    private Vector<Sprite> subMenuItems;
    private Sprite subMenuBackground;

    //    private Sprite background;
    private Sprite cover;

    private Paint pa, pb, pc, pd, grey;

    private OnSectionClickListener listener;

    private Rect ia, ib, ic;

    private int width, height;

    //16:9
    private int BASE = -50;
    private int TARGET_X[] = {BASE, BASE + 505, BASE + 1010, BASE + 1515};
    private int TARGET_X_M[][] = {{BASE + 310, BASE + 688, BASE + 1065}, {BASE + 330, BASE + 828}, {BASE + 330, BASE + 760, BASE + 1269}};

    //Real Use
    private int FINAL_TARGET_X[] = {BASE, BASE + 505, BASE + 1010, BASE + 1515};
    private int FINAL_TARGET_X_M[][] = {{BASE + 310, BASE + 688, BASE + 1065}, {BASE + 330, BASE + 828}, {BASE + 330, BASE + 760, BASE + 1269}};

    public OnSectionClickListener getListener() {
        return listener;
    }

    public void setListener(OnSectionClickListener listener) {
        this.listener = listener;
    }

    private Bitmap raycast;
    private Canvas raycastCanvas;

    public TitleView(Context con, AttributeSet attr) {
        super(con, attr);
        this.setWillNotDraw(false);

        //Default 16:9
        FINAL_TARGET_X = TARGET_X;
        FINAL_TARGET_X_M = TARGET_X_M;
        int leftDrawable = R.drawable.left;
        int delta = 0;

        double ratio = getResources().getDisplayMetrics().widthPixels * 1.0 / getResources().getDisplayMetrics().heightPixels;
        if (ratio < 1.6) {
            //4.3
            delta = 120;
            leftDrawable = R.drawable.left_new;
        }


        for (int x = 0; x < FINAL_TARGET_X.length; x++) {
            FINAL_TARGET_X[x] = FINAL_TARGET_X[x] / 2 - delta;
        }

        for (int x = 0; x < FINAL_TARGET_X_M.length; x++) {
            for (int y = 0; y < FINAL_TARGET_X_M[x].length; y++) {
                FINAL_TARGET_X_M[x][y] = FINAL_TARGET_X_M[x][y] / 2 - delta;
            }
        }

        screenScale = 1;

        subMenuItems = new Vector<Sprite>();

        Bitmap ba, bb, bc, bd;
        Bitmap aa, ab, ac, ad;
        Matrix ma, mb, mc, md;

        mainMenuItemSprites = new Vector<Sprite>();
        zSortedSprites = new Vector<Sprite>();
        Options opts = new Options();
        opts.inScaled = true;
        opts.inTargetDensity = 160;

        translateScale = 1;

        raycast = Bitmap.createBitmap(2560, DESIGN_HEIGHT, Bitmap.Config.ARGB_8888);
        raycastCanvas = new Canvas();
        raycastCanvas.setBitmap(raycast);
        aa = BitmapFactory.decodeResource(getResources(),
                R.raw.bu_aboutus_normal, opts);
        ab = BitmapFactory.decodeResource(getResources(),
                R.raw.bu_universallife_normal, opts);

        ac = BitmapFactory.decodeResource(getResources(),
                R.raw.bu_termlife_normal, opts);
        ad = BitmapFactory.decodeResource(getResources(), R.raw.bu_library,
                opts);

        ba = BitmapFactory.decodeResource(getResources(),
                R.raw.bu_aboutus_overlayer, opts);
        bb = BitmapFactory.decodeResource(getResources(),
                R.raw.bu_universallife_onverlayer, opts);

        bc = BitmapFactory.decodeResource(getResources(),
                R.raw.bu_termlife_overlayer, opts);
        bd = BitmapFactory.decodeResource(getResources(),
                R.raw.bu_library_overlayer, opts);

        pa = createPaint(Color.RED, DEBUG_ALPHA);
        pb = createPaint(Color.YELLOW, DEBUG_ALPHA);
        pc = createPaint(Color.GREEN, DEBUG_ALPHA);
        pd = createPaint(Color.BLUE, DEBUG_ALPHA);
        grey = createPaint(Color.GRAY, 255);

//        Bitmap bbg = BitmapFactory.decodeResource(getResources(), R.drawable.tlb, opts);
//        background = new Sprite(bbg, bbg, new Matrix(), 0, -100, grey);
        Bitmap cg = BitmapFactory.decodeResource(getResources(), leftDrawable, opts);
        cover = new Sprite(cg, cg, new Matrix(), 0, 10000, grey);
//        zSortedSprites.add(background);
        zSortedSprites.add(cover);

        createSubMenu(0);

        ma = new Matrix();
        mb = new Matrix();
        mc = new Matrix();
        md = new Matrix();

        int rectHeight = (int) (aa.getHeight()); //-bottomMenuPx*2);	//Image Height - Bottom Menu - Sub Menu

        float aScale = 1;


        ia = new Rect();
        ib = new Rect();
        ic = new Rect();

        mainMenuItemSprites.add(new Sprite(ba, aa, ma, INDEX_ABOUT, 130, pa));
        mainMenuItemSprites.add(new Sprite(bb, ab, mb, INDEX_UNIVERSAL, 120, pb));
        mainMenuItemSprites.add(new Sprite(bc, ac, mc, INDEX_TERM, 110, pc));
        mainMenuItemSprites.add(new Sprite(bd, ad, md, INDEX_LIBRARY, 100, pd));

        zSortedSprites.addAll(mainMenuItemSprites);

        zSortedSprites.addAll(subMenuItems);
        zSortedSprites.add(subMenuBackground);

        sort();

        init();

        this.setOnTouchListener(this);

        prevIndex = -1;
    }

    private Sprite findSelected(int x, int y) {
        sortReverse();
        raycastCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        int pixel, alpha;
        Sprite selected=null;
        for (Sprite s : zSortedSprites) {
            if (s.drawBitmap != null) {

//                Log.i("", "Sprite: " + s.index + ", Location X: " + x + ", Y: " + y + ", raycast.getWidth(): " + raycast.getWidth() + ", raycast.getHeight(): " + raycast.getHeight());

                raycastCanvas.drawBitmap(s.drawBitmap, s.matrix, null);
                if (x > raycast.getWidth() || y > raycast.getHeight()) {
                    continue;
                }

                pixel = raycast.getPixel(x, y);
                alpha = Color.alpha(pixel);
                if (alpha != 0) {
                    selected= s;
                    break;
                }
            }
        }
        sort();
        return selected;
    }

    private void sort() {
        Collections.sort(zSortedSprites, new Comparator<Sprite>() {

            @Override
            public int compare(Sprite lhs, Sprite rhs) {
                return lhs.z - rhs.z;
            }

        });
    }

    private void sortReverse() {
        Collections.sort(zSortedSprites, new Comparator<Sprite>() {

            @Override
            public int compare(Sprite lhs, Sprite rhs) {
                return rhs.z - lhs.z;
            }

        });
    }

    private void freeSubMenu() {
        for (Sprite s : subMenuItems) {
            if (s.normal != null) {
                s.normal.recycle();
                if (s.hover != null) {
                    if (s.normal != s.hover) {
                        s.hover.recycle();
                    }
                }
            }

            s.normal = null;
            s.drawBitmap = null;
            s.hover = null;
            zSortedSprites.remove(s);
        }
        subMenuItems.clear();
    }

    private void createSubMenu(int i) {
        freeSubMenu();
        Options opts = new Options();
        opts.inScaled = true;
        opts.inTargetDensity = 160;


//                getResources(), R.color.main_menu_sub_menu_bg ,opts);
        if (subMenuBackground == null) {
            Bitmap subMenuBg = Bitmap.createBitmap(2560, 200, Bitmap.Config.RGB_565);
            Canvas c = new Canvas(subMenuBg);
            Paint p = new Paint();
            p.setColor(getResources().getColor(R.color.main_menu_sub_menu_bg));
            c.drawRect(0, 0, 2560, 200, p);
            subMenuBackground = new Sprite(subMenuBg, subMenuBg, new Matrix(), 0, 450, pd);
        }

        subMenuItems.clear();
        switch (i) {
            case 0:
                Bitmap off1 = BitmapFactory.decodeResource(getResources(), R.raw.sub_menu_who_who_off, opts);
                Bitmap on1 = BitmapFactory.decodeResource(getResources(), R.raw.sub_menu_who_who_on, opts);
                subMenuItems.add(new Sprite(off1, on1, new Matrix(), INDEX_ABOUT_WHO, 500, pd));

                Bitmap off2 = BitmapFactory.decodeResource(getResources(), R.raw.bu_strengths_off, opts);
                Bitmap on2 = BitmapFactory.decodeResource(getResources(), R.raw.bu_strengths_on, opts);
                subMenuItems.add(new Sprite(off2, on2, new Matrix(), INDEX_ABOUT_STRENGTH, 600, pd));

                Bitmap off3 = BitmapFactory.decodeResource(getResources(), R.raw.bu_contact_off, opts);
                Bitmap on3 = BitmapFactory.decodeResource(getResources(), R.raw.bu_contact_on, opts);
                subMenuItems.add(new Sprite(off3, on3, new Matrix(), INDEX_ABOUT_CONTACT, 500, pd));

                break;
            case 1:
                Bitmap off4 = BitmapFactory.decodeResource(getResources(), R.raw.universal_ife_ul_button1_off, opts);
                Bitmap on4 = BitmapFactory.decodeResource(getResources(), R.raw.universal_ife_ul_button1_on, opts);
                subMenuItems.add(new Sprite(off4, on4, new Matrix(), INDEX_UNIVERSAL_ONE, 500, pd));

                Bitmap off5 = BitmapFactory.decodeResource(getResources(), R.raw.universal_ife_ul_button2_off, opts);
                Bitmap on5 = BitmapFactory.decodeResource(getResources(), R.raw.universal_ife_ul_button2_on, opts);
                subMenuItems.add(new Sprite(off5, on5, new Matrix(), INDEX_UNIVERSAL_TWO, 600, pd));
                break;
            case 2:
                Bitmap off8 = BitmapFactory.decodeResource(getResources(), R.raw.sub_menu_term_what_off, opts);
                Bitmap on8 = BitmapFactory.decodeResource(getResources(), R.raw.sub_menu_term_what_on, opts);
                subMenuItems.add(new Sprite(off8, on8, new Matrix(), INDEX_TERM_WHAT, 500, pd));

                Bitmap off6 = BitmapFactory.decodeResource(getResources(), R.raw.sub_menu_term_super_off, opts);
                Bitmap on6 = BitmapFactory.decodeResource(getResources(), R.raw.sub_menu_term_super_on, opts);
                subMenuItems.add(new Sprite(off6, on6, new Matrix(), INDEX_TERM_SUPER, 500, pd));

                Bitmap off7 = BitmapFactory.decodeResource(getResources(), R.raw.sub_menu_term_rop_off, opts);
                Bitmap on7 = BitmapFactory.decodeResource(getResources(), R.raw.sub_menu_term_rop_on, opts);
                subMenuItems.add(new Sprite(off7, on7, new Matrix(), INDEX_TERM_ROP, 600, pd));

                break;
        }

        for (Sprite s : subMenuItems) {
            zSortedSprites.add(s);
        }
        sort();
    }

    public static final int INDEX_TERM = 1, INDEX_TERM_WHAT = 2, INDEX_TERM_SUPER = 3, INDEX_TERM_ROP = 4;
    public static final int INDEX_ABOUT = 5, INDEX_ABOUT_WHO = 6, INDEX_ABOUT_STRENGTH = 7, INDEX_ABOUT_CONTACT = 8;
    public static final int INDEX_UNIVERSAL = 9, INDEX_UNIVERSAL_ONE = 10, INDEX_UNIVERSAL_TWO = 11;
    public static final int INDEX_LIBRARY = 12;

    private Paint createPaint(int color, int alpha) {
        Paint p = new Paint();
        p.setColor(color);
        p.setAlpha(alpha);
        return p;
    }

    private void init() {
        for (int i = 0; i < mainMenuItemSprites.size(); i++) {
            setPosition(mainMenuItemSprites.get(i), -1000, 0);
        }

        setPosition(subMenuBackground, 0, -1000); //TODO set out of screen
        for (int i = 0; i < subMenuItems.size(); i++) {
            setPosition(subMenuItems.get(i), -1000, 0);
        }
    }


    public int getItems() {
        return mainMenuItemSprites.size();
    }

    public void setPosition(Sprite s, int x, int y) {
        s.matrix.reset();
        s.matrix.preTranslate(x, y);
//        s.touchRect.offsetTo(x, y);
        invalidate();
    }

    public void rectifyRect() {


    }

    @Override
    public void onMeasure(int w, int h) {
        super.onMeasure(w, h);
        int w1 = getMeasuredWidth();
        int h1 = getMeasuredHeight();
//        screenScale=2.0f/3;
//                screenScale=3.0f/4;
//        screenScale=w1*1.0f/2560;
        screenScale = (h1 * 1.0f / 1240) * 2;
    }

    protected void onDraw(Canvas canvas) {

        canvas.drawRect(0, 0, (int) (DESIGN_WIDTH), (int) (DESIGN_HEIGHT), grey);
        canvas.scale(screenScale, screenScale);


        for (Sprite s : zSortedSprites) {
//            Log.i(TAG," drawing "+s+" "+s.drawBitmap+" "+s.matrix);
            if (s.drawBitmap != null) {
                canvas.drawBitmap(s.drawBitmap, s.matrix, null);
            }
            if (DEBUG) {
//                canvas.drawRect(s.touchRect, s.debugPaint);
            }
        }

    }

    private Sprite lastTouched;
private boolean switchedTouch;
    @Override
    public boolean onTouch(View v, MotionEvent event) {
//        Log.i(TAG, "touch x " + event.getX() + ", y " + event.getY());
        int action = event.getAction(), x = (int) (event.getX() / screenScale), y = (int) (event
                .getY() / screenScale);

        if (action == MotionEvent.ACTION_DOWN) {
            Sprite s = findSelected(x, y);
            lastTouched = s;
            if (s != null) {
                Log.i(TAG, "touch on " + s.index + " listener is " + listener);
                s.drawBitmap = s.hover;
            }
            switchedTouch=false;
            invalidate();
        } else if (action == MotionEvent.ACTION_MOVE) {
            Sprite s = findSelected(x, y);
            if (lastTouched != null ) {
                if (s != lastTouched) {
                    // hover different
                    switchedTouch=true;
                    lastTouched.drawBitmap = lastTouched.normal;
                    lastTouched = s;
                    if(s!=null) {
                        lastTouched.drawBitmap = lastTouched.hover;
                    }
                } else {
// hover same
                }
            }else{
                //was hover nothing
                lastTouched=s;
                if(lastTouched!=null){
                    //show hover state
                    lastTouched.drawBitmap=lastTouched.hover;
                }
            }
            invalidate();
        } else if (action == MotionEvent.ACTION_UP) {
            Sprite s = findSelected(x, y);
            if(s!=null) {

                if (lastTouched == s) {
                    if (listener != null && !switchedTouch) {
                        listener.onClick(s.index);
                    }
                    for (Sprite sprite : mainMenuItemSprites) {
                        if (sprite.index != s.index) {
                            sprite.drawBitmap = sprite.normal;
                        }
                    }
                } else {
                    s.drawBitmap = s.normal;
                }
            }
            invalidate();
        }


        return true;
    }

    public interface OnSectionClickListener {
        public void onClick(int i);
    }

    private class Sprite {
        Bitmap normal, hover;
        Matrix matrix;
        Bitmap drawBitmap;
        int z;
        Paint debugPaint;
        int index;

        public Sprite(Bitmap normal, Bitmap hover, Matrix mat, int index,
                      int z, Paint d) {
            Log.i(TAG, "normal is " + normal + " hover is " + hover);
            this.normal = normal;
            this.hover = hover;
            this.matrix = mat;
            drawBitmap = normal;
            this.z = z;
            this.debugPaint = d;
            this.index = index;
        }
    }

    private void createAnimation(Sprite t, int from, int to, int fy, int ty, int duration, int delay) {
        ValueAnimator anim = ValueAnimator.ofInt(0, 100);
        anim.setDuration(duration);
        anim.setStartDelay(delay);
        anim.setInterpolator(new LinearInterpolator());
        anim.addUpdateListener(new MyAnimatorUpdateListener(t, from, to, fy, ty));
        anim.start();
    }

    private class MyAnimatorUpdateListener implements ValueAnimator.AnimatorUpdateListener {
        private Sprite target;
        private int fx, tx, fy, ty;

        public MyAnimatorUpdateListener(Sprite target, int fx, int tx, int fy, int ty) {
            this.target = target;
            this.fx = fx;
            this.tx = tx;
            this.fy = fy;
            this.ty = ty;
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            int f = (Integer) animation.getAnimatedValue();
            setPosition(target, fx + (tx - fx) * f / 100, fy + (ty - fy) * f / 100);
        }

    }

    public void startMainMenuAnimation() {
        createAnimation(mainMenuItemSprites.get(0), -1000, FINAL_TARGET_X[0], 0, 0, 600, 0);
        createAnimation(mainMenuItemSprites.get(1), FINAL_TARGET_X[0], FINAL_TARGET_X[1], 0, 0, 600, 650);
        createAnimation(mainMenuItemSprites.get(2), FINAL_TARGET_X[1], FINAL_TARGET_X[2], 0, 0, 600, 1300);
        createAnimation(mainMenuItemSprites.get(3), FINAL_TARGET_X[2], FINAL_TARGET_X[3], 0, 0, 600, 1950);
    }

    private int prevIndex;

    public void startSubMenuAnimation(int index) {
        if (prevIndex != index) {
            createSubMenu(index);
        }
        prevIndex = index;

        createAnimation(subMenuBackground, FINAL_TARGET_X_M[prevIndex][0], FINAL_TARGET_X_M[prevIndex][0], 640, 560, 600, 0);
        for (int i = 0; i < subMenuItems.size(); i++) {
            createAnimation(subMenuItems.get(i), FINAL_TARGET_X_M[prevIndex][i], FINAL_TARGET_X_M[prevIndex][i], 640, 560, 600, 0);
        }
    }

    public void hideSubMenuAnimation() {
        createAnimation(subMenuBackground, FINAL_TARGET_X_M[prevIndex][0], FINAL_TARGET_X_M[prevIndex][0], 560, 640, 600, 0);
        for (int i = 0; i < subMenuItems.size(); i++) {
            createAnimation(subMenuItems.get(i), FINAL_TARGET_X_M[prevIndex][i], FINAL_TARGET_X_M[prevIndex][i], 560, 640, 600, 0);
        }

    }

    // free bitmaps
    public void free() {
        for (Sprite s : zSortedSprites) {
            if (s.hover != null) {
                s.hover.recycle();
            }
            if (s.normal != null) {
                s.normal.recycle();
            }
            s.hover = null;
            s.normal = null;
        }

//        if (background != null) {
//            if (background.normal != null) {
//                background.normal.recycle();
//            }
//        }

        if (cover != null) {
            if (cover.normal != null) {
                cover.normal.recycle();
            }
        }

        cover = null;
//        background = null;
    }

    public void switchMainMenuButton(boolean isOn) {
        for (Sprite sprite : mainMenuItemSprites) {
            sprite.drawBitmap = isOn ? sprite.hover : sprite.normal;
        }
        invalidate();
    }
}
