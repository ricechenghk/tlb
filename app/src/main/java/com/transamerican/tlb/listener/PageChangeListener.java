package com.transamerican.tlb.listener;

/**
 * Created by Rice on 21/1/15.
 */
public interface PageChangeListener {
    public void goToPage(int page);
}
