package com.transamerican.tlb.listener;

/**
 * Created by Rice on 21/1/15.
 */
public interface MainSectionChangeListener {
    public void onSectionChange(boolean showExtra);
}
