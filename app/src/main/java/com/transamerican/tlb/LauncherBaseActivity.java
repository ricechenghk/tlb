package com.transamerican.tlb;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.transamerican.tlb.sync.AppDataSource;


public abstract class LauncherBaseActivity extends Activity {
    private static final String TAG = LauncherBaseActivity.class.getSimpleName();

    protected abstract int getLayout();

    protected abstract Class getMainActivity();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());

        Log.i("", "Density of Device is: " + getResources().getDisplayMetrics().density);
        Log.i("", "Density DPI of Device is: " + getResources().getDisplayMetrics().densityDpi);
        Log.i("", "Density scaled of Device is: " + getResources().getDisplayMetrics().scaledDensity);
        Log.i("", "Density width of Device is: " + getResources().getDisplayMetrics().widthPixels);
        Log.i("", "Density height of Device is: " + getResources().getDisplayMetrics().heightPixels);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(AppDataSource.getInstance().isShowPushAlert(LauncherBaseActivity.this))
                {
                    AppDataSource.getInstance().setShowPushAlert(LauncherBaseActivity.this, false);
                    new AlertDialog.Builder(LauncherBaseActivity.this)
//                    .setTitle("Delete entry")
                            .setMessage(getResources().getString(R.string.push_alert))
                            .setCancelable(false)
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    goToMain();
                                }
                            })
                            .show();
                }
                else {
                    goToMain();
                }
            }
        }, 2000);



    }

    protected void onResume() {
        super.onResume();
    }

    private void goToMain()
    {
        finish();
        Intent intent = new Intent(LauncherBaseActivity.this, getMainActivity());
        startActivity(intent);
    }

}
