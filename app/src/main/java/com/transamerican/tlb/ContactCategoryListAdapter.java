package com.transamerican.tlb;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.transamerican.tlb.model.ContactCategory;

import java.util.ArrayList;
import java.util.List;

public class ContactCategoryListAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<ContactCategory> contactCategory = new ArrayList<ContactCategory>();

    private int selectedPosition=0;

    public ContactCategoryListAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void replaceWith(List<ContactCategory> contactCategory) {
        this.contactCategory = contactCategory;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return contactCategory.size();
    }

    @Override
    public ContactCategory getItem(int position) {
        return contactCategory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.list_item_contact_category, viewGroup, false);

            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view.findViewById(R.id.contact_category_title);
            viewHolder.arrow = (ImageView) view.findViewById(R.id.arrow_selector);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        ContactCategory category = getItem(position);

        viewHolder.title.setText(category.getTitle());
        viewHolder.arrow.setSelected(position==selectedPosition);
        return view;

    }

    static class ViewHolder {
        TextView title;
        ImageView arrow;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }
}
