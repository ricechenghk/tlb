package com.transamerican.tlb.setting;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.transamerican.tlb.ContactCategoryListAdapter;
import com.transamerican.tlb.ContactListAdapter;
import com.transamerican.tlb.MainBaseActivity;
import com.transamerican.tlb.R;
import com.transamerican.tlb.model.Contact;
import com.transamerican.tlb.model.ContactCategory;
import com.transamerican.tlb.sync.AppDataSource;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;
import hk.com.playmore.syncframework.util.RunnableArgument;
import hk.com.playmore.syncframework.util.SyncFragment;

/**
 * Created by Rice on 24/1/15.
 */
public class ContactInfoFragment extends SyncFragment {

    @InjectView(R.id.contact_category_list)
    public ListView contactCategoryList;
    public ContactCategoryListAdapter contactCategoryListAdapter;

    @InjectView(R.id.contact_list)
    public ListView contactList;
    public ContactListAdapter contactListAdapter;

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_info, container, false);
        ButterKnife.inject(this, view);

        contactCategoryListAdapter = new ContactCategoryListAdapter(getActivity());
        contactCategoryList.setAdapter(contactCategoryListAdapter);

        contactListAdapter = new ContactListAdapter(getActivity());
        contactList.setAdapter(contactListAdapter);

        ((MainBaseActivity)getActivity()).showLoading();
        AppDataSource.getInstance().getContactCategoryFromServer(getActivity(), this, new RunnableArgument() {
            @Override
            public void run(String object) {
                AppDataSource.getInstance().getContactFromServer(getActivity(), ContactInfoFragment.this, new RunnableArgument() {
                    @Override
                    public void run(String object) {
                        ((MainBaseActivity)getActivity()).hideLoading();
                        List<ContactCategory> categoryList = AppDataSource.getInstance().getContactCategory(getActivity());
                        contactCategoryListAdapter.replaceWith(categoryList);

                        if (categoryList.size() > 0) {
                            ContactCategory category = categoryList.get(0);
                            List<Contact> contactList = AppDataSource.getInstance().getContact(getActivity(), category.getId());
                            contactListAdapter.replaceWith(contactList);
                        }
                    }
                });
            }
        });

        return view;
    }

    @OnClick(R.id.btn_close)
    public void onCloseClicked() {
        getFragmentManager().popBackStack();
    }


    @OnItemClick(R.id.contact_category_list)
    public void onContactCategoryItemClicked(AdapterView<?> parent, View view, int position, long id) {
        contactCategoryListAdapter.setSelectedPosition(position);
        contactCategoryListAdapter.notifyDataSetChanged();

        ContactCategory category = contactCategoryListAdapter.getItem(position);
        List<Contact> contactList = AppDataSource.getInstance().getContact(getActivity(), category.getId());
        contactListAdapter.replaceWith(contactList);
    }

}
