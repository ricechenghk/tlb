package com.transamerican.tlb.setting;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transamerican.tlb.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Rice on 24/1/15.
 */
public class SettingPrivacyFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting_privacy, container, false);
        ButterKnife.inject(this, view);

        return view;
    }

    @OnClick(R.id.btn_close)
    public void onCloseClicked() {
        getFragmentManager().popBackStack();
    }

}
