package com.transamerican.tlb.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transamerican.tlb.R;
import com.transamerican.tlb.TutorialActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Rice on 24/1/15.
 */
public class SettingFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        ButterKnife.inject(this, view);

        return view;
    }

    @OnClick(R.id.main)
    public void onMainClicked() {
        getFragmentManager().popBackStack();
    }

    @OnClick(R.id.setting_terms)
    public void onTermsClicked() {
        SettingTermFragment fr = new SettingTermFragment();
        popAddFragment(fr);
    }

    @OnClick(R.id.setting_privacy)
    public void onPrivacyClicked() {
        SettingPrivacyFragment fr = new SettingPrivacyFragment();
        popAddFragment(fr);
    }

    @OnClick(R.id.setting_help)
    public void onHelpClicked() {
        getFragmentManager().popBackStack();
        Intent intent = new Intent(getActivity(), TutorialActivity.class);
        getActivity().startActivity(intent);
    }

    public void popAddFragment(Fragment fr) {
        getFragmentManager().popBackStack();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fr);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
