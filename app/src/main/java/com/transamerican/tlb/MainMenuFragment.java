package com.transamerican.tlb;

import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.transamerican.tlb.about.AboutUsFragment;
import com.transamerican.tlb.library.LibraryFragment;
import com.transamerican.tlb.library.LoginFragment;
import com.transamerican.tlb.listener.MainSectionChangeListener;
import com.transamerican.tlb.sync.AppDataSource;
import com.transamerican.tlb.term.TermLifeFragment;
import com.transamerican.tlb.universal.UniversalLifeFragment;

public class MainMenuFragment extends Fragment implements TitleView.OnSectionClickListener, OnClickListener{
    private static final String TAG = MainMenuFragment.class.getSimpleName();
    TitleView titleView;

    MainSectionChangeListener mainSectionChangeListener;

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);
        titleView=(TitleView)view.findViewById(R.id.title_view);
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        DisplayMetrics oo = new DisplayMetrics();
        display.getMetrics(oo);

        titleView.setListener(this);
        Point size = new Point();
        display.getSize(size);

        return view;
    }
    private boolean submenushown;

    @Override
    public void onClick(int i) {
        Log.i(TAG, "vs rect " + i + " is clicked");

        boolean isSectionChanged = false;
        boolean showExtra = false;

        if (i == TitleView.INDEX_LIBRARY) {

            boolean isLogin = AppDataSource.getInstance().isLogin(getActivity());
            if (isLogin) {
                isSectionChanged = true;
                showExtra = true;
                LibraryFragment fr = new LibraryFragment();
                replaceFragment(fr);
            } else {
                LoginFragment fr = new LoginFragment();
                fr.setMainSectionChangeListener(mainSectionChangeListener);
                addFragment(fr);
            }

        } else if (i == TitleView.INDEX_ABOUT) {
            if(submenushown){
                titleView.hideSubMenuAnimation();
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        titleView.startSubMenuAnimation(0);
                    }
                },600);
            }else {
                titleView.startSubMenuAnimation(0);
                submenushown = true;
            }
//            submenushown=!submenushown;
        }else if(i==TitleView.INDEX_TERM){
            if(submenushown){
                titleView.hideSubMenuAnimation();
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        titleView.startSubMenuAnimation(2);
                    }
                },600);

            }else {
                titleView.startSubMenuAnimation(2);
                submenushown = true;
            }
//            submenushown=!submenushown;
        }else if(i==TitleView.INDEX_UNIVERSAL) {

            if(submenushown){
                titleView.hideSubMenuAnimation();
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        titleView.startSubMenuAnimation(1);
                    }
                },600);

            }else {
                titleView.startSubMenuAnimation(1);
                submenushown = true;
            }
//            submenushown=!submenushown;


        } else if (i == TitleView.INDEX_ABOUT_WHO || i == TitleView.INDEX_ABOUT_STRENGTH || i == TitleView.INDEX_ABOUT_CONTACT) {
            isSectionChanged = true;
            AboutUsFragment fr = new AboutUsFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("section", i);
            fr.setArguments(bundle);
            replaceFragment(fr);

        } else if (i == TitleView.INDEX_UNIVERSAL_ONE || i == TitleView.INDEX_UNIVERSAL_TWO) {
            isSectionChanged = true;
            UniversalLifeFragment fr = new UniversalLifeFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("section", i);
            fr.setArguments(bundle);
            replaceFragment(fr);

        } else if (i == TitleView.INDEX_TERM_WHAT || i == TitleView.INDEX_TERM_SUPER || i == TitleView.INDEX_TERM_ROP) {
            isSectionChanged = true;
            TermLifeFragment fr = new TermLifeFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("section", i);
            fr.setArguments(bundle);
            replaceFragment(fr);
        }

        if (isSectionChanged) {
            if (mainSectionChangeListener != null) {
                mainSectionChangeListener.onSectionChange(showExtra);
            }
        }
    }

    public void onResume(){
        super.onResume();
        titleView.switchMainMenuButton(true);
        titleView.startMainMenuAnimation();
    }


    public void onDestroy(){
        super.onDestroy();
        titleView.free();
    }

    @Override
    public void onClick(View v) {
//		if(v.getId()==R.id.btn_strength)
//		{
//			Log.i("", "Strength is clicked");
//		}
    }

    public void replaceFragment(Fragment fr) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fr);
        fragmentTransaction.commit();
    }

    public void addFragment(Fragment fr) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fr);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void setMainSectionChangeListener(MainSectionChangeListener mainSectionChangeListener) {
        this.mainSectionChangeListener = mainSectionChangeListener;
    }
}
