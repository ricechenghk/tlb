package com.transamerican.tlb.universal.page;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transamerican.tlb.R;

import butterknife.ButterKnife;

/**
 * Created by Rice on 18/1/15.
 */
public class UniversalLifeWhatPage7Fragment extends UniversalVideoPagerFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_universal_what_page7, container, false);
        ButterKnife.inject(this, v);

        initVideo();

        return v;
    }

    public String getVideo() {
        return "crediting_interest_demo_retina.mp4";
    }


    @Override
    public int getTitle() {
        return R.string.universal_what_is_universal_life_plan;
    }

    @Override
    public int getSlider() {
        return R.drawable.about_us_slide8_7;
    }

}
