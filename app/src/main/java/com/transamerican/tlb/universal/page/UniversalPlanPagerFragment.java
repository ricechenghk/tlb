package com.transamerican.tlb.universal.page;

import com.transamerican.tlb.R;
import com.transamerican.tlb.base.PagerFragment;

import butterknife.OnClick;

/**
 * Created by Rice on 19/1/15.
 */
public abstract class UniversalPlanPagerFragment extends PagerFragment {

    @OnClick(R.id.universal_life_plan_private_series)
    public void onSection1Clicked() {
        if (listener != null) {
            listener.goToPage(9);
        }
    }

    @OnClick(R.id.universal_life_plan_universal_life)
    public void onSection2Clicked() {
        if (listener != null) {
            listener.goToPage(11);
        }
    }

    @OnClick(R.id.universal_life_plan_universal_life_plus)
    public void onSection3Clicked() {
        if (listener != null) {
            listener.goToPage(12);
        }
    }

    @OnClick(R.id.universal_life_plan_universal_life_max)
    public void onSection4Clicked() {
        if (listener != null) {
            listener.goToPage(13);
        }
    }


}
