package com.transamerican.tlb.universal;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transamerican.tlb.R;
import com.transamerican.tlb.TitleView;
import com.transamerican.tlb.base.PagerFragment;
import com.transamerican.tlb.base.SectionBaseFragment;
import com.transamerican.tlb.universal.page.UniversalLifePlanPage1Fragment;
import com.transamerican.tlb.universal.page.UniversalLifePlanPage2Fragment;
import com.transamerican.tlb.universal.page.UniversalLifePlanPage3Fragment;
import com.transamerican.tlb.universal.page.UniversalLifePlanPage4Fragment;
import com.transamerican.tlb.universal.page.UniversalLifePlanPage5Fragment;
import com.transamerican.tlb.universal.page.UniversalLifeWhatPage1Fragment;
import com.transamerican.tlb.universal.page.UniversalLifeWhatPage2Fragment;
import com.transamerican.tlb.universal.page.UniversalLifeWhatPage3Fragment;
import com.transamerican.tlb.universal.page.UniversalLifeWhatPage4Fragment;
import com.transamerican.tlb.universal.page.UniversalLifeWhatPage5Fragment;
import com.transamerican.tlb.universal.page.UniversalLifeWhatPage6Fragment;
import com.transamerican.tlb.universal.page.UniversalLifeWhatPage7Fragment;
import com.transamerican.tlb.universal.page.UniversalLifeWhatPage8Fragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class UniversalLifeFragment extends SectionBaseFragment {

    public static final String TAG = UniversalLifeFragment.class.getName();

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_universal, container, false);
        ButterKnife.inject(this, view);

        initScreen();

        if (getArguments() != null) {
            int sectionId = getArguments().getInt("section");
            switch (sectionId) {
                case TitleView.INDEX_UNIVERSAL_TWO:
                    goToPage(9);
                    break;
            }
        }

        return view;
    }


    @OnClick(R.id.sub_menu_btn_1)
    public void onSubMenuBtn1Clicked() {
        goToPage(1);
    }

    @OnClick(R.id.sub_menu_btn_2)
    public void onSubMenuBtn2Clicked() {
        goToPage(9);
    }


    @Override
    public PagerFragment getPagerFragment(int pos) {

        PagerFragment fr = null;
        switch (pos) {

            case 0:
                fr = new UniversalLifeWhatPage1Fragment();
                resetInfo(fr.getTitle(), fr.getSlider(), fr.isVideoExist());
                fr.onPageSelected();    //For init case, to start the video
                break;
            case 1:
                fr = new UniversalLifeWhatPage2Fragment();
                break;
            case 2:
                fr = new UniversalLifeWhatPage3Fragment();
                break;
            case 3:
                fr = new UniversalLifeWhatPage4Fragment();
                break;
            case 4:
                fr = new UniversalLifeWhatPage5Fragment();
                break;
            case 5:
                fr = new UniversalLifeWhatPage6Fragment();
                break;
            case 6:
                fr = new UniversalLifeWhatPage7Fragment();
                break;
            case 7:
                fr = new UniversalLifeWhatPage8Fragment();
                break;
            case 8:
                fr = new UniversalLifePlanPage1Fragment();
                break;
            case 9:
                fr = new UniversalLifePlanPage2Fragment();
                break;
            case 10:
                fr = new UniversalLifePlanPage3Fragment();
                break;
            case 11:
                fr = new UniversalLifePlanPage4Fragment();
                break;
            case 12:
                fr = new UniversalLifePlanPage5Fragment();
                break;
            default:
                fr = new UniversalLifeWhatPage1Fragment();
                break;
        }

        return fr;
    }

    @Override
    public int getPagerCount() {
        return 13;
    }
}
