package com.transamerican.tlb.universal.page;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.transamerican.tlb.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Rice on 18/1/15.
 */
public class UniversalLifeWhatPage2Fragment extends UniversalPagerFragment {

    @InjectView(R.id.detail_sub_img)
    ImageView image;

    boolean isLeftClicked = false;
    boolean isRightClicked = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_universal_what_page2, container, false);
        ButterKnife.inject(this, v);

        return v;
    }

    @Override
    public void onPageSelected() {
        image.setImageResource(R.drawable.universal_life_flex_off_trans_off);
        isRightClicked = false;
        isLeftClicked = false;
    }

    @Override
    public void onPageExited() {

    }

    @Override
    public int getTitle() {
        return R.string.universal_what_is_universal_life_plan;
    }

    @Override
    public int getSlider() {
        return R.drawable.about_us_slide8_2;
    }

    @Override
    public boolean isVideoExist() {
        return false;
    }

    @Override
    public void onReplayClicked() {

    }

    @OnClick(R.id.detail_sub_img_overlay_left)
    public void onLeftOverlayClicked() {
        isRightClicked = false;
        if (!isLeftClicked) {
            image.setImageResource(R.drawable.universal_life_flex_on_trans_off);
            isLeftClicked = true;
        } else {
            image.setImageResource(R.drawable.universal_life_flex_off_trans_off);
            isLeftClicked = false;
        }
    }

    @OnClick(R.id.detail_sub_img_overlay_right)
    public void onRightOverlayClicked() {
        isLeftClicked = false;
        if (!isRightClicked) {
            image.setImageResource(R.drawable.universal_life_flex_off_trans_on);
            isRightClicked = true;
        } else {
            image.setImageResource(R.drawable.universal_life_flex_off_trans_off);
            isRightClicked = false;
        }
    }

}
