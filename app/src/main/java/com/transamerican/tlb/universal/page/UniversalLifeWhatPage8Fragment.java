package com.transamerican.tlb.universal.page;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.transamerican.tlb.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectViews;
import butterknife.OnClick;

/**
 * Created by Rice on 18/1/15.
 */
public class UniversalLifeWhatPage8Fragment extends UniversalVideoPagerFragment {

    @InjectViews({R.id.detail_sub_checkbox1, R.id.detail_sub_checkbox2, R.id.detail_sub_checkbox3})
    List<ImageView> checkboxs;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_universal_what_page8, container, false);
        ButterKnife.inject(this, v);

        isStartWhenPrepared = false;

        initVideo();

        return v;
    }

    @Override
    public String getVideo() {
        return "flexible_arrangment_01_demoA_retina.mp4";
    }

    @Override
    public void onPageSelected() {
        resetCheckbox();
        prepareVideo(getVideo());
    }

    @Override
    public void onPageExited() {

    }

    @Override
    public void onPrepared(MediaPlayer arg0) {
        // TODO Auto-generated method stub
        Log.i("", "Media is prepared!!!!");
        handleAspectRatio();
        mediaPlayer.seekTo(1);

        if (isCheckboxClicked()) {
            mediaPlayer.start();
        }
    }

    @Override
    public int getTitle() {
        return R.string.universal_what_is_universal_life_plan;
    }

    @Override
    public int getSlider() {
        return R.drawable.about_us_slide8_8;
    }

    public void resetCheckbox() {
        for (ImageView view : checkboxs) {
            view.setSelected(false);
        }
    }

    public void selectCheckbox(View v) {
        resetCheckbox();
        v.setSelected(true);
    }

    @OnClick(R.id.detail_sub_checkbox1)
    public void onCheckbox1Clicked(View v) {
        selectCheckbox(v);
        prepareVideo(getVideo());
    }

    @OnClick(R.id.detail_sub_checkbox2)
    public void onCheckbox2Clicked(View v) {
        selectCheckbox(v);
        prepareVideo("flexible_arrangment_01_demoB_retina.mp4");
    }

    @OnClick(R.id.detail_sub_checkbox3)
    public void onCheckbox3Clicked(View v) {
        selectCheckbox(v);
        prepareVideo("flexible_arrangment_01_demoC_retina.mp4");
    }

    public boolean isCheckboxClicked() {
        for (ImageView view : checkboxs) {
            if (view.isSelected()) {
                return true;
            }
        }
        return false;
    }
}
