package com.transamerican.tlb.universal.page;

import com.transamerican.tlb.R;
import com.transamerican.tlb.base.PagerFragment;

import butterknife.OnClick;

/**
 * Created by Rice on 19/1/15.
 */
public abstract class UniversalPagerFragment extends PagerFragment {

    @OnClick(R.id.universal_life_introduction)
    public void onSectionIntroClicked() {
        if (listener != null) {
            listener.goToPage(1);
        }
    }

    @OnClick(R.id.universal_life_benefits)
    public void onSectionBenefitsClicked() {
        if (listener != null) {
            listener.goToPage(2);
        }
    }

    @OnClick(R.id.universal_life_mechanism)
    public void onSectionMechanismClicked() {
        if (listener != null) {
            listener.goToPage(4);
        }
    }
}
