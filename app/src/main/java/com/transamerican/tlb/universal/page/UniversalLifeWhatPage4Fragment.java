package com.transamerican.tlb.universal.page;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.transamerican.tlb.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectViews;
import butterknife.OnClick;

/**
 * Created by Rice on 18/1/15.
 */
public class UniversalLifeWhatPage4Fragment extends UniversalPagerFragment {

    @InjectViews({R.id.detail_sub_checkbox1, R.id.detail_sub_checkbox2, R.id.detail_sub_checkbox3})
    List<ImageView> checkboxs;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_universal_what_page4, container, false);
        ButterKnife.inject(this, v);

        return v;
    }

    @Override
    public void onPageSelected() {
        unselectCheckbox();
    }

    @Override
    public void onPageExited() {

    }

    @Override
    public int getTitle() {
        return R.string.universal_what_is_universal_life_plan;
    }

    @Override
    public int getSlider() {
        return R.drawable.about_us_slide8_4;
    }

    @Override
    public boolean isVideoExist() {
        return false;
    }

    @Override
    public void onReplayClicked() {

    }

    public void unselectCheckbox() {
        for (ImageView view : checkboxs) {
            view.setSelected(false);
        }
    }

    public void selectCheckbox(View v) {
        unselectCheckbox();
        v.setSelected(true);
    }

    @OnClick(R.id.detail_sub_checkbox1)
    public void onCheckbox1Clicked(View v) {
        selectCheckbox(v);
        jumpNext();
    }

    @OnClick(R.id.detail_sub_checkbox2)
    public void onCheckbox2Clicked(View v) {
        selectCheckbox(v);
        jumpNext();
    }

    @OnClick(R.id.detail_sub_checkbox3)
    public void onCheckbox3Clicked(View v) {
        selectCheckbox(v);
        jumpNext();
    }

    public void jumpNext() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (listener != null) {
                    listener.goToPage(5);
                }
            }
        }, 500);

    }
}
