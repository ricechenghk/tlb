package com.transamerican.tlb;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class MySubMenuView extends AbstractAnimatedView implements OnTouchListener {

	public static final int SECTION_ABOUT_US = 0;
	public static final int SECTION_UNIVERSAL_LIFE = 1;
	public static final int SECTION_TERM_LIFE = 2;
	public static final int SECTION_LIBRARY = 3;

	private static final String TAG = MySubMenuView.class.getSimpleName();
	//TODO: set this to false to remove colored debug rectangles
	private static final boolean DEBUG = false;
	private static final int DEBUG_ALPHA = 100;

	private static final int  DESIGN_HEIGHT = 150, DESIGN_WIDTH=1920;
	private float screenScale,translateScale;
	private Matrix scaleDown;

	private Vector<Sprite> ss;
	private Vector<Sprite> zSortedSprites;

	private Paint pa, pb, pc, pd, grey;

	private OnSectionClickListener listener;

	private Rect ia, ib, ic;

	private int width, height;

    private int TARGET_X[];

	public OnSectionClickListener getListener() {
		return listener;
	}

	public void setListener(OnSectionClickListener listener) {
		this.listener = listener;
	}

	public MySubMenuView(Context con, Point size, DisplayMetrics display, float bottomMenuPx) {
        super(con);

    }

    private boolean layouted;

    public void onLayout(boolean changed, int a,int b, int c, int d){
        super.onLayout(changed,a,b,c,d);
		this.setWillNotDraw(false);

        if(layouted){
            return;
        }

        layouted=true;
//		width = size.x;
//		height = (int)(size.y);


        width=getMeasuredWidth();
        height=getMeasuredHeight();


		scaleDown = new Matrix();

		Bitmap ba, bb, bc, bd;
		Bitmap aa, ab, ac, ad;
		Matrix ma, mb, mc, md;
		Rect ra, rb, rc, rd;

		ss = new Vector<Sprite>();
		zSortedSprites = new Vector<Sprite>();
		Options opts = new Options();
		opts.inScaled=true;
		opts.inTargetDensity=160;
//
//
//
//		aa = BitmapFactory.decodeResource(getResources(),
//				R.drawable.bu_strengths_off, opts);
//		ab = BitmapFactory.decodeResource(getResources(),
//				R.drawable.bu_contact_off, opts);
//
//		ac = BitmapFactory.decodeResource(getResources(),
//				R.drawable.bu_strengths_off, opts);
//		ad = BitmapFactory.decodeResource(getResources(), R.drawable.bu_setting_off,
//				opts);
//
//		ba = BitmapFactory.decodeResource(getResources(),
//				R.drawable.bu_strengths_on, opts);
//		bb = BitmapFactory.decodeResource(getResources(),
//				R.drawable.bu_contact_on, opts);
//
//		bc = BitmapFactory.decodeResource(getResources(),
//				R.drawable.bu_strengths_on, opts);
//		bd = BitmapFactory.decodeResource(getResources(),
//				R.drawable.bu_setting_on, opts);

//		leftBg = BitmapFactory.decodeResource(getResources(), R.raw.left, opts);

//        float sh=height*1.0f/aa.getHeight();
//        screenScale=sh;
        translateScale=width*1.0f/DESIGN_WIDTH;
//        setMinimumHeight(DESIGN_HEIGHT);
//        Log.i(TAG,"scale used="+width+" "+height+" ,, "+sh+" "+translateScale);
        TARGET_X=new int[4];
//        TARGET_X[0]=(int)(330/sh);
//        TARGET_X[1]=(int)(TARGET_X[0]+aa.getWidth()*0.92f);
//        TARGET_X[2]=(int)(TARGET_X[1]+ab.getWidth()*0.92f);
//        TARGET_X[3]=(int)(TARGET_X[2]+ac.getWidth()*0.92f);

        pa = createPaint(Color.RED, DEBUG_ALPHA);
		pb = createPaint(Color.YELLOW, DEBUG_ALPHA);
		pc = createPaint(Color.GREEN, DEBUG_ALPHA);
		pd = createPaint(Color.BLUE, DEBUG_ALPHA);
		grey = createPaint(Color.GRAY, 255);

		ma = new Matrix();
		mb = new Matrix();
		mc = new Matrix();
		md = new Matrix();

		int rectHeight =height;// (int)(aa.getHeight()); //-bottomMenuPx*2);	//Image Height - Bottom Menu - Sub Menu
		
		float aScale=1;
//		ra = new Rect(0, 0, (int) (aa.getWidth() * aScale),
//				(int) (rectHeight));
//		rb = new Rect(0, 0, (int) (ab.getWidth() * aScale),
//				(int) (rectHeight));
//		rc = new Rect(0, 0, (int) (ac.getWidth() * aScale),
//				(int) (rectHeight));
//		rd = new Rect(0, 0, (int) (ad.getWidth() * aScale),
//				(int) (rectHeight));

		ia = new Rect();
		ib = new Rect();
		ic = new Rect();

//		ss.add(new Sprite(ba, aa, ma, ra, 400, pa));
//		ss.add(new Sprite(bb, ab, mb, rb, 300, pb));
//		ss.add(new Sprite(bc, ac, mc, rc, 200, pc));
//		ss.add(new Sprite(bd, ad, md, rd, 100, pd));

		zSortedSprites.addAll(ss);
		Collections.sort(zSortedSprites, new Comparator<Sprite>() {

			@Override
			public int compare(Sprite lhs, Sprite rhs) {
				return lhs.z - rhs.z;
			}

		});

		init();
		this.setOnTouchListener(this);
	}

	private Paint createPaint(int color, int alpha) {
		Paint p = new Paint();
		p.setColor(color);
		p.setAlpha(alpha);
		return p;
	}

	private void init() {
		for (int i = 0; i < ss.size(); i++) {
			setPosition(i, -1);
		}
	}

	public int getItems() {
		return ss.size();
	}

	public void setPosition(int index, float x) {
        if(ss==null){
            return;
        }
		Sprite s = ss.get(index);
		int t = (int) (TARGET_X[index] * x * translateScale);
		
		Log.i("", "Set Position: "+index+" , x: "+x +" , t: "+t);
		
		s.matrix.reset();
//		s.matrix.preScale(screenScale, screenScale);
//		s.matrix.preTranslate(TARGET_X[index] * x, 0);
		s.matrix.preTranslate(t, 0);
//		s.touchRect.offsetTo((int)(TARGET_X[index] * x), 0);
		s.touchRect.offsetTo(t, 0);
		invalidate();
	}

	public void rectifyRect() {

		ia.setIntersect(ss.get(0).touchRect, ss.get(1).touchRect);
		ib.setIntersect(ss.get(1).touchRect, ss.get(2).touchRect);
		ic.setIntersect(ss.get(2).touchRect, ss.get(3).touchRect);

		Log.i(TAG, "rectify rect called " + ia + " " + ib + " " + ic);

		if (ic.width() > 0) {
			int first = (ia.left + ia.right) / 2, second = (ib.left + ib.right) / 2, third = (ic.left + ic.right) / 2;
			ss.get(0).touchRect.right = first;
			ss.get(1).touchRect.left = first;
			ss.get(1).touchRect.right = second;
			ss.get(2).touchRect.left = second;
			ss.get(2).touchRect.right = third;
			ss.get(3).touchRect.left = third;
		}
	}

	protected void onDraw(Canvas canvas) {
//		canvas.scale(0.5f/translateScale, 0.5f/translateScale);
		canvas.scale(screenScale,screenScale);
//		canvas.drawRect(0, 0, (int)(DESIGN_WIDTH/screenScale), (int)(DESIGN_HEIGHT/screenScale), grey);
		
		for (Sprite s : zSortedSprites) {
			canvas.drawBitmap(s.drawBitmap, s.matrix, null);
			if (DEBUG) {
				canvas.drawRect(s.touchRect, s.debugPaint);
			}
		}
		
//		canvas.drawBitmap(leftBg, 0, 0, null);

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int action = event.getAction(), x = (int) (event.getX() ), y = (int) (event
				.getY() );
		
		x/=screenScale;
		y/=screenScale;
//		x*=translateScale/0.5f;
//		y*=translateScale/0.5f;
//		Log.i(TAG,"ddd "+x+" "+y);
		Sprite hit = null;

		for (Sprite r : ss) {
			r.drawBitmap = r.normal;
			if (hit == null && r.touchRect.contains(x, y)) {
				hit = r;
			}
		}

		if (hit != null) {

			if (action == MotionEvent.ACTION_UP) {
				if (listener != null) {
					listener.onClick(ss.indexOf(hit));
				}
			} else {
				hit.drawBitmap = hit.hover;
			}
		}

		invalidate();

		return true;
	}

	public interface OnSectionClickListener {
		public void onClick(int i);
	}

	private class Sprite {
		Bitmap normal, hover;
		Matrix matrix;
		Rect touchRect;
		Bitmap drawBitmap;
		int z;
		Paint debugPaint;

		public Sprite(Bitmap normal, Bitmap hover, Matrix mat, Rect touchRect,
				int z, Paint d) {
			this.normal = normal;
			this.hover = hover;
			this.matrix = mat;
			this.touchRect = touchRect;
			drawBitmap = normal;
			this.z = z;
			this.debugPaint = d;
		}
	}

}
