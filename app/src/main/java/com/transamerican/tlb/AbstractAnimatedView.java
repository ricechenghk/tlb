package com.transamerican.tlb;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceView;

/**
 * Created by patrickchan on 9/1/15.
 */
public abstract class AbstractAnimatedView extends SurfaceView {
    abstract int getItems();
    abstract void rectifyRect();

    public AbstractAnimatedView(Context c){
        super(c);
    }

    public AbstractAnimatedView(Context c,AttributeSet a){
        super(c,a);
    }
}
