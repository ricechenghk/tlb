package com.transamerican.tlb.phone.about;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transamerican.tlb.R;
import com.transamerican.tlb.TitleView;
import com.transamerican.tlb.base.PagerFragment;
import com.transamerican.tlb.phone.about.page.AboutUsContactPFragment;
import com.transamerican.tlb.phone.about.page.AboutUsStrengthPage1PFragment;
import com.transamerican.tlb.phone.about.page.AboutUsStrengthPage2PFragment;
import com.transamerican.tlb.phone.about.page.AboutUsStrengthPage3PFragment;
import com.transamerican.tlb.phone.about.page.AboutUsStrengthPage4PFragment;
import com.transamerican.tlb.phone.about.page.AboutUsStrengthPage5PFragment;
import com.transamerican.tlb.phone.about.page.AboutUsStrengthPage6PFragment;
import com.transamerican.tlb.phone.about.page.AboutUsStrengthPage7PFragment;
import com.transamerican.tlb.phone.about.page.AboutUsWhoPage1PFragment;
import com.transamerican.tlb.phone.about.page.AboutUsWhoPage2PFragment;
import com.transamerican.tlb.phone.about.page.AboutUsWhoPage3PFragment;
import com.transamerican.tlb.phone.base.SectionBasePFragment;
import com.transamerican.tlb.sync.AppDataSource;

public class AboutUsPFragment extends SectionBasePFragment {

    public static final String TAG = AboutUsPFragment.class.getName();

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        //Query PDF Data from Server
        AppDataSource.getInstance().getOurStrengthFromServer(getActivity(), this, null);

        if (getArguments() != null) {
            int sectionId = getArguments().getInt("section");
            goToSection(sectionId);
        }
        return view;
    }

    @Override
    public PagerFragment getPagerFragment(int pos) {

        PagerFragment fr = null;
        switch (pos) {

            case 0:
                fr = new AboutUsWhoPage1PFragment();
                break;
            case 1:
                fr = new AboutUsWhoPage2PFragment();
                break;
            case 2:
                fr = new AboutUsWhoPage3PFragment();
                break;
            case 3:
                fr = new AboutUsStrengthPage1PFragment();
                break;
            case 4:
                fr = new AboutUsStrengthPage2PFragment();
                break;
            case 5:
                fr = new AboutUsStrengthPage3PFragment();
                break;
            case 6:
                fr = new AboutUsStrengthPage4PFragment();
                break;
            case 7:
                fr = new AboutUsStrengthPage5PFragment();
                break;
            case 8:
                fr = new AboutUsStrengthPage6PFragment();
                break;
            case 9:
                fr = new AboutUsStrengthPage7PFragment();
                break;
            case 10:
                fr = new AboutUsContactPFragment();
                break;
            default:
                fr = new AboutUsWhoPage1PFragment();
                break;
        }

        return fr;
    }

    @Override
    public int getPagerCount() {
        return 11;
    }

    @Override
    public int getTopBackgroundResId() {
        return R.drawable.iphone_about_top_bg;
    }

    @Override
    public int getTopSectionTextResId() {
        return R.drawable.iphone_about_top_text;
    }

    @Override
    public int getMenuTitle()
    {
        return R.string.phone_pop_menu_about;
    }

    @Override
    public String[] getMenuItems() {
        String[] item = new String[3];
        item[0] = getResources().getString(R.string.about_us_title_who_we_are);
        item[1] = getResources().getString(R.string.about_us_title_our_strength);
        item[2] = getResources().getString(R.string.about_us_title_contact_us);
        return item;

    }

    @Override
    public void onMenuItemClicked(int pos) {
        int sectionId = TitleView.INDEX_ABOUT_WHO;
        switch (pos) {
            case 0:
                sectionId = TitleView.INDEX_ABOUT_WHO;
                break;
            case 1:
                sectionId = TitleView.INDEX_ABOUT_STRENGTH;
                break;
            case 2:
                sectionId = TitleView.INDEX_ABOUT_CONTACT;
                break;
        }
        goToSection(sectionId);
    }

    public void goToSection(int sectionId) {
        switch (sectionId) {
            case TitleView.INDEX_ABOUT_WHO:
                goToPage(1);
                break;
            case TitleView.INDEX_ABOUT_STRENGTH:
                goToPage(4);
                break;
            case TitleView.INDEX_ABOUT_CONTACT:
                goToPage(11);
                break;
        }
    }
}
