package com.transamerican.tlb.phone.library;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.transamerican.tlb.MainApplication;
import com.transamerican.tlb.MainBaseActivity;
import com.transamerican.tlb.R;
import com.transamerican.tlb.listener.MainSectionChangeListener;
import com.transamerican.tlb.model.LibraryCategory;
import com.transamerican.tlb.sync.AppDataSource;
import com.transamerican.tlb.util.LibraryExternalStorageHelper;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectViews;
import butterknife.OnClick;
import hk.com.playmore.syncframework.util.RunnableArgument;
import hk.com.playmore.syncframework.util.SyncFragment;

public class LibraryPFragment extends SyncFragment {

    MainSectionChangeListener mainSectionChangeListener;

    @InjectViews({R.id.btn_product_collaterals, R.id.btn_field_communications, R.id.btn_forms, R.id.btn_underwriting_guide, R.id.btn_transware})
    List<LinearLayout> sections;

    private int currentSection = LibraryCategory.PRODUCT_COLLATERALS; //Default start is Product Collaterals

    private ProgressDialog mLoadingDialog;
    private LibraryExternalStorageHelper storageHelper = new LibraryExternalStorageHelper();

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_p_library, container, false);
        ButterKnife.inject(this, view);

        return view;
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void resetSectionsButtonState() {
        for (LinearLayout view : sections) {
            view.setSelected(false);
        }
    }

    @OnClick(R.id.btn_product_collaterals)
    public void onProductCollateralsClicked(View view) {
        resetSectionsButtonState();
        view.setSelected(true);

        ((MainApplication) getActivity().getApplication()).sendTrackerEvent(R.string.ga_prod_col);

        ((MainBaseActivity)getActivity()).showLoading();
        //Get Category from Server
        AppDataSource.getInstance().getProdColCategoryFromServer(getActivity(), this, new RunnableArgument() {

            @Override
            public void run(String object) {

                //Get Library From Server
                AppDataSource.getInstance().getProdColLibraryFromServer(getActivity(), LibraryPFragment.this, new RunnableArgument() {

                    @Override
                    public void run(String object) {
                        ((MainBaseActivity)getActivity()).hideLoading();
                        currentSection = LibraryCategory.PRODUCT_COLLATERALS;
                        goToCategory();
                    }
                });
            }
        });
    }

    @OnClick(R.id.btn_field_communications)
    public void onFieldCommunicationsClicked(View view) {
        resetSectionsButtonState();
        view.setSelected(true);

        ((MainApplication) getActivity().getApplication()).sendTrackerEvent(R.string.ga_field_comm);

        ((MainBaseActivity)getActivity()).showLoading();
        //Get Category from Server
        AppDataSource.getInstance().getFieldCommCategoryFromServer(getActivity(), this, new RunnableArgument() {

            @Override
            public void run(String object) {

                //Get Library From Server
                AppDataSource.getInstance().getFieldCommLibraryFromServer(getActivity(), LibraryPFragment.this, new RunnableArgument() {

                    @Override
                    public void run(String object) {
                        ((MainBaseActivity)getActivity()).hideLoading();
                        currentSection = LibraryCategory.FIELD_COMMUNICATIONS;
                        goToCategory();
                    }
                });
            }
        });
    }

    @OnClick(R.id.btn_forms)
    public void onFormsClicked(View view) {
        resetSectionsButtonState();
        view.setSelected(true);

        ((MainApplication) getActivity().getApplication()).sendTrackerEvent(R.string.ga_app_forms);

        ((MainBaseActivity)getActivity()).showLoading();
        //Get Category from Server
        AppDataSource.getInstance().getFormsCategoryFromServer(getActivity(), this, new RunnableArgument() {

            @Override
            public void run(String object) {

                //Get Library From Server
                AppDataSource.getInstance().getFormsLibraryFromServer(getActivity(), LibraryPFragment.this, new RunnableArgument() {

                    @Override
                    public void run(String object) {
                        ((MainBaseActivity)getActivity()).hideLoading();
                        currentSection = LibraryCategory.FORMS;
                        goToCategory();
                    }
                });
            }
        });
    }

    @OnClick(R.id.btn_underwriting_guide)
    public void onUnderwritingGuideClicked(View view) {
        resetSectionsButtonState();
        view.setSelected(true);

        ((MainApplication) getActivity().getApplication()).sendTrackerEvent(R.string.ga_under_guide);

        ((MainBaseActivity)getActivity()).showLoading();
        //Get Category from Server
        AppDataSource.getInstance().getUnderwritingGuideCategoryFromServer(getActivity(), this, new RunnableArgument() {

            @Override
            public void run(String object) {

                //Get Library From Server
                AppDataSource.getInstance().getUnderwritingGuideLibraryFromServer(getActivity(), LibraryPFragment.this, new RunnableArgument() {

                    @Override
                    public void run(String object) {
                        ((MainBaseActivity)getActivity()).hideLoading();
                        currentSection = LibraryCategory.UNDERWRITING_GUIDE;
                        goToCategory();
                    }
                });
            }
        });
    }

    @OnClick(R.id.btn_transware)
    public void onTranswareClicked(View view) {
        resetSectionsButtonState();
        view.setSelected(true);

        ((MainApplication) getActivity().getApplication()).sendTrackerEvent(R.string.ga_transware);

        ((MainBaseActivity)getActivity()).showLoading();
        //Get Category from Server
        AppDataSource.getInstance().getTranswareCategoryFromServer(getActivity(), this, new RunnableArgument() {

            @Override
            public void run(String object) {

                //Get Library From Server
                AppDataSource.getInstance().getTranswareLibraryFromServer(getActivity(), LibraryPFragment.this, new RunnableArgument() {

                    @Override
                    public void run(String object) {
                        ((MainBaseActivity)getActivity()).hideLoading();
                        currentSection = LibraryCategory.TRANSWARE;
                        goToCategory();
                    }
                });
            }
        });
    }

    public void goToCategory() {
        Bundle bundle = new Bundle();
        bundle.putInt("section", currentSection);
        LibraryCategoryPFragment fr = new LibraryCategoryPFragment();
        fr.setMainSectionChangeListener(mainSectionChangeListener);
        fr.setArguments(bundle);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fr);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    public void setMainSectionChangeListener(MainSectionChangeListener mainSectionChangeListener) {
        this.mainSectionChangeListener = mainSectionChangeListener;
    }
}
