package com.transamerican.tlb.phone.setting;

import android.os.Bundle;
import android.widget.ListView;

import com.transamerican.tlb.ContactListAdapter;
import com.transamerican.tlb.R;
import com.transamerican.tlb.model.Contact;
import com.transamerican.tlb.sync.AppDataSource;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import hk.com.playmore.syncframework.util.SyncFragmentActivity;


public class ContactDetailPActivity extends SyncFragmentActivity {
    private static final String TAG = ContactDetailPActivity.class.getSimpleName();

    @InjectView(R.id.contact_list)
    public ListView contactList;
    public ContactListAdapter contactListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p_contact_detail);
        ButterKnife.inject(this);

        contactListAdapter = new ContactListAdapter(this);
        contactList.setAdapter(contactListAdapter);

        String categoryId = getIntent().getStringExtra("categoryId");

        List<Contact> contactList = AppDataSource.getInstance().getContact(this, categoryId);
        contactListAdapter.replaceWith(contactList);

    }

    protected void onResume() {
        super.onResume();
    }


    @OnClick(R.id.btn_back)
    public void onBackClicked() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.btn_close)
    public void onCloseClicked() {
        setResult(RESULT_OK);
        finish();
    }
}
