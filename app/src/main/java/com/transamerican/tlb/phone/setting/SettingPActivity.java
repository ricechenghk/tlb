package com.transamerican.tlb.phone.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.transamerican.tlb.R;
import com.transamerican.tlb.phone.TutorialPActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class SettingPActivity extends FragmentActivity {
    private static final String TAG = SettingPActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p_setting);
        ButterKnife.inject(this);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    protected void onResume() {
        super.onResume();
    }

    @OnClick(R.id.setting_terms)
    public void onTermsClicked() {
        Intent intent = new Intent(this, SettingTermPActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.setting_privacy)
    public void onPrivacyClicked() {
        Intent intent = new Intent(this, SettingPrivacyPActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.setting_help)
    public void onHelpClicked() {
        Intent intent = new Intent(this, TutorialPActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_close)
    public void onCloseClicked() {
        finish();
    }
}
