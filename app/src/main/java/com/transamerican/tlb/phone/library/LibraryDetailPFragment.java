package com.transamerican.tlb.phone.library;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;
import com.transamerican.tlb.R;
import com.transamerican.tlb.library.LibraryPDFActivity;
import com.transamerican.tlb.library.adapter.LibraryGridAdapter;
import com.transamerican.tlb.library.listener.LibraryActionListener;
import com.transamerican.tlb.listener.MainSectionChangeListener;
import com.transamerican.tlb.model.Library;
import com.transamerican.tlb.model.LibraryCategory;
import com.transamerican.tlb.model.ProdColCategory;
import com.transamerican.tlb.sync.AppDataSource;
import com.transamerican.tlb.sync.LibraryDownloadTask;
import com.transamerican.tlb.util.LibraryExternalStorageHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import hk.com.playmore.syncframework.util.SyncFragment;

public class LibraryDetailPFragment extends SyncFragment implements LibraryActionListener, AdapterView.OnItemClickListener {

    public static final String BROADCAST_LIBRARY_DOWNLOAD = "BROADCAST_LIBRARY_DOWNLOAD";
    private MainSectionChangeListener mainSectionChangeListener;
    @InjectView(R.id.grid_library)
    public StickyGridHeadersGridView libraryGrid;
    public LibraryGridAdapter mLibraryGridAdapter;

    @InjectView(R.id.section_bar)
    TextView sectionBar;

    private int currentSection = LibraryCategory.PRODUCT_COLLATERALS; //Default start is Product Collaterals
    private LibraryCategory category;

    private ProgressDialog mLoadingDialog;
    private LibraryExternalStorageHelper storageHelper = new LibraryExternalStorageHelper();

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_p_library_detail, container, false);
        ButterKnife.inject(this, view);

        mLibraryGridAdapter = new LibraryGridAdapter(getActivity(), currentSection, new ArrayList<LibraryCategory>(), new ArrayList<Library>(), this);
        libraryGrid.setAdapter(mLibraryGridAdapter);
        libraryGrid.setOnItemClickListener(this);

        if (getArguments() != null) {

            String title = getArguments().getString("title").trim();
            sectionBar.setText(title);
            sectionBar.invalidate();

            currentSection = getArguments().getInt("section");
            String id = getArguments().getString("id");
            category = AppDataSource.getInstance().getRootLibraryCategoryByCategoryId(getActivity(), currentSection, id);
            constructLibraryTreeByRootId(category.getId());

        }

        return view;
    }

    public void onResume() {
        super.onResume();

        if (mainSectionChangeListener != null) {
            mainSectionChangeListener.onSectionChange(false);
        }

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                mMessageReceiver, new IntentFilter(BROADCAST_LIBRARY_DOWNLOAD));
    }

    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(
                mMessageReceiver);
    }

    public void constructLibraryTreeByRootId(String id) {
        int count = 0;
        List<ProdColCategory> prodColCatListWithLib = new ArrayList<>();
        List<LibraryCategory> catListWithLib = new ArrayList<>();
        List<Library> allLibList = new ArrayList<>();
        List<? extends LibraryCategory> list = AppDataSource.getInstance().getLibraryCategoryBySection(getActivity(), currentSection, id);
        for (LibraryCategory item : list) {
            List<String> subCatIds = getAllSubCategoryID(item);
            List<? extends Library> libList = AppDataSource.getInstance().getAllLibraryInSubCategoryBySection(getActivity(), currentSection, subCatIds);
            Log.i("", item.getTitle() + ": " + subCatIds.toString() + " , Count: " + libList.size());
            item.setCount(libList.size());
            count += libList.size();

            if (libList.size() > 0) {
                allLibList.addAll(libList);
                catListWithLib.add(item);
            }

            if (currentSection == LibraryCategory.PRODUCT_COLLATERALS) {
                ProdColCategory prodColCategory = (ProdColCategory) item;
                if (prodColCategory.getShortForm() != null && !prodColCategory.getShortForm().isEmpty()) {
                    prodColCatListWithLib.add((ProdColCategory) item);
                }
            }
        }

        //If no sub category, get root library
        if(list==null || list.size()==0)
        {
            List<String> subCatIds = new ArrayList<>();
            subCatIds.add(id);
            List<? extends Library> libList = AppDataSource.getInstance().getAllLibraryInSubCategoryBySection(getActivity(), currentSection, subCatIds);
            count +=libList.size();

            if(libList.size()>0)
            {
                allLibList.addAll(libList);

                category.setCount(count);
                catListWithLib.add(category);
            }
        }

        mLibraryGridAdapter.replaceWith(currentSection, catListWithLib, allLibList);

    }

    public List<String> getAllSubCategoryID(LibraryCategory item) {
        List<String> subCatIds = new ArrayList<String>();
        subCatIds.add(item.getId());
        getSubCategoryID(item, subCatIds);
        return subCatIds;
    }

    public void getSubCategoryID(LibraryCategory root, List<String> subCatIds) {
        List<? extends LibraryCategory> list = AppDataSource.getInstance().getLibraryCategoryBySection(getActivity(), currentSection, root.getId());
        for (LibraryCategory item : list) {
            subCatIds.add(item.getId());
            getSubCategoryID(item, subCatIds);
        }
    }

    @Override
    public void onLibraryDownloadClicked(Library library) {

        mLoadingDialog = new ProgressDialog(getActivity());
        mLoadingDialog.setMessage(getString(R.string.loading));
        mLoadingDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mLoadingDialog.setIndeterminate(false);
        mLoadingDialog.setCancelable(false);
        mLoadingDialog.show();

        LibraryDownloadTask task = new LibraryDownloadTask(getActivity(), currentSection, library.getId(), BROADCAST_LIBRARY_DOWNLOAD);
        task.execute();
    }

    @Override
    public void onLibraryDeleteClicked(final Library library) {

        final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setMessage(R.string.library_delete_confirm_msg);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                storageHelper.deleteLibrary(getActivity(), currentSection, library.getId());
                mLibraryGridAdapter.notifyDataSetChanged();

            }
        });
        alert.setNegativeButton(R.string.cancel, null);
        alert.show();

    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            String state = intent.getStringExtra(LibraryDownloadTask.BROADCAST_STATE);
            if (state == LibraryDownloadTask.STATE_DOWNLOADING) {
                int progress = intent.getIntExtra(LibraryDownloadTask.BROADCAST_PROGRESS, 0);
                if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
                    mLoadingDialog.setProgress(progress);
                }
            } else if (state == LibraryDownloadTask.STATE_FINISH) {
                String error = intent.getStringExtra(LibraryDownloadTask.BROADCAST_ERROR);
                if (error == null || error.trim().isEmpty()) {
                    Log.i("", "File is downloaded Success");
                } else {
                    Log.i("", "File is downloaded Fail");
                }

                if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
                    mLoadingDialog.dismiss();
                }
                mLibraryGridAdapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Library library = mLibraryGridAdapter.getItem(position);
        boolean downloaded = mLibraryGridAdapter.isLibraryDownloaded(currentSection, library);
        if (downloaded) {
            Intent intent = new Intent(getActivity(), LibraryPDFActivity.class);
            intent.putExtra(LibraryPDFActivity.EXTRA_LIBRARY_SECTION, currentSection);
            intent.putExtra(LibraryPDFActivity.EXTRA_LIBRARY_ID, library.getId());
            getActivity().startActivity(intent);
        }
    }

    public void setMainSectionChangeListener(MainSectionChangeListener mainSectionChangeListener) {
        this.mainSectionChangeListener = mainSectionChangeListener;
    }
}
