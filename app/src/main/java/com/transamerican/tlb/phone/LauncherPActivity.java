package com.transamerican.tlb.phone;

import com.transamerican.tlb.LauncherBaseActivity;
import com.transamerican.tlb.R;


public class LauncherPActivity extends LauncherBaseActivity {


    @Override
    protected int getLayout() {
        return R.layout.activity_p_launch;
    }

    @Override
    protected Class getMainActivity() {
        return MainPActivity.class;
    }
}
