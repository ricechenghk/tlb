package com.transamerican.tlb.phone.universal;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transamerican.tlb.R;
import com.transamerican.tlb.TitleView;
import com.transamerican.tlb.base.PagerFragment;
import com.transamerican.tlb.phone.base.SectionBasePFragment;
import com.transamerican.tlb.phone.universal.page.UniversalLifePlanPage1PFragment;
import com.transamerican.tlb.phone.universal.page.UniversalLifePlanPage2PFragment;
import com.transamerican.tlb.phone.universal.page.UniversalLifePlanPage3PFragment;
import com.transamerican.tlb.phone.universal.page.UniversalLifePlanPage4PFragment;
import com.transamerican.tlb.phone.universal.page.UniversalLifePlanPage5PFragment;
import com.transamerican.tlb.phone.universal.page.UniversalLifeWhatPage1PFragment;
import com.transamerican.tlb.phone.universal.page.UniversalLifeWhatPage2PFragment;
import com.transamerican.tlb.phone.universal.page.UniversalLifeWhatPage3PFragment;
import com.transamerican.tlb.phone.universal.page.UniversalLifeWhatPage4PFragment;
import com.transamerican.tlb.phone.universal.page.UniversalLifeWhatPage5PFragment;

public class UniversalLifePFragment extends SectionBasePFragment {

    public static final String TAG = UniversalLifePFragment.class.getName();

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        if (getArguments() != null) {
            int sectionId = getArguments().getInt("section");
            goToSection(sectionId);
        }
        return view;
    }

    @Override
    public PagerFragment getPagerFragment(int pos) {

        PagerFragment fr = null;
        switch (pos) {

            case 0:
                fr = new UniversalLifeWhatPage1PFragment();
                break;
            case 1:
                fr = new UniversalLifeWhatPage2PFragment();
                break;
            case 2:
                fr = new UniversalLifeWhatPage3PFragment();
                break;
            case 3:
                fr = new UniversalLifeWhatPage4PFragment();
                break;
            case 4:
                fr = new UniversalLifeWhatPage5PFragment();
                break;
            case 5:
                fr = new UniversalLifePlanPage1PFragment();
                break;
            case 6:
                fr = new UniversalLifePlanPage2PFragment();
                break;
            case 7:
                fr = new UniversalLifePlanPage3PFragment();
                break;
            case 8:
                fr = new UniversalLifePlanPage4PFragment();
                break;
            case 9:
                fr = new UniversalLifePlanPage5PFragment();
                break;
            default:
                fr = new UniversalLifeWhatPage1PFragment();
                break;
        }

        return fr;
    }

    @Override
    public int getPagerCount() {
        return 10;
    }

    @Override
    public int getTopBackgroundResId() {
        return R.drawable.iphone_universal_top_bg;
    }

    @Override
    public int getTopSectionTextResId() {
        return R.drawable.iphone_universal_top_text;
    }

    @Override
    public int getMenuTitle()
    {
        return R.string.phone_pop_menu_universal;
    }

    @Override
    public String[] getMenuItems() {
        String[] item = new String[2];
        item[0] = getResources().getString(R.string.universal_what_is_universal_life_plan);
        item[1] = getResources().getString(R.string.universal_our_universal_life_plan);
        return item;

    }

    @Override
    public void onMenuItemClicked(int pos) {
        int sectionId = TitleView.INDEX_UNIVERSAL_ONE;
        switch (pos) {
            case 0:
                sectionId = TitleView.INDEX_UNIVERSAL_ONE;
                break;
            case 1:
                sectionId = TitleView.INDEX_UNIVERSAL_TWO;
                break;
        }
        goToSection(sectionId);
    }

    public void goToSection(int sectionId) {
        switch (sectionId) {
            case TitleView.INDEX_UNIVERSAL_ONE:
                goToPage(1);
                break;
            case TitleView.INDEX_UNIVERSAL_TWO:
                goToPage(6);
                break;
        }
    }
}
