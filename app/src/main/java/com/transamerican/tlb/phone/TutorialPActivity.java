package com.transamerican.tlb.phone;

import com.transamerican.tlb.R;
import com.transamerican.tlb.TutorialBaseActivity;

public class TutorialPActivity extends TutorialBaseActivity {
    private static final String TAG = TutorialPActivity.class.getSimpleName();

    @Override
    public int getLayoutResId() {
        return R.layout.activity_p_tutorial;
    }

    @Override
    public int[] getTutorialResId() {
        return new int[]{
                R.drawable.tutorial_p_01,
                R.drawable.tutorial_p_02,
                R.drawable.tutorial_p_03,
                R.drawable.tutorial_p_04,
                R.drawable.tutorial_p_05,
                R.drawable.tutorial_p_06,
                R.drawable.tutorial_p_07,
                R.drawable.tutorial_p_08
        };
    }
}
