package com.transamerican.tlb.phone.base;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.transamerican.tlb.R;
import com.transamerican.tlb.base.PagerFragment;
import com.transamerican.tlb.listener.PageChangeListener;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import hk.com.playmore.syncframework.util.SyncFragment;

/**
 * Created by Rice on 23/1/15.
 */
public abstract class SectionBasePFragment extends SyncFragment implements ViewPager.OnPageChangeListener, PageChangeListener {

    public static final String TAG = SectionBasePFragment.class.getName();

    @InjectView(R.id.section_top_bg)
    RelativeLayout background;

    @InjectView(R.id.section_top_text)
    ImageView sectionText;

    @InjectView(R.id.section_top_subtitle)
    TextView sectionSubtitle;

    @InjectView(R.id.slider)
    ImageView slider;

    @InjectView(R.id.viewPager)
    ViewPager viewPager;
    MyPagerAdapter adapter;

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_p_section, container, false);
        ButterKnife.inject(this, view);

        background.setBackgroundResource(getTopBackgroundResId());
        sectionText.setImageResource(getTopSectionTextResId());

        adapter = new MyPagerAdapter(this.getChildFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(this);

        PagerFragment fr = getPagerFragment(0);
        resetInfo(fr.getTitle(), fr.getSlider());

        return view;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Log.i("", "Page: " + position + "is selected");
        Fragment page = getChildFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + viewPager.getCurrentItem());
        if (page != null) {
            PagerFragment fr = ((PagerFragment) page);
            fr.onPageSelected();
            resetInfo(fr.getTitle(), fr.getSlider());
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void goToPage(int page) {
        int pos = page - 1;
        if (pos != viewPager.getCurrentItem()) {
            viewPager.setCurrentItem(pos);
            PagerFragment fr = adapter.getItem(pos);
            resetInfo(fr.getTitle(), fr.getSlider());
        }
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public PagerFragment getItem(int pos) {

            PagerFragment fr = getPagerFragment(pos);
            fr.setListener(SectionBasePFragment.this);

            return fr;
        }

        @Override
        public int getCount() {
            return getPagerCount();
        }

    }

    public void resetInfo(int titleResId, int sliderResId) {
        sectionSubtitle.setText(titleResId);
        if (sliderResId == 0) {
            slider.setVisibility(View.INVISIBLE);
        } else {
            slider.setVisibility(View.VISIBLE);
            slider.setImageResource(sliderResId);
        }
    }

    public abstract PagerFragment getPagerFragment(int pos);

    public abstract int getPagerCount();

    public abstract int getTopBackgroundResId();

    public abstract int getTopSectionTextResId();

    public abstract int getMenuTitle();

    public abstract String[] getMenuItems();

    public abstract void onMenuItemClicked(int pos);

    @OnClick(R.id.menu)
    public void onMenuClicked() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getMenuTitle())
                .setItems(getMenuItems(), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        onMenuItemClicked(which);
                    }
                });
        builder.show();
    }

}