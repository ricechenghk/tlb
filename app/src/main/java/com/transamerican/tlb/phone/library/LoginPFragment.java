package com.transamerican.tlb.phone.library;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.transamerican.tlb.R;
import com.transamerican.tlb.library.LoginFragment;

/**
 * Created by Rice on 24/1/15.
 */
public class LoginPFragment extends LoginFragment {

    public int getResId() {
        return R.layout.fragment_p_login;
    }

    public void goToLibrary() {
        LibraryPFragment fr = new LibraryPFragment();
        fr.setMainSectionChangeListener(mainSectionChangeListener);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fr);
        fragmentTransaction.commit();
    }
}
