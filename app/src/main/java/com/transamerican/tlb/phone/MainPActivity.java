package com.transamerican.tlb.phone;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.ImageView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.transamerican.tlb.MainBaseActivity;
import com.transamerican.tlb.R;
import com.transamerican.tlb.TitleView;
import com.transamerican.tlb.library.LoginFragment;
import com.transamerican.tlb.phone.about.AboutUsPFragment;
import com.transamerican.tlb.phone.library.LoginPFragment;
import com.transamerican.tlb.phone.setting.ContactInfoPActivity;
import com.transamerican.tlb.phone.setting.SettingPActivity;
import com.transamerican.tlb.sync.AppDataSource;

import butterknife.InjectView;
import butterknife.OnClick;

public class MainPActivity extends MainBaseActivity {
    private static final String TAG = MainPActivity.class.getSimpleName();

    SlidingMenu menu;

    @InjectView(R.id.btn_top_menu_home)
    ImageView topMenuHome;

    @InjectView(R.id.btn_top_menu_back)
    ImageView topMenuBack;

    ImageView slideMenuHome;
    ImageView slideMenuSetting;
    ImageView slideMenuLogout;
    ImageView slideMenuLocation;
    ImageView slideMenuLink;
    ImageView slideMenuContact;


    @Override
    public int getLayoutResId() {
        return R.layout.activity_p_main;
    }

    @Override
    public Class getTutorialClass() {
        return TutorialPActivity.class;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.RIGHT);
//        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
//        menu.setTouchModeBehind(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        menu.setTouchModeBehind(SlidingMenu.TOUCHMODE_NONE);
        menu.setBehindWidthRes(R.dimen.phone_menu_width);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu);

        slideMenuHome = (ImageView) menu.findViewById(R.id.btn_home);
        slideMenuHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.toggle(false);
                onHomeClicked();
            }
        });

        slideMenuSetting = (ImageView) menu.findViewById(R.id.btn_setting);
        slideMenuSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.toggle(false);
                onSettingClicked();
            }
        });

        slideMenuLogout = (ImageView) menu.findViewById(R.id.btn_logout);
        slideMenuLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.toggle(false);
                onLogoutClicked();
            }
        });

        slideMenuLocation = (ImageView) menu.findViewById(R.id.btn_location);
        slideMenuLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.toggle(false);
                onLocationClicked();
            }
        });

        slideMenuLink = (ImageView) menu.findViewById(R.id.btn_link);
        slideMenuLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.toggle(false);
                onLinkClicked();
            }
        });

        slideMenuContact = (ImageView) menu.findViewById(R.id.btn_contact);
        slideMenuContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.toggle(false);
                onContactClicked();
            }
        });


        showMainMenu();
    }

    @Override
    public void toggleExtra() {
        boolean showExtra = AppDataSource.getInstance().isLogin(this);
        slideMenuLink.setVisibility(showExtra ? View.VISIBLE : View.INVISIBLE);
        slideMenuContact.setVisibility(showExtra ? View.VISIBLE : View.INVISIBLE);
        slideMenuLogout.setImageResource(showExtra ? R.drawable.iphone_menu_logout : R.drawable.iphone_menu_login_on); //icon drawable revised only
    }

    @Override
    public void showMainMenu() {
        MainMenuPFragment fr = new MainMenuPFragment();
        fr.setMainSectionChangeListener(this);
        replaceFragment(fr);

        setTopMenuHomeVisible(false);
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onSectionChange(boolean showExtra) {

        //Use "extra" flag to control show back or home in phone
        if (showExtra) {
            setTopMenuBackVisible(false);
            setTopMenuHomeVisible(true);
        } else {
            setTopMenuBackVisible(true);
            setTopMenuHomeVisible(false);
        }
        toggleExtra();
    }

    @OnClick(R.id.btn_slide_menu)
    public void onSlideMenuClicked() {
        menu.toggle();
    }

    public void setTopMenuHomeVisible(boolean show) {
        topMenuHome.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void setTopMenuBackVisible(boolean show) {
        topMenuBack.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.btn_top_menu_home)
    public void onTopMenuHomeClicked() {
        showMainMenu();
    }

    @OnClick(R.id.btn_top_menu_back)
    public void onTopMenuBackClicked() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack();

        if (fragmentManager.getBackStackEntryCount() == 1) {
            setTopMenuBackVisible(false);
            setTopMenuHomeVisible(true);
        }
    }

    public void onHomeClicked() {
        showMainMenu();
    }

    public void onSettingClicked() {

        Intent intent = new Intent(this, SettingPActivity.class);
        startActivity(intent);
    }

    public void onLogoutClicked() {

        boolean isLogin = AppDataSource.getInstance().isLogin(this);
        if (isLogin) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.logout)
                    .setMessage(R.string.logout_confirm_msg)
                    .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            logout();
                            onHomeClicked();
                        }
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .show();
        } else {

            Fragment lastFr = getSupportFragmentManager().findFragmentById(R.id.container);
            if(lastFr instanceof LoginPFragment)
            {
                return;
            }

            LoginPFragment fr = new LoginPFragment();
            fr.setMainSectionChangeListener(this);
            addFragment(fr);
        }

    }

    public void onLocationClicked() {
        onSectionChange(true);

        AboutUsPFragment fr = new AboutUsPFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("section", TitleView.INDEX_ABOUT_CONTACT);
        fr.setArguments(bundle);
        replaceFragment(fr);
    }

    public void onLinkClicked() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.links)
                .setItems(R.array.linkTextArray, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String[] linkArray = getResources().getStringArray(R.array.linkArray);
                        String link = linkArray[which];
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(link));
                        startActivity(i);
                    }
                });
        builder.show();
    }

    public void onContactClicked() {
        Intent intent = new Intent(this, ContactInfoPActivity.class);
        startActivity(intent);
    }
}
