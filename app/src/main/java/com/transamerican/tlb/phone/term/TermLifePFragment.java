package com.transamerican.tlb.phone.term;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transamerican.tlb.R;
import com.transamerican.tlb.TitleView;
import com.transamerican.tlb.base.PagerFragment;
import com.transamerican.tlb.phone.base.SectionBasePFragment;
import com.transamerican.tlb.phone.term.page.TermLifeRopPage1PFragment;
import com.transamerican.tlb.phone.term.page.TermLifeRopPage2PFragment;
import com.transamerican.tlb.phone.term.page.TermLifeSuperPage1PFragment;
import com.transamerican.tlb.phone.term.page.TermLifeSuperPage2PFragment;
import com.transamerican.tlb.phone.term.page.TermLifeWhatPFragment;

public class TermLifePFragment extends SectionBasePFragment {

    public static final String TAG = TermLifePFragment.class.getName();

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        if (getArguments() != null) {
            int sectionId = getArguments().getInt("section");
            goToSection(sectionId);
        }
        return view;
    }

    @Override
    public PagerFragment getPagerFragment(int pos) {

        PagerFragment fr = null;
        switch (pos) {

            case 0:
                fr = new TermLifeWhatPFragment();
                break;
            case 1:
                fr = new TermLifeSuperPage1PFragment();
                break;
            case 2:
                fr = new TermLifeSuperPage2PFragment();
                break;
            case 3:
                fr = new TermLifeRopPage1PFragment();
                break;
            case 4:
                fr = new TermLifeRopPage2PFragment();
                break;
            default:
                fr = new TermLifeWhatPFragment();
                break;
        }

        return fr;
    }

    @Override
    public int getPagerCount() {
        return 5;
    }

    @Override
    public int getTopBackgroundResId() {
        return R.drawable.iphone_term_top_bg;
    }

    @Override
    public int getTopSectionTextResId() {
        return R.drawable.iphone_term_top_text;
    }

    @Override
    public int getMenuTitle()
    {
        return R.string.phone_pop_menu_term;
    }

    @Override
    public String[] getMenuItems() {
        String[] item = new String[3];
        item[0] = getResources().getString(R.string.term_life_what_is_term_life);
        item[1] = getResources().getString(R.string.term_life_trendsetter_super_no_sm);
        item[2] = getResources().getString(R.string.term_life_trendsetter_rop_no_sm);
        return item;

    }

    @Override
    public void onMenuItemClicked(int pos) {
        int sectionId = TitleView.INDEX_TERM_WHAT;
        switch (pos) {
            case 0:
                sectionId = TitleView.INDEX_TERM_WHAT;
                break;
            case 1:
                sectionId = TitleView.INDEX_TERM_SUPER;
                break;
            case 2:
                sectionId = TitleView.INDEX_TERM_ROP;
                break;
        }
        goToSection(sectionId);
    }

    public void goToSection(int sectionId) {
        switch (sectionId) {
            case TitleView.INDEX_TERM_WHAT:
                goToPage(1);
                break;
            case TitleView.INDEX_TERM_SUPER:
                goToPage(2);
                break;
            case TitleView.INDEX_TERM_ROP:
                goToPage(4);
                break;
        }
    }
}
