package com.transamerican.tlb.phone.setting;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.transamerican.tlb.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class SettingTermPActivity extends Activity {
    private static final String TAG = SettingTermPActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p_setting_term);
        ButterKnife.inject(this);
    }

    protected void onResume() {
        super.onResume();
    }

    @OnClick(R.id.btn_close)
    public void onCloseClicked() {
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
