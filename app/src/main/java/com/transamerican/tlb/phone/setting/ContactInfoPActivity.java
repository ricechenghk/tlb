package com.transamerican.tlb.phone.setting;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.transamerican.tlb.ContactCategoryListAdapter;
import com.transamerican.tlb.MainBaseActivity;
import com.transamerican.tlb.R;
import com.transamerican.tlb.model.ContactCategory;
import com.transamerican.tlb.sync.AppDataSource;
import com.transamerican.tlb.util.SimpleLoadingDialog;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;
import hk.com.playmore.syncframework.util.RunnableArgument;
import hk.com.playmore.syncframework.util.SyncFragmentActivity;


public class ContactInfoPActivity extends SyncFragmentActivity {
    private static final String TAG = ContactInfoPActivity.class.getSimpleName();

    @InjectView(R.id.contact_category_list)
    public ListView contactCategoryList;
    public ContactCategoryListAdapter contactCategoryListAdapter;

    private ProgressDialog mLoadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p_contact_info);
        ButterKnife.inject(this);

        contactCategoryListAdapter = new ContactCategoryListAdapter(this);
        contactCategoryListAdapter.setSelectedPosition(-1); //to make all arrow downward
        contactCategoryList.setAdapter(contactCategoryListAdapter);

        showLoading();
        AppDataSource.getInstance().getContactCategoryFromServer(this, this, new RunnableArgument() {
            @Override
            public void run(String object) {
                AppDataSource.getInstance().getContactFromServer(ContactInfoPActivity.this, ContactInfoPActivity.this, new RunnableArgument() {
                    @Override
                    public void run(String object) {
                        hideLoading();
                        List<ContactCategory> categoryList = AppDataSource.getInstance().getContactCategory(ContactInfoPActivity.this);
                        contactCategoryListAdapter.replaceWith(categoryList);
                    }
                });
            }
        });
    }

    protected void onResume() {
        super.onResume();
    }


    @OnClick(R.id.btn_close)
    public void onCloseClicked() {
        finish();
    }


    @OnItemClick(R.id.contact_category_list)
    public void onContactCategoryItemClicked(AdapterView<?> parent, View view, int position, long id) {
        ContactCategory category = contactCategoryListAdapter.getItem(position);
        Intent intent = new Intent(this, ContactDetailPActivity.class);
        intent.putExtra("categoryId", category.getId());
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                finish(); //if ok means close contact, call finish() as well
            }
        }
    }

    public void showLoading() {
        try {
            hideLoading();
            mLoadingDialog = SimpleLoadingDialog.show(this, R.string.loading_text);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideLoading() {
        try {
            if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
                mLoadingDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

