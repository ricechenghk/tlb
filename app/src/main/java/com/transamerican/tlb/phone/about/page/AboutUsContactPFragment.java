package com.transamerican.tlb.phone.about.page;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transamerican.tlb.R;
import com.transamerican.tlb.base.PagerFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Rice on 18/1/15.
 */
public class AboutUsContactPFragment extends PagerFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_p_about_contact_page1, container, false);
        ButterKnife.inject(this, v);
        return v;
    }

    @Override
    public void onPageSelected() {

    }

    @Override
    public void onPageExited() {

    }

    @Override
    public int getTitle() {
        return R.string.about_us_title_contact_us;
    }

    @Override
    public int getSlider() {
        return 0;
    }

    @Override
    public boolean isVideoExist() {
        return false;
    }

    @Override
    public void onReplayClicked() {

    }

    @OnClick(R.id.about_us_contact_bermuda_office_map)
    public void onBermudaOfficeMapClicked() {
        String uri = "geo:0,0?q=32.294335,-64.781183(44 Church Street, Hamilton, Bermuda)";
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));
    }

    @OnClick(R.id.about_us_contact_hk_branch_office_map)
    public void onHongKongOfficeMapClicked() {
        String uri = "geo:0,0?q=22.286087,114.213389(One Island East, 18 Westlands Road, Island East, Hong Kong)";
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));
    }

    @OnClick(R.id.about_us_contact_singapore_office_map)
    public void onSingaporeOfficeMapClicked() {
        String uri = "geo:0,0?q=1.282537,103.851482(1 Finlayson Green 13-00 Singapore 049246)";
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));
    }

}
