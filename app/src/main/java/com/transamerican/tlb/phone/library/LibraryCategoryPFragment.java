package com.transamerican.tlb.phone.library;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.transamerican.tlb.R;
import com.transamerican.tlb.library.adapter.FirstLevelCategoryListAdapter;
import com.transamerican.tlb.library.adapter.InterestRateListAdapter;
import com.transamerican.tlb.listener.MainSectionChangeListener;
import com.transamerican.tlb.model.Library;
import com.transamerican.tlb.model.LibraryCategory;
import com.transamerican.tlb.model.ProdColCategory;
import com.transamerican.tlb.sync.AppDataSource;
import com.transamerican.tlb.util.LibraryExternalStorageHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;
import hk.com.playmore.syncframework.util.SyncFragment;

public class LibraryCategoryPFragment extends SyncFragment {

    MainSectionChangeListener mainSectionChangeListener;

    @InjectView(R.id.section_bar)
    TextView sectionBar;

    @InjectView(R.id.list_category)
    public ListView categoryList;
    public FirstLevelCategoryListAdapter firstLevelMenuAdapter;

    @InjectView(R.id.lib_interest_rate_txt)
    public TextView firstLevelInterestRateTxt;
    @InjectView(R.id.lib_interest_rate_list)
    public ListView firstLevelInterestRateList;
    public InterestRateListAdapter firstLevelInterestRateListAdapter;

    private int currentSection = LibraryCategory.PRODUCT_COLLATERALS; //Default start is Product Collaterals

    private ProgressDialog mLoadingDialog;
    private LibraryExternalStorageHelper storageHelper = new LibraryExternalStorageHelper();

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_p_library_category, container, false);
        ButterKnife.inject(this, view);

        firstLevelMenuAdapter = new FirstLevelCategoryListAdapter(getActivity());
        categoryList.setAdapter(firstLevelMenuAdapter);

        firstLevelInterestRateListAdapter = new InterestRateListAdapter(getActivity());
        firstLevelInterestRateList.setAdapter(firstLevelInterestRateListAdapter);

        if (getArguments() != null) {
            currentSection = getArguments().getInt("section");
            setSectionBarTitle();
        }
        constructLibraryTreeByRootId("");

        return view;
    }

    public void onResume() {
        super.onResume();

        if (mainSectionChangeListener != null) {
            mainSectionChangeListener.onSectionChange(false);
        }
    }

    public void onPause() {
        super.onPause();
    }

    public void constructLibraryTreeByRootId(String id) {
        int count = 0;
        List<ProdColCategory> prodColCatListWithLib = new ArrayList<>();
        List<LibraryCategory> catListWithLib = new ArrayList<>();
        List<Library> allLibList = new ArrayList<>();
        List<? extends LibraryCategory> list = AppDataSource.getInstance().getLibraryCategoryBySection(getActivity(), currentSection, id);
        for (LibraryCategory item : list) {
            List<String> subCatIds = getAllSubCategoryID(item);
            List<? extends Library> libList = AppDataSource.getInstance().getAllLibraryInSubCategoryBySection(getActivity(), currentSection, subCatIds);
            Log.i("", item.getTitle() + ": " + subCatIds.toString() + " , Count: " + libList.size());
            item.setCount(libList.size());
            count += libList.size();

            if (libList.size() > 0) {
                allLibList.addAll(libList);
                catListWithLib.add(item);
            }

            if (currentSection == LibraryCategory.PRODUCT_COLLATERALS) {
                ProdColCategory prodColCategory = (ProdColCategory) item;
                if (prodColCategory.getShortForm() != null && !prodColCategory.getShortForm().isEmpty()) {
                    prodColCatListWithLib.add((ProdColCategory) item);
                }
            }
        }

        firstLevelInterestRateListAdapter.replaceWith(new ArrayList<ProdColCategory>());
        if (id == null || id.isEmpty()) {
            firstLevelMenuAdapter.replaceWith(list);

            if (currentSection == LibraryCategory.PRODUCT_COLLATERALS) {
                firstLevelInterestRateTxt.setVisibility(View.VISIBLE);
                firstLevelInterestRateList.setVisibility(View.VISIBLE);
                firstLevelInterestRateListAdapter.replaceWith(prodColCatListWithLib);
            } else {
                firstLevelInterestRateTxt.setVisibility(View.INVISIBLE);
                firstLevelInterestRateList.setVisibility(View.INVISIBLE);
            }

        }

    }

    public List<String> getAllSubCategoryID(LibraryCategory item) {
        List<String> subCatIds = new ArrayList<String>();
        subCatIds.add(item.getId());
        getSubCategoryID(item, subCatIds);
        return subCatIds;
    }

    public void getSubCategoryID(LibraryCategory root, List<String> subCatIds) {
        List<? extends LibraryCategory> list = AppDataSource.getInstance().getLibraryCategoryBySection(getActivity(), currentSection, root.getId());
        for (LibraryCategory item : list) {
            subCatIds.add(item.getId());
            getSubCategoryID(item, subCatIds);
        }
    }

    public void setMainSectionChangeListener(MainSectionChangeListener mainSectionChangeListener) {
        this.mainSectionChangeListener = mainSectionChangeListener;
    }

    @OnItemClick(R.id.list_category)
    public void onRootCategoryClicked(AdapterView<?> parent, View view, int position, long id) {
        LibraryCategory cate = (LibraryCategory) parent.getItemAtPosition(position);
        Bundle bundle = new Bundle();
        bundle.putInt("section", currentSection);
        bundle.putString("id", cate.getId());
        bundle.putString("title", cate.getTitle());
        LibraryDetailPFragment fr = new LibraryDetailPFragment();
        fr.setMainSectionChangeListener(mainSectionChangeListener);
        fr.setArguments(bundle);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fr);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void setSectionBarTitle()
    {
        int resId = R.string.lib_prod_col;
        if(currentSection == LibraryCategory.PRODUCT_COLLATERALS)
        {
            resId = R.string.lib_prod_col;
        }
        else if(currentSection == LibraryCategory.FIELD_COMMUNICATIONS)
        {
            resId = R.string.lib_field_comm;
        }
        else if(currentSection == LibraryCategory.FORMS)
        {
            resId = R.string.lib_forms;
        }
        else if(currentSection == LibraryCategory.UNDERWRITING_GUIDE)
        {
            resId = R.string.lib_under_guide;
        }
        else if(currentSection == LibraryCategory.TRANSWARE)
        {
            resId = R.string.lib_transware;
        }
        sectionBar.setText(resId);
    }
}
