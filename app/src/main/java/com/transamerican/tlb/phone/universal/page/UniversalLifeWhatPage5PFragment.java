package com.transamerican.tlb.phone.universal.page;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transamerican.tlb.R;
import com.transamerican.tlb.base.PagerFragment;

import butterknife.ButterKnife;

/**
 * Created by Rice on 18/1/15.
 */
public class UniversalLifeWhatPage5PFragment extends PagerFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_p_universal_what_page5, container, false);
        ButterKnife.inject(this, v);
        return v;
    }

    @Override
    public void onPageSelected() {

    }

    @Override
    public void onPageExited() {

    }

    @Override
    public int getTitle() {
        return R.string.universal_what_is_universal_life_plan;
    }

    @Override
    public int getSlider() {
        return R.drawable.about_us_slide5_5;
    }

    @Override
    public boolean isVideoExist() {
        return false;
    }

    @Override
    public void onReplayClicked() {

    }
}
