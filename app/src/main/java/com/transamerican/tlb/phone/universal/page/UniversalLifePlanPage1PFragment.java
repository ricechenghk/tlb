package com.transamerican.tlb.phone.universal.page;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transamerican.tlb.R;
import com.transamerican.tlb.base.PagerFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Rice on 18/1/15.
 */
public class UniversalLifePlanPage1PFragment extends PagerFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_p_universal_plan_page1, container, false);
        ButterKnife.inject(this, v);

        return v;
    }

    @Override
    public void onPageSelected() {

    }

    @Override
    public void onPageExited() {

    }

    @Override
    public int getTitle() {
        return R.string.universal_our_universal_life_plan;
    }

    @Override
    public int getSlider() {
        return R.drawable.about_us_slide5_1;
    }

    @Override
    public boolean isVideoExist() {
        return false;
    }

    @Override
    public void onReplayClicked() {

    }

    @OnClick(R.id.universal_life_plan_universal_life_btn)
    public void onLifeClicked() {
        onSectionClicked(8);
    }

    @OnClick(R.id.universal_life_plan_universal_life_max_btn)
    public void onLifeMaxClicked() {
        onSectionClicked(10);
    }

    @OnClick(R.id.universal_life_plan_universal_life_plus_btn)
    public void onLifePlanClicked() {
        onSectionClicked(9);
    }

    public void onSectionClicked(int page) {
        if (listener != null) {
            listener.goToPage(page);
        }
    }
}
