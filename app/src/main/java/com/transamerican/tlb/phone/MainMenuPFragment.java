package com.transamerican.tlb.phone;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.transamerican.tlb.R;
import com.transamerican.tlb.TitleView;
import com.transamerican.tlb.listener.MainSectionChangeListener;
import com.transamerican.tlb.phone.about.AboutUsPFragment;
import com.transamerican.tlb.phone.library.LibraryPFragment;
import com.transamerican.tlb.phone.library.LoginPFragment;
import com.transamerican.tlb.phone.term.TermLifePFragment;
import com.transamerican.tlb.phone.universal.UniversalLifePFragment;
import com.transamerican.tlb.sync.AppDataSource;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MainMenuPFragment extends Fragment {
    private static final String TAG = MainMenuPFragment.class.getSimpleName();

    MainSectionChangeListener mainSectionChangeListener;

    @InjectView(R.id.about_sub)
    LinearLayout subAbout;

    @InjectView(R.id.universal_sub)
    LinearLayout subUniversal;

    @InjectView(R.id.term_sub)
    LinearLayout subTerm;

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_p_main_menu, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    public void replaceFragment(Fragment fr) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fr);
        fragmentTransaction.commit();
    }

    public void addFragment(Fragment fr) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fr);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void setMainSectionChangeListener(MainSectionChangeListener mainSectionChangeListener) {
        this.mainSectionChangeListener = mainSectionChangeListener;
    }

    @OnClick(R.id.about)
    public void onAboutClicked() {
        subUniversal.setVisibility(View.GONE);
        subTerm.setVisibility(View.GONE);
        subAbout.setVisibility(subAbout.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.universal)
    public void onUniversalClicked() {
        subAbout.setVisibility(View.GONE);
        subTerm.setVisibility(View.GONE);
        subUniversal.setVisibility(subUniversal.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.term)
    public void onTermClicked() {
        subUniversal.setVisibility(View.GONE);
        subAbout.setVisibility(View.GONE);
        subTerm.setVisibility(subTerm.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.library)
    public void onLibraryClicked() {
        subUniversal.setVisibility(View.GONE);
        subAbout.setVisibility(View.GONE);
        subTerm.setVisibility(View.GONE);

        boolean isLogin = AppDataSource.getInstance().isLogin(getActivity());
        if (isLogin) {
            LibraryPFragment fr = new LibraryPFragment();
            fr.setMainSectionChangeListener(mainSectionChangeListener);
            replaceFragment(fr);

            onSectionChange();
        } else {
            LoginPFragment fr = new LoginPFragment();
            fr.setMainSectionChangeListener(mainSectionChangeListener);
            addFragment(fr);
        }
    }

    public void onSectionChange() {
        if (mainSectionChangeListener != null) {
            mainSectionChangeListener.onSectionChange(true);
        }
    }

    public void onAboutSectionClicked(int section) {
        AboutUsPFragment fr = new AboutUsPFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("section", section);
        fr.setArguments(bundle);
        replaceFragment(fr);

        onSectionChange();
    }

    @OnClick(R.id.about_sub_who)
    public void onAboutWhoClicked() {
        onAboutSectionClicked(TitleView.INDEX_ABOUT_WHO);
    }

    @OnClick(R.id.about_sub_strength)
    public void onAboutStrengthClicked() {
        onAboutSectionClicked(TitleView.INDEX_ABOUT_STRENGTH);
    }

    @OnClick(R.id.about_sub_contact)
    public void onAboutContactClicked() {
        onAboutSectionClicked(TitleView.INDEX_ABOUT_CONTACT);
    }

    public void onUniversalSectionClicked(int section) {
        UniversalLifePFragment fr = new UniversalLifePFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("section", section);
        fr.setArguments(bundle);
        replaceFragment(fr);

        onSectionChange();
    }

    @OnClick(R.id.universal_sub_what)
    public void onUniversalWhatClicked() {
        onUniversalSectionClicked(TitleView.INDEX_UNIVERSAL_ONE);
    }

    @OnClick(R.id.universal_sub_plan)
    public void onUniversalPlanClicked() {
        onUniversalSectionClicked(TitleView.INDEX_UNIVERSAL_TWO);
    }

    public void onTermSectionClicked(int section) {
        TermLifePFragment fr = new TermLifePFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("section", section);
        fr.setArguments(bundle);
        replaceFragment(fr);

        onSectionChange();
    }

    @OnClick(R.id.term_sub_what)
    public void onTermWhatClicked() {
        onTermSectionClicked(TitleView.INDEX_TERM_WHAT);
    }

    @OnClick(R.id.term_sub_super)
    public void onTermSuperClicked() {
        onTermSectionClicked(TitleView.INDEX_TERM_SUPER);
    }

    @OnClick(R.id.term_sub_rop)
    public void onTermRopClicked() {
        onTermSectionClicked(TitleView.INDEX_TERM_ROP);
    }
}
