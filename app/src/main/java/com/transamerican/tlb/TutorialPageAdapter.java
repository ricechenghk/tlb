package com.transamerican.tlb;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by Rice on 24/1/15.
 */

public class TutorialPageAdapter extends PagerAdapter {
    Context context;
    private int[] GalImages = new int[]{
            R.drawable.tutorial_01,
            R.drawable.tutorial_02,
            R.drawable.tutorial_03,
            R.drawable.tutorial_04,
            R.drawable.tutorial_05,
            R.drawable.tutorial_06,
            R.drawable.tutorial_07
    };

    TutorialPageAdapter(Context context) {
        this.context = context;
    }

    public void setTutorialResId(int[] resId) {
        GalImages = resId;
    }

    @Override
    public int getCount() {
        return GalImages.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);
//        int padding = context.getResources().getDimensionPixelSize(R.dimen.padding_medium);
//        imageView.setPadding(padding, padding, padding, padding);
//        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView.setImageResource(GalImages[position]);
        ((ViewPager) container).addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }
}